New features:
- Dockerfiles: An image which contains all information for the database (postgreSQL) and another image with all backend information of the testdatengenerator
- Docker-Compose.yml: It starts the different images and reserves all ports
- tdg_entrypoint.sh: It's possible to revise the config.properties file inside the container 
