FROM vm-129-150.cloud.dkfz-heidelberg.de/maven-samply AS build

ADD . /workingdir/
RUN cd /workingdir && \
    mvn clean && \
    mvn install -DskipTests && \
    cd target && \
    mkdir /build && \
    unzip *.war -d /build


FROM tomcat:7-jre8

ENV DEPLOYMENT_CONTEXT "ROOT"

RUN rm -r /usr/local/tomcat/webapps/*
RUN apt update && apt install dos2unix

COPY --from=build /build /usr/local/tomcat/webapps/$DEPLOYMENT_CONTEXT
COPY ./tdg_entrypoint.sh tdg_entrypoint.sh

ENV TDG_TEMP_DIRECTORY ./tobesent
RUN mkdir $TDG_TEMP_DIRECTORY
RUN dos2unix ./tdg_entrypoint.sh

ENV CATALINA_OPTS "-agentlib:jdwp=transport=dt_socket,address=1099,server=y,suspend=n"
CMD ["catalina.sh", "debug"]

ENTRYPOINT ["./tdg_entrypoint.sh"]