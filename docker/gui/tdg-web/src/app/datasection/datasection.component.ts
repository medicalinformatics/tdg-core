import {Component, Input, Output, EventEmitter} from '@angular/core';
import {trigger, state, style, animate, transition} from '@angular/animations';

import '../../assets/font-awesome-4.7.0/css/font-awesome.min.css';

import {Property} from '../model/property';
import {ProfileService} from '../services/profile.service';

@Component({
    selector: 'data-section',
    templateUrl: './datasection.component.html',
    animations: [
        trigger('visible', [
            state('false', style({
                transform: 'rotate(0)'
            })),
            state('true', style({
                transform: 'rotate(90deg)'
            })),
            transition('false => true', animate('100ms ease-in')),
            transition('true => false', animate('100ms ease-out'))
        ])
    ]
})
export class DataSectionComponent{
    @Input() title: string;
    @Input() section: string;
    visible: boolean = false;
    @Input() properties: Property[];
    @Output() onPropertyValueChanges = new EventEmitter<Property>();

    handlePropertyChange(event: any): void{
        this.onPropertyValueChanges.emit(this.properties.find(
            (property: Property) => {return property.pname === event.target.id}
        ));
    }

    getHighlighted(propertyName: string): string{
        let property: Property = this.properties.find((property) => {
            return propertyName == property.pname;
        })
        if(property.isModified)
            return '#F00';
        else '';
    }

}