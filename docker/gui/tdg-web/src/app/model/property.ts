export class Property{
    pname: string;
    probability: number;
    psection: string;
    pgroup: string;
    isModified: boolean;
}

 