import {Property} from './property';

export class Profile {
    name: string;
    properties : Property[];
}