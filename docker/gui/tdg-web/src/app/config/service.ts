import { Injectable } from '@angular/core';

import {config} from './configData';

@Injectable()
export class ConfigService {
    urlPath : string;

    constructor() {
        config.protocol
        config.host
        config.port
        config.rest
    }

    getUrlPath(){
        return this.urlPath = config.protocol + '://' + config.host + config.port + config.rest;
    }

}