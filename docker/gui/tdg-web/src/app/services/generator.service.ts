import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Property} from '../model/property';
import {ConfigService} from "../config/service";

@Injectable()
export class GeneratorService{
    urlPatient : string;

    constructor(private http: HttpClient , public configService: ConfigService){
        this.urlPatient = configService.getUrlPath()
    }

    generatePatients(profileName?: string, amount?: number){
        let body = {
            "amount": (amount != 0) ? amount : 10,
            "profileName": profileName
        };
        return this.http.post(this.urlPatient + 'patientGenerator/generateTnmAndAddToTumour', body);
    }

    generatePatientsFromProperties(properties: Property[], amount: number) {
        let body = {
            "amount": amount,
            "probabilities": this.convertPropertiesToProbabilityJSON(properties),
        };
        console.log(body);
        return this.http.post( this.urlPatient + 'patientGenerator/generateWithProbabilities', body);
    }

    convertPropertiesToProbabilityJSON(properties: Property[]): any[]{
        let probabilityJSON: any[] = [];
        properties.forEach(property => {
            probabilityJSON.push({
                "name": property.pname,
                "section": property.psection,
                "group": property.pgroup,
                "probability": property.probability,
                "modifiedFlag": property.isModified,
            })
        });
        return probabilityJSON;
    };
}