import { Component, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

import { ProfileService} from '../services/profile.service';
import { GeneratorService } from '../services/generator.service';

@Component({
    selector: 'profile-selection',
    templateUrl: './profileselection.component.html',
    providers: [ProfileService, GeneratorService]
})
export class ProfileSelectionComponent{
    profileNames: String[];
    @Input() generateAmount: number;
    @ViewChild('profileName') select: ElementRef;
    @Output() onProfileChange = new EventEmitter<String>();

    constructor(private profileSelection : ProfileService, private generatorService: GeneratorService){
        this.getProfileNames();
    }

    onChangeProfile(): void{
        this.onProfileChange.emit(this.select.nativeElement.value);
    }

    generatePatients(): void{
        console.log(this.select.nativeElement.value)
        this.generatorService.generatePatients(this.select.nativeElement.value, this.generateAmount).subscribe(data => {
            console.log(data);
        })
    }

    getProfileNames(): void {
        this.profileSelection.getProfileNames().subscribe(data => {
            if(data instanceof Array){
                this.profileNames = data as String[];
            }
        });
    }
}