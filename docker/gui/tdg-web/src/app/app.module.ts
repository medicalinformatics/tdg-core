import { BrowserModule }  from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { DataSectionComponent } from './datasection/datasection.component';
import { ProfileSelectionComponent } from './profileselection/profileselection.component';
import { ProfileSaferComponent } from './profilesafer/profilesafer.component';
import {ConfigService} from "./config/service";
import {NgModule} from "@angular/core";


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule
  ],
  declarations: [
    AppComponent,
    DataSectionComponent,
    ProfileSelectionComponent,
    ProfileSaferComponent
  ],
  providers: [ConfigService],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
