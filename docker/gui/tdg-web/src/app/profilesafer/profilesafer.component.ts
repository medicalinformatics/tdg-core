import {Component, Input, Output, EventEmitter} from '@angular/core';

import {Profile} from '../model/profile';
import {ProfileService} from '../services/profile.service';

@Component({
    selector: 'profile-safer',
    templateUrl: './profilesafer.component.html',
    styleUrls: ['./profilesafer.component.css'],
    providers: [ProfileService]
})
export class ProfileSaferComponent{
    profileName : string;
    profileDescription: string;
    update: boolean;
    @Input() profile: Profile;
    @Output() onProfileSafe = new EventEmitter(); 

    constructor(private profileService:ProfileService){
       console.log("Profile safeer" + this.profile);
    }

    safeProfile(): void{
        let name = (this.update) ? this.profile.name : this.profileName;
        this.profileService.safeProfile(name, this.profileDescription, this.profile.properties).subscribe(data => {
            console.log(data);
            this.onProfileSafe.emit();
        });
    }
}