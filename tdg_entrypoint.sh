#!/bin/bash -e

echo HelloWorld

: "${TDG_DB_NAME:=tdgdatabase}"
: "${TDG_DB_USER:=postgres}"
: "${TDG_DB_PASS:=postgres}"
: "${TDG_DB_TYPE:=postgresql}"
: "${TDG_DB_HOST:=db}"
: "${TDG_DB_PORT:=5432}"
: "${TDG_FTP_PORT:=21}"
: "${TDG_FTP_PATH:=/}"
: "${TDG_FTP_SERVER:=mitro-069}"
: "${TDG_FTP_USER:=user}"
: "${TDG_FTP_PASS:=user}"

CONFIG_DIR=/usr/local/tomcat/webapps/${DEPLOYMENT_CONTEXT}/WEB-INF/classes
if [ -e $CONFIG_DIR/config.properties ]; then
    echo "creating tmp config"
    sed -i -e "s|port = 21|port = $TDG_FTP_PORT|g; \
    s|mitro = /|mitro = $TDG_FTP_PATH|g; \
    s|server = mitro-069|server = $TDG_FTP_SERVER|g; \
    s|userFTP = user|userFTP = $TDG_FTP_USER|g; \
    s|passwordFTP = user|passwordFTP = $TDG_FTP_PASS|g; \
    s|userDB = postgres|userDB = $TDG_DB_USER|g; \
    s|passwordDB = postgres|passwordDB = $TDG_DB_PASS|g; \
    s|urlDB = jdbc:postgresql://localhost:5433/tdgdatabase|urlDB = jdbc:$TDG_DB_TYPE://$TDG_DB_HOST:$TDG_DB_PORT/$TDG_DB_NAME|g " \
    $CONFIG_DIR/config.properties
    echo "new config should look like this"
    cat $CONFIG_DIR/config.properties
fi
exec catalina.sh run

