package dktk.testdatengenerator.xml;

import java.io.*;

import java.util.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.xml.bind.Marshaller;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import de.kairos_med.CentraXXDataExchange;

import dktk.testdatengenerator.customelements.Patient;
import dktk.testdatengenerator.generator.centraxx.CentraXXGenerator;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import properties.PropertiesEnum;

/**
 * This class has the exercise to split the incoming PatientDataSets into good sizes for processing. </br>
 * It stores the incoming [PatientDataSetType] in a Queue and then gives chunks of this data to different [XMLFileWriter]
 * Created by brennert on 19.06.2017.
 */
public class XMLFileWriterFactory {

    private String path;

    private String user;

    private String mitro;

    private String server;

    private String password;

    private int port;

    private int fileCount;

    private int currentProcessing;

    private int maxProcessing = 100;

    private JAXBContext jaxbContext;

    private Queue<Patient> queue;

    private FTPClient ftpClient = new FTPClient();

    private boolean isSendFilesToCentraXX = true;
    private boolean isDeleteFilesAfterProcess = true;

    private CentraXXGenerator centraXXGenerator = new CentraXXGenerator();


    public XMLFileWriterFactory(int maxPatientsFile) throws IOException {

        Properties properties = new Properties();

        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String propertyFile = rootPath + "config.properties";
        properties.load(new FileInputStream(propertyFile));

        this.server = properties.getProperty((PropertiesEnum.SERVER).toString());
        this.user = properties.getProperty((PropertiesEnum.USER).toString());
        this.port = new Integer(properties.getProperty(PropertiesEnum.PORT.toString()));
        this.mitro = properties.getProperty((PropertiesEnum.MITRO).toString());
        this.password = properties.getProperty((PropertiesEnum.PASSWORD).toString());
        this.path = getPath(properties);

        if (maxPatientsFile > 0)
            maxProcessing = maxPatientsFile;
        else
            maxProcessing = 100;
        try {
            jaxbContext = JAXBContext.newInstance(CentraXXDataExchange.class);
        } catch (JAXBException jaxbe) {
            System.out.println("JAXBContext couldn't be created");
            jaxbe.printStackTrace();
        }
        queue = new LinkedList<Patient>();
        fileCount = 0;
        currentProcessing = 0;
    }

    private String getPath(Properties properties){

        String path = System.getenv("TDG_TEMP_DIRECTORY");
        return (path == null) ? properties.getProperty(PropertiesEnum.PATH.toString()) : path;

    }

    public static boolean uploadSingleFile(FTPClient ftpClient, String localFilePath, String remoteFilePath) throws IOException {
        File localFile = new File(localFilePath);
        InputStream inputStream = new FileInputStream(localFile);
        try {
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            return ftpClient.storeFile(remoteFilePath, inputStream);
        } finally {
            inputStream.close();
        }
    }

    /**
     * This method adds an element to the Queue for Elements which should be progressed.</br>
     * There are 2 different cases in which this method can be called: </br>
     * 1.) The class is currently in the state of doing nothing. In this case the add function should </br>
     * initiate the work of this class </br>
     * 2.) The class is working and the method will only add this element to the input queue
     *
     * @param patients
     */
    public void add(ArrayList<Patient> patients) {
        for (Patient patient : patients) {
            queue.add(patient);
        }
        // TODO: protect against concurrent processing
        doWork();
    }

    public void add(Patient patient) {
        queue.add(patient);

        doWork();
    }

    private void doWork() {
        while (queue.peek() != null) {

            ArrayList<Patient> outputPatients = new ArrayList<Patient>();
            while (currentProcessing < maxProcessing && queue.peek() != null) {
                outputPatients.add(queue.remove());
                currentProcessing = currentProcessing + 1;
            }

            File resultFile = createNewFile();
            Marshaller marshaller = createMarshaller(jaxbContext);
            CentraXXDataExchange centraXXDataExchange = centraXXGenerator.generate(outputPatients);
            XMLFileWriter xmlFileWriter = new XMLFileWriter(marshaller, resultFile, centraXXDataExchange);
            xmlFileWriter.writeToFile(); // TODO: to be executed out of the factory

            if (isSendFilesToCentraXX){
                sendResultFileToCentraXX(resultFile);
            }

            if (isDeleteFilesAfterProcess){
                resultFile.deleteOnExit();
            }

            currentProcessing = 0;
        }
    }

    private void sendResultFileToCentraXX(File resultFile){

        try {

            ftpClient.connect(server, port);
            ftpClient.login(user, password);

            // Use local passive mode (is necessary to pass firewall)
            ftpClient.enterLocalPassiveMode();

            String remoteDirPath = mitro   + resultFile.getName();
            uploadSingleFile(ftpClient, path + "/" + resultFile.getName() , remoteDirPath);

            ftpClient.logout();
            ftpClient.disconnect();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    // ---------- Helping Methods ---------- //
    private File createNewFile() {
        fileCount = fileCount + 1;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss-SS");
        File file = new File(path + "/" + "patients_" + dateFormat.format(new Date()) + ".xml");
        try {
            file.createNewFile();
        } catch (IOException ioe) {
            System.out.println("Couldn't create new File");
            ioe.printStackTrace();
        }
        return file;
    }

    private Marshaller createMarshaller (JAXBContext jaxbContext){

        try {

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.kairos-med.de ../CentraXXExchange.xsd");

            return marshaller;

        } catch (JAXBException e) {
            e.printStackTrace();
            return null; // TODO: throw exception
        }
    }

    public void setSendFilesToCentraXX(boolean sendFilesToCentraXX) {
        isSendFilesToCentraXX = sendFilesToCentraXX;
    }

    public void setDeleteFilesAfterProcess(boolean deleteFilesAfterProcess) {
        isDeleteFilesAfterProcess = deleteFilesAfterProcess;
    }
}
