package dktk.testdatengenerator.xml;

import java.io.File;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.Marshaller;
import javax.xml.bind.JAXBException;

import de.kairos_med.EffectDataType;
import de.kairos_med.PatientDataSetType;
import de.kairos_med.CentraXXDataExchange;
import de.kairos_med.EntitySourceEnumType;

/**
 * This class gets three parameters.</br>
 * First a JAXBContext from the class [CentraXXDataExchange]. This is needed because the repeated creation of this context takes a big amount of time.</br>
 * Second a File to which it should write.</br>
 * Third the data which should be written to the file.
 * Created by brennert on 02.06.2017.
 */
public class XMLFileWriter {

    private File file;
    private CentraXXDataExchange centraXXDataExchange;
    private Marshaller marshaller;


    public XMLFileWriter(Marshaller marshaller, File testResultFile, CentraXXDataExchange centraXXDataExchange) {

        this.marshaller = marshaller;

        if (testResultFile == null) {
            testResultFile = new File(System.getenv("TDG_TEMP_DIRECTORY") + "/" + "testResult.xml");
        }
        if (!testResultFile.exists()) {
            try {
                testResultFile.createNewFile();
                setFile(testResultFile);
            } catch (IOException ioe) {
                System.out.println("Couldn't create a new File");
                ioe.printStackTrace();
            }
        } else {
            setFile(testResultFile);
        }

        this.centraXXDataExchange = centraXXDataExchange;

    }

    /**
     * This method writes the ArrayList<PatientDataSetType> patients to the XML File
     */
    public void writeToFile() {
        marshallCentraXXDataExchange(centraXXDataExchange);
    }

    // ---------- Helping methods ---------- //

    private void marshallCentraXXDataExchange(CentraXXDataExchange centraXXDataExchange) {
        try {
            marshaller.marshal(centraXXDataExchange, getFile());
        } catch (JAXBException jaxbe) {
            System.out.println("Couldn't create jaxb context");
            jaxbe.printStackTrace(); // TODO: throw exception
        }
    }

    // ----------- Getters and Setters ---------- //

    public File getFile() {
        return file;
    }

    public List<PatientDataSetType> getPatients() {
        return (centraXXDataExchange != null) ? centraXXDataExchange.getEffectData().getPatientDataSet() : new ArrayList<>();
    }

    public void setFile(File file) {
        this.file = file;
    }
}
