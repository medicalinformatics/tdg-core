package dktk.testdatengenerator.interfaces;

import java.io.Closeable;

/**
 * This interface is for CRUD Operations which means, that classes which implement this interface must support
 * all CRUD Operations which are: create, read, update and delete.
 * Created by brennert on 13.07.2017.
 */
public interface CRUDInterface extends Closeable {
    /**
     * This method creates a DataSet in Database
     * @param name
     * @param value
     */
    void create(String name, double value);

    /**
     * This methods reads all values from database for a given attribute name
     * @param name
     * @return
     */
    double read(String name);

    /**
     * This method updates a given dataset by a given value
     * @param name
     * @param value
     */
    void update(String name, double value);

    /**
     * This method deletes a DataSet from database
     * @param name
     */
    void delete(String name);


}
