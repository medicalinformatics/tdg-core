package dktk.testdatengenerator.enums;

/**
 * Created by Brennert on 18.07.2017.
 */
public enum OncologyOrganisationUnitsEnum {

    NEU17("Dr. Senckenbergisches Institut für Neuroonkologie"),
    STR("Klinik für Strahlentherapie und Onkologie"),
    HÄM("Medizinische Klinik II: Hämatologie und Onkologie, Rheumatologie, Infektiologie, Therapie der HIV-Er");

    private String description;

    private OncologyOrganisationUnitsEnum(String description){
        this.description = description;
    }

    public String getDescription(){
        return this.description;
    }

}
