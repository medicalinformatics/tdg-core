package dktk.testdatengenerator.enums;

/**
 * Created by BrennerT on 30.06.2017.
 */
public enum AgeGroupEnum {

    Under10(0, 9),
    Between10And19(10, 19),
    Between20And29(20, 29),
    Between30And39(30, 39),
    Between40And49(40, 49),
    Between50And59(50, 59),
    Between60And69(60, 69),
    Between70And79(70, 79),
    Between80And84(80, 84),
    Over85(85, 110);

    private int min;
    private int max;

    private AgeGroupEnum(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public String value() {
        return name();
    }

    public static AgeGroupEnum fromValue(String v) {
        return valueOf(v);
    }

    public int getMin() {
        return this.min;
    }

    public int getMax() {
        return this.max;
    }

}
