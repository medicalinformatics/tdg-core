package dktk.testdatengenerator.enums;

/**
 * Created by Brennert on 18.07.2017.
 */
public enum OrganisationUnitsEnum {
    GYN("Frauenheilkunde und Geburtshilfe"),
    EXT("ex domo"),
    NUK("Klinik für Nuklearmedizin"),
    UHW("Klinik für Unfall-, Hand- und Wiederherstellungschirurgie"),
    URO("Klinik für Urologie und Kinderurologie"),
    NCH("Klinik und Poliklinik für Neurochirurgie"),
    Palliativmedizin("Palliativmedizin"),
    RAD("Zentrum der Radiologie"),
    ZNA("Zentrale Notaufnahme"),
    PAD("Klinik für Kinder- und Jugendmedizin"),
    KICH("Kinderchirurgie und Kinderurologie"),
    IPU("Lungenzentrum"),
    NEUROPATH("Neuropathologie"),
    HEMAT("Hämatologie"),
    KAIS("Klinik für Anästhesiologie, Intensivmedizin und Schmerztherapie"),
    NEUROLOGIE("NEUROLOGIE"),
    HAUTKLINIK("HAUTKLINIK"),
    UROLOGIE("UROLOGIE");

    private String description;

    private OrganisationUnitsEnum(String description){
        this.description = description;
    }

    public String getDescription(){
        return this.description;
    }
}
