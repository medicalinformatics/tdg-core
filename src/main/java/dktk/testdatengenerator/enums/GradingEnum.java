package dktk.testdatengenerator.enums;

/**
 * Created by brennert on 21.07.2017.
 */
public enum GradingEnum {
    Zero("malignes Melonom der Konjunktiva"),
    One("gut differenziert"),
    Two("mäßig differenziert"),
    Three("schlecht differenziert"),
    Four("undifferenziert"),
    X("nicht bestimmbar"),
    L("low grade(G1 oder G2)"),
    M("intermediate(G2 oder G3)"),
    H("high grade(G3 oder G4)"),
    B("Borderline"),
    U("unbekannt"),
    T("trifft nicht zu");

    private String defition;

    private GradingEnum(String definition){
        this.defition = definition;
    }

    public String getDefition() {
        return defition;
    }

}
