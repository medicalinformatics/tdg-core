package dktk.testdatengenerator.enums;

/**
 * Created by brennert on 29.06.2017.
 */
public enum TitleEnum {
    DrMed,
    PDDrMedDrMed,
    Prof,
    ProfDrMed;

    public String value() {
        String returnTitle = name();
        returnTitle = returnTitle.replace("Dr", "Dr.");
        returnTitle = returnTitle.replace("Med", "med.");
        returnTitle = returnTitle.replace("Prof", "Prof.");
        returnTitle = returnTitle.replace("PD", "PD ");
        returnTitle = returnTitle.replaceAll(".(a-zA-Z)", ".$1");
        returnTitle = returnTitle.replace(".",". ");
        return returnTitle.trim();
    }

    public static TitleEnum fromValue(String v) {
        return valueOf(v);
    }

}