package dktk.testdatengenerator.enums;

/**
 * Enum which contains the different MDR Values for the SampleTypes
 */
public enum SampleTypeEnumType{

    Protein,
    RNA,
    DNA,
    Knochenmark,
    Liquor,
    Urin,
    Plasma,
    Serum,
    Vollblut,
    Normalgewebe,
    Tumorgewebe

}
