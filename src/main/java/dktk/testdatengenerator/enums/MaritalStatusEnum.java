package dktk.testdatengenerator.enums;

/**
 * Created by brennert on 28.06.2017.
 */
public enum MaritalStatusEnum {
    // Anulled("Annu"),
    Devorced("GS"),
    // Interlocutory("Interl"),
    // LegallySeparated("Getr"),
    Married("VH"),
    // Polygamous("Poly"),
    NeverMarried("LD"),
    DomesticPartner("VP"),
    Widow("VW");

    public String value() {
        return name().replaceAll("([a-z])([A-Z])", "$1\\ $2");
    }

    public static MaritalStatusEnum fromValue(String v) {
        return valueOf(v);
    }

    private String code;

    private MaritalStatusEnum(String code){
        this.code = code;
    }

    public String getCode(){
        return code;
    }

}
