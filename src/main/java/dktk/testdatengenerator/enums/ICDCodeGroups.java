package dktk.testdatengenerator.enums;

/**
 * Created by brennert on 06.07.2017.
 */
public enum ICDCodeGroups {

    /**
     * This Value represents all missing values
     */
    C16(new String[]{"C16"}),
    CBetween18And21(new String[]{"C18","C19","C20","C21"}),
    C22(new String[]{"C22"}),
    C25(new String[]{"C25"}),
    CBetween33And34(new String[]{"C33", "C34"}),
    C43(new String[]{"C43"}),
    C50(new String[]{"C50"}),
    C53(new String[]{"C53"}),
    C56(new String[]{"C56"}),
    C61(new String[]{"C61"}),
    C64(new String[]{"C64"}),
    C62(new String[]{"C62"}),
    C73(new String[]{"C73"}),
    CBetween91And95(new String[]{"C91","C92","C93","C94","C95"}),
    D00(new String[]{"D00"}),
    DBetween32And35(new String[]{"D32","D33","D34","D35.2","D35.3","D35.4"}),
    DBetween37And48(new String[]{"D37","D38","D39","D40","D41","D42","D43","D44","D45","D46","D47","D48"});

    private String[] codes;

    private ICDCodeGroups(String[] numbers){
        setCodes(numbers);
    }

    public String[] getCodes() {
        return codes;
    }


    private void setCodes(String[] codes) {
        this.codes = codes;
    }

}
