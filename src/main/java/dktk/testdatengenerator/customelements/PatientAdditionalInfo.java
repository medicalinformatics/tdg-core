package dktk.testdatengenerator.customelements;

import de.kairos_med.FlexibleDataSetInstanceType;

import java.util.ArrayList;
import java.util.List;

public class PatientAdditionalInfo {

    private List<FlexibleDataSetInstanceType> flexibleDataSetInstanceList = new ArrayList<>();

    public void addMolecularMarker(FlexibleDataSetInstanceType molecularMarker){
        flexibleDataSetInstanceList.add(molecularMarker);
    }

    public List<FlexibleDataSetInstanceType> getAllFlexibleDataSetInstances(){
        return flexibleDataSetInstanceList;
    }

}
