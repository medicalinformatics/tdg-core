package dktk.testdatengenerator.customelements;

import de.kairos_med.PatientDataSetType;

public class Patient {

    private PatientDataSetType patientDataSet;
    private PatientAdditionalInfo patientAdditionalInfo = new PatientAdditionalInfo();


    public Patient() {
        this.patientDataSet = new PatientDataSetType();
    }

    public Patient(PatientDataSetType patientDataSet) {
        this.patientDataSet = patientDataSet;
    }

    public PatientDataSetType getPatientDataSet() {
        return patientDataSet;
    }

    public PatientAdditionalInfo getPatientAdditionalInfo() {
        return patientAdditionalInfo;
    }

}
