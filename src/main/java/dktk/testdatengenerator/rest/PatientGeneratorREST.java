package dktk.testdatengenerator.rest;

import java.util.Date;
import java.util.ArrayList;

import java.io.IOException;

import javax.ws.rs.*;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import dktk.testdatengenerator.customelements.Patient;
import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.db.DbConnection;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.db.model.Probability;
import dktk.testdatengenerator.xml.XMLFileWriterFactory;
import dktk.testdatengenerator.rest.model.PostParameter;
import dktk.testdatengenerator.rest.model.ProbabilityXML;
import dktk.testdatengenerator.generator.centraxx.PatientGenerator;
import dktk.testdatengenerator.exceptions.ProbabilityException;

/**
 * Created by brennert on 27.07.2017.
 */
@Provider
@Path("/patientGenerator")
public class PatientGeneratorREST {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response sayPlainTextHello() {
        return Response.ok("This is the Patientgenerator REST Interface").build();
    }

    @POST
    @Path("/generateTnmAndAddToTumour")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces("text/plain")
    public Response generatePatients(final PostParameter postParameter) { // TODO: Business logic in another class

        try (DbConnection dbConnection = new DbConnection(new DBConnector())) {

            Date start = new Date();
            TDGProfile tdgProfile = null;
            if (postParameter.profileName == null)
                tdgProfile = new TDGProfile(dbConnection.getDbConnector());
            else
                tdgProfile = new TDGProfile(postParameter.profileName, null, dbConnection.getDbConnector());
            PatientGenerator patientGenerator = new PatientGenerator(tdgProfile, dbConnection.getDbConnector());
            generate(postParameter, patientGenerator);
            Date end = new Date();
            System.out.println("The request is processed. Time needed: " + (end.getTime() - start.getTime()));

        } catch (IOException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }

        return Response.ok("generated Patients: with profile: " + postParameter.profileName).build();
    }

    @POST
    @Path("/generateWithProbabilities")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces("text/plain")
    public Response generatePatientsFromProbabilities(final PostParameter postParameter) {

        try (DbConnection dbConnection = new DbConnection(new DBConnector())) {

            Date start = new Date();
            if (postParameter.probabilities == null)
                Response.noContent().build();
            ArrayList<Probability> probabilities = convertProbabilities(postParameter.probabilities);
            TDGProfile tdgProfile = new TDGProfile(probabilities, dbConnection.getDbConnector());
            PatientGenerator patientGenerator = new PatientGenerator(tdgProfile, dbConnection.getDbConnector());

            // TODO: make asynchron (create a new thread)
            // TODO: make thread status available : Table Thread-ID + Status-Info (additional logic)
            generate(postParameter, patientGenerator);
            Date end = new Date();
            System.out.println("The request is processed. Time needed: " + (end.getTime() - start.getTime()));

        } catch (IOException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }

        return Response.ok("generated Patients: with no currently safed profile").build();
    }

    private void generate(PostParameter postParameter, PatientGenerator patientGenerator) throws IOException {
        /** Dateien mit circa 1000 Patientendatensätzen stehen unter Verdacht, Import Fehler
         * bei CentraXX zu verursachen. Deshalb wurde der Standardwert auf 100 abgesenkt.
         */
        XMLFileWriterFactory xmlFileWriterFactory = new XMLFileWriterFactory(100);
        while(postParameter.amount != 0){
            ArrayList<Patient> generatedPatients = null;
            if(postParameter.amount < 1000){
                generatedPatients = patientGenerator.generate(postParameter.amount);
                System.out.println("amount: "+ postParameter.amount + " patients are generated");
                postParameter.amount -= postParameter.amount;
                System.out.println("Now finished generating. Rest amount is: " + postParameter.amount);
            }
            else{
                generatedPatients = patientGenerator.generate(1000);
                System.out.println("amountPerFile: "+ 1000 + " patients are generated");
                postParameter.amount -= 1000;
                System.out.println("Remaining amount of patients is: " + postParameter.amount);
            }
            xmlFileWriterFactory.add((ArrayList<Patient>) generatedPatients);
            generatedPatients.clear();
        }
    }

    private ArrayList<Probability> convertProbabilities(ArrayList<ProbabilityXML> probabilitiesXML) {

        try (DbConnection dbConnection = new DbConnection(new DBConnector())) {

            ArrayList<Probability> probabilities = new ArrayList<>();
            for (ProbabilityXML probabilityXML : probabilitiesXML) {
                try {
                    Probability prob = new Probability(probabilityXML.name, probabilityXML.probability, dbConnection.getDbConnector());
                    if (probabilityXML.modifiedFlag)
                        prob.setModifiedFlag();
                    probabilities.add(prob);
                } catch (ProbabilityException pe) {
                    pe.printStackTrace();
                }
            }

            return probabilities;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

