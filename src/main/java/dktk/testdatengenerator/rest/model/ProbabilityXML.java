package dktk.testdatengenerator.rest.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by brennert on 03.08.2017.
 */
@XmlRootElement
public class ProbabilityXML {

    @XmlElement
    public String name;

    @XmlElement
    public String section;

    @XmlElement
    public String group;

    @XmlElement
    public double probability;

    @XmlElement
    public boolean modifiedFlag;

}
