package dktk.testdatengenerator.rest.model;

import dktk.testdatengenerator.db.model.Probability;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by brennert on 27.07.2017.
 */
@XmlRootElement
public class PostParameter {
    @XmlElement
    public int amount;
    @XmlElement
    public String profileName;
    @XmlElement
    public String profileDescription;
    @XmlElement
    public ArrayList<ProbabilityXML> probabilities;
}
