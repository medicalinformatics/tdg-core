package dktk.testdatengenerator.rest;

import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.db.DbConnection;
import dktk.testdatengenerator.db.model.Probability;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.exceptions.ProbabilityException;
import dktk.testdatengenerator.rest.model.PostParameter;
import dktk.testdatengenerator.rest.model.ProbabilityXML;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by brennert on 28.07.2017.
 */
@Path("/profile")
public class ProfileREST {

    @GET
    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    public TDGProfile getStandardProfile(){
        DBConnector dbConnector = new DBConnector();
        dbConnector.openConnection();

        // TODO: Where is dbConnector closed?
        return new TDGProfile(dbConnector);
    }

    @GET
    @Path("/get{profileName}")
    @Produces(MediaType.APPLICATION_JSON)
    public TDGProfile getProfile(@PathParam("profileName") String profileName){
        DBConnector dbConnector = new DBConnector();
        dbConnector.openConnection();

        // TODO: Where is dbConnector closed?
        return new TDGProfile(profileName, null, dbConnector);
    }

    @GET
    @Path("/getNames")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProfileNames(){

        ArrayList<String> profileNames = new ArrayList<>();

        try (DbConnection dbConnection = new DbConnection(new DBConnector())){

            ResultSet resultSet = dbConnection.getDbConnector().sendQuery("SELECT profile_name FROM profile");
            while (resultSet.next())
                profileNames.add(resultSet.getString("profile_name"));

        }catch (SQLException sqle){
            sqle.printStackTrace();
            return Response.serverError().build();
        } catch (IOException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }

        return Response.ok(profileNames, MediaType.APPLICATION_JSON).build();
    }

    @PUT
    @Path("/safeProfile")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response safeProfile(final PostParameter postParameter){
        if(postParameter.probabilities == null)
            return Response.noContent().build();
        ArrayList<Probability> probabilities = convertProbabilities(postParameter.probabilities);

        try (DbConnection dbConnection = new DbConnection(new DBConnector())) {
            TDGProfile tdgProfile = new TDGProfile(probabilities, postParameter.profileName, postParameter.profileDescription, dbConnection.getDbConnector());
            tdgProfile.safeProfile();
        } catch (IOException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }

        return Response.ok("The profile with name: " + postParameter.profileName + " and description : " + postParameter.profileDescription + " was created").build();
    }

    @GET
    @Path("/getPropertyDefault{propertyName}")
    @Produces(MediaType.APPLICATION_JSON)
    public double getPropertyDefault(@PathParam("propertyName") String propertyName){

        try (DbConnection dbConnection = new DbConnection(new DBConnector())){
            return new TDGProfile(dbConnection.getDbConnector()).getProbability(propertyName).getProbability();
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    private ArrayList<Probability> convertProbabilities(ArrayList<ProbabilityXML> probabilitiesXML) {

        ArrayList<Probability> probabilities = new ArrayList<>();

        try (DbConnection dbConnection = new DbConnection(new DBConnector())) {

            for (ProbabilityXML probabilityXML : probabilitiesXML) {
                try {
                    Probability prob = new Probability(probabilityXML.name, probabilityXML.probability, dbConnection.getDbConnector());
                    if (probabilityXML.modifiedFlag)
                        prob.setModifiedFlag();
                    probabilities.add(prob);
                } catch (ProbabilityException pe) {
                    pe.printStackTrace();
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        return probabilities;

    }


}
