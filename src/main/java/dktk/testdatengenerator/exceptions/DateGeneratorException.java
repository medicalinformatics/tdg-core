package dktk.testdatengenerator.exceptions;

/**
 * Created by brennert on 11.07.2017.
 */
public class DateGeneratorException extends Exception{

    public DateGeneratorException(){
        // parameterless constructor
    }

    public DateGeneratorException(String message){
        super(message);
    }

}
