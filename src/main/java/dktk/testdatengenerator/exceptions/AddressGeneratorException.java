package dktk.testdatengenerator.exceptions;

/**
 * Created by brennert on 04.07.2017.
 */
public class AddressGeneratorException extends Exception {

    public AddressGeneratorException() {
        // parameterless exception
    }

    public AddressGeneratorException(String message) {
        super(message);
    }

}
