package dktk.testdatengenerator.exceptions;

/**
 * Created by brennert on 13.07.2017.
 */
public class ProbabilityException extends Exception {

    public ProbabilityException(){
        // parameterless constructor
    }

    public ProbabilityException(String message){
        super(message);
    }

}
