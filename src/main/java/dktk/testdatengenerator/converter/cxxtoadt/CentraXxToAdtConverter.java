package dktk.testdatengenerator.converter.cxxtoadt;

import de.gekid.namespace.ADTGEKID;
import de.kairos_med.CentraXXDataExchange;

public interface CentraXxToAdtConverter {

    public ADTGEKID convert (CentraXXDataExchange centraXXDataExchange);

}
