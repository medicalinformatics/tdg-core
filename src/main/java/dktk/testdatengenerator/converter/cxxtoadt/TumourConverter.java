package dktk.testdatengenerator.converter.cxxtoadt;

import de.gekid.namespace.ADTGEKID;
import de.kairos_med.*;
import dktk.testdatengenerator.converter.general.Converter;
import dktk.testdatengenerator.converter.general.Node;

import java.util.List;

public class TumourConverter implements Converter<GTDSTumourEIType, ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung.Tumorzuordnung> {


    @Override
    public void convertAndAdd(Node<GTDSTumourEIType> cxxTumourNode, Node<ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung.Tumorzuordnung> adtTumourNode) {

        GTDSTumourEIType cxxTumour = cxxTumourNode.getElement();

        if (cxxTumour != null) {

            List<GTDSProgressEIType> progressList = cxxTumour.getProgress();
            List<GTDSTNMEIType> tnmList = cxxTumour.getTnm();
            List<GTDSTumourLocalisationEIType> tumourLocalisationList = cxxTumour.getTumourLocalisation();
            List<GTDSHistologyType> histologyList = cxxTumour.getHistology();
            List<GTDSMetastasisEIType> metastasisList = cxxTumour.getMetastasis();

            ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung.Tumorzuordnung tumorzuordnung = adtTumourNode.getElement();

            addProgress(progressList);
            addTnm(tnmList);
            addTumourLocalisation(tumourLocalisationList);
            addHistology(histologyList);
            addMetastasis(metastasisList);

        }

    }

    private void addProgress (List<GTDSProgressEIType> progressList){

        //TODO
    }

    private void addTnm (List<GTDSTNMEIType> tnmList){

        //TODO


    }

    private void addTumourLocalisation (List<GTDSTumourLocalisationEIType> tumourLocalisationList){

        //TODO

    }

    private void addHistology (List<GTDSHistologyType> histologyList){

        //TODO
    }

    private void addMetastasis (List<GTDSMetastasisEIType> metastasisList){

        //TODO
    }


}
