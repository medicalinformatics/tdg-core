package dktk.testdatengenerator.converter.cxxtoadt;

import de.gekid.namespace.ADTGEKID;
import de.kairos_med.DateType;
import de.kairos_med.DiagnosisType;
import de.kairos_med.GTDSTumourEIType;
import de.kairos_med.IcdEntryRefType;
import dktk.testdatengenerator.converter.general.Converter;
import dktk.testdatengenerator.converter.general.ConverterManager;
import dktk.testdatengenerator.converter.general.Node;

import java.util.List;

public class DiagnosisConverter implements Converter<DiagnosisType, ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung.Diagnose> {

    private TumourConverter tumourConverter = new TumourConverter();


    @Override
    public void convertAndAdd(Node<DiagnosisType> cxxDiagnosisNode, Node<ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung.Diagnose> adtDiagnosisNode) {

        DiagnosisType cxxDiagnosis = cxxDiagnosisNode.getElement();

        if (cxxDiagnosis != null){

            ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung.Diagnose adtDiagnosis = adtDiagnosisNode.getElement();

            // CXX: DIAG_DIAGNOSISCODE
            IcdEntryRefType icdEntryRef = cxxDiagnosis.getIcdEntryRef();
            // CXX: DIAG_DIAG_DATE
            DateType date = cxxDiagnosis.getDate();
            List<GTDSTumourEIType> tumourList = cxxDiagnosis.getTumour();

            //TODO
            addTumourList(tumourList, cxxDiagnosisNode, adtDiagnosisNode);

        }

    }


    private void addTumourList (List<GTDSTumourEIType> tumours, Node<DiagnosisType> cxxDiagnosisNode, Node<ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung.Diagnose> adtdiagnoseNode){

        Node<ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung> meldungNode = adtdiagnoseNode.getParentNode(ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung.class);
        ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung meldung = meldungNode.getElement();
        ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung.Tumorzuordnung tumorzuordnung = getTumourzuordnung(meldung);

        if (tumours != null && tumours.size() > 0){
            // ADT: Diagnose can have only one tumour: We take only the first node
            GTDSTumourEIType tumour = tumours.get(0);
            ConverterManager.convertAndAdd(tumour, cxxDiagnosisNode, tumorzuordnung, meldungNode, tumourConverter);
        }


    }

    private ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung.Tumorzuordnung getTumourzuordnung (ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung meldung){

        ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung.Tumorzuordnung tumorzuordnung = meldung.getTumorzuordnung();
        if (tumorzuordnung == null){

            tumorzuordnung = new ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung.Tumorzuordnung();
            meldung.setTumorzuordnung(tumorzuordnung);

        }

        return tumorzuordnung;

    }

}
