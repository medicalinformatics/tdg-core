package dktk.testdatengenerator.converter.cxxtoadt;

import de.gekid.namespace.ADTGEKID;
import de.kairos_med.*;
import dktk.testdatengenerator.converter.general.Converter;
import dktk.testdatengenerator.converter.general.ConverterManager;
import dktk.testdatengenerator.converter.general.Node;

import java.util.List;

public class PatientDataSetConverter implements Converter<PatientDataSetType, ADTGEKID.MengePatient.Patient> {


    private DiagnosisConverter diagnosisConverter = new DiagnosisConverter();

    @Override
    public void convertAndAdd(Node<PatientDataSetType> centraxxPatientNode, Node<ADTGEKID.MengePatient.Patient> adtPatientNode) {

        addDiagnosisList(centraxxPatientNode, adtPatientNode);
        addMasterdata(centraxxPatientNode, adtPatientNode);
        addIdContainer(centraxxPatientNode, adtPatientNode);
        addSampleData(centraxxPatientNode, adtPatientNode);

    }

    private void addDiagnosisList(Node<PatientDataSetType> centraxxPatientNode, Node<ADTGEKID.MengePatient.Patient> adtPatientNode){

        PatientDataSetType centraxxPatient = centraxxPatientNode.getElement();
        List<DiagnosisType> diagnosisList = centraxxPatient.getDiagnosis();
        ConverterManager.convertAndAdd(diagnosisList, centraxxPatientNode, () -> createDiagnosisNodeAndAddToPatient(adtPatientNode), diagnosisConverter);

    }

    private Node<ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung.Diagnose> createDiagnosisNodeAndAddToPatient(Node<ADTGEKID.MengePatient.Patient> patientNode){

        ADTGEKID.MengePatient.Patient patient = patientNode.getElement();
        ADTGEKID.MengePatient.Patient.MengeMeldung mengeMeldung = patient.getMengeMeldung();
        List<ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung> meldungList = mengeMeldung.getMeldung();
        ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung meldung = new ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung();
        meldungList.add(meldung);
        Node<ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung> meldungNode = new Node<>(patientNode, meldung);

        return createDiagnosisNodeAndAddToMeldung(meldungNode);

    }

    private Node<ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung.Diagnose> createDiagnosisNodeAndAddToMeldung(Node<ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung> meldungNode){

        ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung.Diagnose diagnose = new ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung.Diagnose();
        ADTGEKID.MengePatient.Patient.MengeMeldung.Meldung meldung = meldungNode.getElement();
        meldung.setDiagnose(diagnose);

        return new Node<>(meldungNode, diagnose);

    }

    private void addMasterdata (Node<PatientDataSetType> centraxxPatientNode, Node<ADTGEKID.MengePatient.Patient> adtPatientNode){

        //TODO

    }

    private void addIdContainer (Node<PatientDataSetType> centraxxPatientNode, Node<ADTGEKID.MengePatient.Patient> adtPatientNode){

        //TODO

    }

    private void addSampleData (Node<PatientDataSetType> centraxxPatientNode, Node<ADTGEKID.MengePatient.Patient> adtPatientNode){

        //TODO

    }

}
