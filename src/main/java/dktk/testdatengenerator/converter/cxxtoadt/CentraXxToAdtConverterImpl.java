package dktk.testdatengenerator.converter.cxxtoadt;

import de.gekid.namespace.ADTGEKID;
import de.kairos_med.CentraXXDataExchange;
import de.kairos_med.EffectDataType;
import de.kairos_med.FlexibleDataSetInstanceType;
import de.kairos_med.PatientDataSetType;
import dktk.testdatengenerator.converter.general.ConverterManager;
import dktk.testdatengenerator.converter.general.Node;

import java.util.List;

public class CentraXxToAdtConverterImpl implements CentraXxToAdtConverter {

    private PatientDataSetConverter patientDataSetConverter = new PatientDataSetConverter();


    @Override
    public ADTGEKID convert(CentraXXDataExchange centraXXDataExchange) {

        ADTGEKID adt = new ADTGEKID();

        if (centraXXDataExchange != null){

            EffectDataType effectData = centraXXDataExchange.getEffectData();
            adt = addEffectData(adt, effectData);

        }


        return adt;

    }

    private ADTGEKID addEffectData (ADTGEKID adt, EffectDataType effectData){

        if (effectData != null){

            List<PatientDataSetType> patientDataSets = effectData.getPatientDataSet();
            List<FlexibleDataSetInstanceType> flexibleDataSets = effectData.getFlexibleDataSetInstance();

            addPatientDataSets(adt, patientDataSets);
            addFlexibleDataSets(adt, flexibleDataSets);

        }

        return adt;

    }


    private void addPatientDataSets (ADTGEKID adt, List<PatientDataSetType> patientDataSets){

        ADTGEKID.MengePatient mengePatient = adt.getMengePatient();
        ConverterManager.convertAndAdd(patientDataSets, () -> createPatientAndAddToMengePatient(mengePatient), patientDataSetConverter);

    }

    private ADTGEKID.MengePatient.Patient createPatientAndAddToMengePatient(ADTGEKID.MengePatient mengePatient){

        ADTGEKID.MengePatient.Patient patient = new ADTGEKID.MengePatient.Patient();;
        List<ADTGEKID.MengePatient.Patient> patientList = mengePatient.getPatient();
        patientList.add(patient);

        return patient;

    }

    private void addFlexibleDataSets (ADTGEKID adt, List<FlexibleDataSetInstanceType> flexibleDataSets){

        for (FlexibleDataSetInstanceType flexibleDataSet : flexibleDataSets){
            addFlexibleDataSet(adt, flexibleDataSet);
        }

    }

    private void addFlexibleDataSet (ADTGEKID adt, FlexibleDataSetInstanceType flexibleDataSet){

        //TODO

    }


}
