package dktk.testdatengenerator.converter.general;

import java.util.function.Supplier;

public class NodeGetterImpl<T> implements NodeGetter<T> {

    private Node<T> node;

    public NodeGetterImpl(Node<T> node) {
        this.node = node;
    }

    public NodeGetterImpl(T element) {
        this (null, element);
    }

    public NodeGetterImpl(Node parentNode, T element) {
        this.node = (parentNode != null) ? new Node<>(parentNode, element) : new Node<>(element);
    }

    public NodeGetterImpl(Supplier<T> supplier) {
        this (null, supplier);
    }

    public NodeGetterImpl(Node parentNode, Supplier<T> supplier) {
        this.node = (parentNode != null) ? new Node<>(parentNode, supplier.get()) : new Node<>(supplier.get());
    }

    @Override
    public Node<T> getNode() {
        return null;
    }

}
