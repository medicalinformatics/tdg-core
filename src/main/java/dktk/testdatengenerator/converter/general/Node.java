package dktk.testdatengenerator.converter.general;

public class Node<T> {

    private Node parent;
    private T element;

    public Node(T element) {
        this.element = element;
    }

    public Node(Node parent, T element) {
        this.parent = parent;
        this.element = element;
    }

    public T getElement(){
        return element;
    }

    protected Node getParent(){
        return parent;
    }

    public <PARENT_TYPE> Node<PARENT_TYPE> getParentNode(Class<PARENT_TYPE> myClass){

        Node parentNode = parent;
        while (parentNode != null && !myClass.isInstance(parentNode.getElement())){
            parentNode = parentNode.getParent();
        }

        return parentNode;

    }

    public <PARENT_TYPE> PARENT_TYPE getParent(Class<PARENT_TYPE> myClass){

        Node<PARENT_TYPE> parentNode = getParentNode(myClass);
        return (parentNode != null) ? (PARENT_TYPE) parentNode.getElement() : null;

    }

}
