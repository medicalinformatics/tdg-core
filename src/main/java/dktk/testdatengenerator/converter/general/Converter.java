package dktk.testdatengenerator.converter.general;

public interface Converter<SOURCE_TYPE, TARGET_TYPE> {

    public void convertAndAdd(Node<SOURCE_TYPE> sourceElementNode, Node<TARGET_TYPE> targetElementNode);

}
