package dktk.testdatengenerator.converter.general;

public interface NodeGetter<T>{
    public Node<T> getNode();
}
