package dktk.testdatengenerator.converter.general;

import java.util.Collection;
import java.util.function.Supplier;

public class ConverterManager {


    public static <SOURCE_TYPE, TARGET_TYPE> void convertAndAdd(SOURCE_TYPE source, TARGET_TYPE target, Converter<SOURCE_TYPE, TARGET_TYPE> converter){
        converter.convertAndAdd(new NodeGetterImpl<>(source).getNode(), new NodeGetterImpl<>(target).getNode());
    }

    public static <SOURCE_TYPE, SOURCE_PARENT_TYPE, TARGET_TYPE, TARGET_PARENT_TYPE> void convertAndAdd(SOURCE_TYPE source, Node<SOURCE_PARENT_TYPE> sourceParentNode, TARGET_TYPE target, Node<TARGET_PARENT_TYPE> targetParentNode, Converter<SOURCE_TYPE, TARGET_TYPE> converter){
        converter.convertAndAdd(new NodeGetterImpl<>(sourceParentNode, source).getNode(), new NodeGetterImpl<>(targetParentNode, target).getNode());
    }

    public static <SOURCE_TYPE, TARGET_TYPE> void convertAndAdd(NodeGetter<SOURCE_TYPE> sourceNodeGetter, NodeGetter<TARGET_TYPE> targetNodeGetter, Converter<SOURCE_TYPE, TARGET_TYPE> converter){
        converter.convertAndAdd(sourceNodeGetter.getNode(), targetNodeGetter.getNode());
    }

    public static <SOURCE_TYPE, TARGET_TYPE> void convertAndAdd(SOURCE_TYPE source, Supplier<TARGET_TYPE> targetSupplier, Converter<SOURCE_TYPE, TARGET_TYPE> converter){
        converter.convertAndAdd(new NodeGetterImpl<>(source).getNode(), new NodeGetterImpl<>(targetSupplier).getNode());
    }

    public static <SOURCE_TYPE, TARGET_TYPE, TARGET_PARENT_TYPE> void convertAndAdd(SOURCE_TYPE source, Node<TARGET_PARENT_TYPE> targetParentNode, Supplier<TARGET_TYPE> targetSupplier, Converter<SOURCE_TYPE, TARGET_TYPE> converter){
        converter.convertAndAdd(new NodeGetterImpl<>(source).getNode(), new NodeGetterImpl<>(targetParentNode, targetSupplier).getNode());
    }

    public static <SOURCE_TYPE, TARGET_TYPE, TARGET_PARENT_TYPE> void convertAndAdd(Node<SOURCE_TYPE> sourceNode, Node<TARGET_PARENT_TYPE> targetParentNode, Supplier<TARGET_TYPE> targetSupplier, Converter<SOURCE_TYPE, TARGET_TYPE> converter){
        converter.convertAndAdd(sourceNode, new NodeGetterImpl<>(targetParentNode, targetSupplier).getNode());
    }

    public static <SOURCE_TYPE, TARGET_TYPE, TARGET_PARENT_TYPE> void convertAndAdd(Collection<Node<SOURCE_TYPE>> sourceNodes, Node<TARGET_PARENT_TYPE> targetParentNode, Supplier<TARGET_TYPE> targetSupplier, Converter<SOURCE_TYPE, TARGET_TYPE> converter){

        for (Node<SOURCE_TYPE> sourceNode : sourceNodes){
            convertAndAdd(sourceNode, targetParentNode, targetSupplier, converter);
        }

    }

    public static <SOURCE_TYPE, SOURCE_PARENT_TYPE, TARGET_TYPE, TARGET_PARENT_TYPE> void convertAndAdd(Collection<SOURCE_TYPE> sources, Node<SOURCE_PARENT_TYPE> sourceParentNode, Node<TARGET_PARENT_TYPE> targetParentNode, Supplier<TARGET_TYPE> targetSupplier, Converter<SOURCE_TYPE, TARGET_TYPE> converter){

        for (SOURCE_TYPE source : sources){
            convertAndAdd(new NodeGetterImpl<>(sourceParentNode, source).getNode(), targetParentNode, targetSupplier, converter);
        }

    }

    public static <SOURCE_TYPE, SOURCE_PARENT_TYPE, TARGET_TYPE, TARGET_PARENT_TYPE> void convertAndAdd(Collection<SOURCE_TYPE> sources, Node<SOURCE_PARENT_TYPE> sourceParentNode, NodeGetter<TARGET_TYPE> targetNodeGetter, Converter<SOURCE_TYPE, TARGET_TYPE> converter){

        for (SOURCE_TYPE source : sources){
            convertAndAdd(new NodeGetterImpl<>(sourceParentNode, source), targetNodeGetter, converter);
        }

    }

    public static <SOURCE_TYPE, TARGET_TYPE, TARGET_PARENT_TYPE> void convertAndAdd(Collection<SOURCE_TYPE> sources, Supplier<TARGET_TYPE> targetSupplier, Converter<SOURCE_TYPE, TARGET_TYPE> converter){

        for (SOURCE_TYPE source : sources){
            convertAndAdd(new NodeGetterImpl<>(source).getNode(), null, targetSupplier, converter);
        }

    }

}
