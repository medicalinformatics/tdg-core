package dktk.testdatengenerator.util;

import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.PermissibleValue;
import de.samply.common.mdrclient.domain.Validations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class MdrValidationConverter {

    private final static String MDR_BASE_URL = "https://mdr.ccpit.dktk.dkfz.de/v3/api/mdr/";
    private MdrClient mdrClient;
    public MdrValidationConverter() {
        mdrClient = new MdrClient(MDR_BASE_URL);
    }

    /**
     * This method creates a List which contains the different possible validations for a given MDRDataElement.
     * Note: At the moment only possible for Enumerated values e.g. urn:dktk:dataelement:90:1
     * @param dataElement
     * @param languageCode
     * @return
     * @throws
     */
    public List<String> getValidationListForEnum(String dataElement, String languageCode) throws ExecutionException, MdrConnectionException, MdrInvalidResponseException {
        Validations dataElementValidations = mdrClient.getDataElementValidations(dataElement, languageCode);
        List<PermissibleValue> permissibleValues = dataElementValidations.getPermissibleValues();
        List<String> result = permissibleValues
                .stream()
                .map(PermissibleValue::getValue)
                .collect(Collectors.toList());
        return result;
    }

    /**
     * This method creates a List of Strings (only the Regular Expressions) which are valid for the specific dataelement.
     * @param dataElement
     * @param languageCode
     * @return
     * @throws ExecutionException
     * @throws MdrConnectionException
     * @throws MdrInvalidResponseException
     */
    public List<String> getValidationListForRegex(String dataElement, String languageCode) throws ExecutionException, MdrConnectionException, MdrInvalidResponseException {
        Validations dataElementValidations = mdrClient.getDataElementValidations(dataElement, languageCode);
        String regexExpression = dataElementValidations.getValidationData();
        List<String> expressions = new ArrayList<>();
        //String fixedExpression = regexExpression.replace("\\", "\\\\");
        expressions.add(regexExpression);
        return expressions;
    }
}

