package dktk.testdatengenerator.util;

import com.mifmif.common.regex.Generex;
import dktk.testdatengenerator.db.model.TDGProfile;
import org.apache.commons.lang3.RandomStringUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;


/**
 * Created by brennert on 10.07.2017.
 */
public class RandomOperations {

    private TDGProfile tdgProfile;

    public RandomOperations(TDGProfile tdgProfile) {
        this.tdgProfile = tdgProfile;
    }

    /**
     * Takes a random element from Enum while respecting the probabilities in database
     *
     * @param enumType
     * @param prefix
     * @param suffix
     * @param <T>
     * @return
     */
    public <T extends Enum<T>> T getRandomFromEnum(Class<T> enumType, String prefix, String suffix) {
        double randomDouble = ThreadLocalRandom.current().nextDouble(0.0, 1.0);
        double probabilitySum = tdgProfile.querySum(enumType, "p" + prefix, suffix);
        double probabilitiesSum = 0.0;
        for (T element : enumType.getEnumConstants()) {
            double probabilityForElement = tdgProfile.getProbability("p" + prefix + element + suffix).getProbability();
            probabilitiesSum += probabilityForElement / probabilitySum;
            if (probabilitiesSum >= randomDouble) {
                return element;
            }
        }
        System.out.println("getRandomFromEnum returns null. Probabilities sum is: " + probabilitiesSum + " for prefix " + prefix + " and enum " + enumType + " and suffix " + suffix);
        return null;
    }

    /**
     * Takes a Random Element from the given Enum. In this Method each Element has the same probability to be selected.
     *
     * @param enumType
     * @param <T>
     * @return
     */
    public <T extends Enum<T>> T getRandomFromEnum(Class<T> enumType) {
        return getRandomFromEnum(enumType, null);
    }

    public <T extends Enum<T>> T getRandomFromEnum(Class<T> enumType, List<T> excluded) {
        List<T> enumList = new LinkedList<>(Arrays.asList(enumType.getEnumConstants()));
        if (excluded != null){
            for (T element : excluded){
                if (enumList.contains(element)) {
                    enumList.remove(element);
                }
            }
        }

        return getRandomFromArrayList(enumList);
    }

    /**
     * This method returns a Random Element from the ArrayList
     * Returns {@code null} then the arrayList size is smaller or equal to 0.
     *
     * @param list
     * @param <T>
     * @return
     */
    public <T> T getRandomFromArrayList(List<T> list) {
        if (list.size() <= 0)
            return null;
        return list.get(ThreadLocalRandom.current().nextInt(0, list.size()));
    }

    /**
     * This Method returns true or false in respect of a given probability for true
     *
     * @param probabilityName the Probability for true
     * @return boolean
     */
    public boolean getBinarRandomFromProbabilty(String probabilityName) {
        double randomNumber = ThreadLocalRandom.current().nextDouble(0, 1);
        double probability = tdgProfile.getProbability(probabilityName).getProbability();
        return randomNumber <= probability;
    }

    /**
     * This method should return random Strings for a given Regex.
     * Note: More information about the generation of the random strings at: https://code.google.com/archive/p/xeger/
     *
     * @param regex a regular Expression
     * @return String randomString created from Regex
     */
    public String generateRandomStringForRegex(String regex) {
        Generex generex = new Generex(regex);
        String randomString = generex.random();
        return randomString;
    }

    public BigDecimal generateRandomBigDecimal (int min, int max){

        BigDecimal bmin = new BigDecimal(min);
        BigDecimal bmax = new BigDecimal(max);

        return generateRandomBigDecimal(bmin, bmax);

    }

    public BigDecimal generateRandomBigDecimal (BigDecimal min, BigDecimal max){

        BigDecimal randomBigDecimal = min.add(new BigDecimal(Math.random()).multiply(max.subtract(min)));
        return randomBigDecimal.setScale(2,BigDecimal.ROUND_HALF_UP);

    }

    public int generateInteger (int min, int max){
        return ThreadLocalRandom.current().nextInt(min, max);
    }

    public BigInteger generateBigInteger(){

        double min = 100000000000000000000d;
        double max = 999999999999999999999d;

        return generateRandomBigDecimal(new BigDecimal(min),new BigDecimal(max)).toBigInteger();

    }

    public boolean throwACoin(){
        return ThreadLocalRandom.current().nextBoolean();
    }


    public int generateNumberOfItemsToBeGenerated (String minDb, String maxDb){

        Double min = tdgProfile.getProbability(minDb).getProbability();
        Double max = tdgProfile.getProbability(maxDb).getProbability();

        return (min != null && max != null) ?
                generateInteger(min.intValue(), max.intValue()) : 2;

    }

    public String generateRandomAlphaNumeric (int size){
        return RandomStringUtils.randomAlphanumeric(size);
    }

}
