package dktk.testdatengenerator.util;

import de.kairos_med.GtdsDictionaryRefType;
import de.kairos_med.GtdsVersionEnumType;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import dktk.testdatengenerator.db.model.TDGProfile;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class MdrUtils {

    private MdrValidationConverter mdrValidationConverter = new MdrValidationConverter();
    private RandomOperations randomOperations;
    private String languageCode = "de";

    public MdrUtils(TDGProfile tdgProfile){
        this.randomOperations = new RandomOperations(tdgProfile);;
    }

    public String generateMdrEnumeratedValue(String mdrId){

        try {
            return generateMdrEnumeratedValue_WithoutManagementException(mdrId);
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        } catch (MdrConnectionException e) {
            e.printStackTrace();
            return null;
        } catch (MdrInvalidResponseException e) {
            e.printStackTrace();
            return null;
        }

    }

    private String generateMdrEnumeratedValue_WithoutManagementException (String mdrId) throws ExecutionException, MdrConnectionException, MdrInvalidResponseException {
        List<String> validationList = mdrValidationConverter.getValidationListForEnum(mdrId, languageCode);

        String result = null;

        for (int i= 0; i<10; i++) {
            result = randomOperations.getRandomFromArrayList(validationList);
            if (result != null && result.length() > 0)
                break;
        }

        return result;

    }

    public GtdsDictionaryRefType generateMdrEnumeratedValueInCentraxxFormat(String mdrId){

        GtdsDictionaryRefType gtdsDictionaryRefType = new GtdsDictionaryRefType();
        String mdrEnumeratedValue = generateMdrEnumeratedValue(mdrId);

        gtdsDictionaryRefType.setCode(mdrEnumeratedValue);
        gtdsDictionaryRefType.setVersion(GtdsVersionEnumType.USERDEFINED);

        return gtdsDictionaryRefType;

    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

}
