package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.*;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.exceptions.DateGeneratorException;
import dktk.testdatengenerator.util.RandomOperations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProgressGenerator {

    private RandomOperations randomOperations;
    private SystemTherapyGenerator systemTherapyGenerator;
    private RadiationTherapyGenerator radiationTherapyGenerator;
    private EpisodeGenerator episodeGenerator;
    private SurgeryGenerator surgeryGenerator;
    private DateGenerator dateGenerator;
    private IdGenerator idGenerator;

    private List<TherapyDoneGTDSEnumType> excludedTherapyDoneGTDSEnum = Arrays.asList(TherapyDoneGTDSEnumType.X);
    private List<FullAssessmentGTDSEnumType> excludedFullAssessmentGTDSEnum = Arrays.asList(FullAssessmentGTDSEnumType.E, FullAssessmentGTDSEnumType.F, FullAssessmentGTDSEnumType.M, FullAssessmentGTDSEnumType.O);

    // TODO : Kairos? U, N, P
    private List<StatePrimaryGTDSEnumType> primaryAssessmentList = Arrays.asList(new StatePrimaryGTDSEnumType[]{StatePrimaryGTDSEnumType.K, StatePrimaryGTDSEnumType.T, StatePrimaryGTDSEnumType.R, StatePrimaryGTDSEnumType.F});

    // TODO : Kairos? U, N, P, X15
    private List<StateLymphNodeGTDSEnumType> primaryAssessmentLymphList = Arrays.asList(new StateLymphNodeGTDSEnumType[]{StateLymphNodeGTDSEnumType.B, StateLymphNodeGTDSEnumType.E, StateLymphNodeGTDSEnumType.F, StateLymphNodeGTDSEnumType.K, StateLymphNodeGTDSEnumType.R, StateLymphNodeGTDSEnumType.T, StateLymphNodeGTDSEnumType.X});
    // TODO : Kairos? P, N, M, T, U
    private List<StateMetastasisGTDSEnumType> metastasisAssessmentList = Arrays.asList(new StateMetastasisGTDSEnumType[]{StateMetastasisGTDSEnumType.K, StateMetastasisGTDSEnumType.R, StateMetastasisGTDSEnumType.F });


    public ProgressGenerator(TDGProfile tdgProfile) {

        randomOperations = new RandomOperations(tdgProfile);
        systemTherapyGenerator = new SystemTherapyGenerator(tdgProfile);
        radiationTherapyGenerator = new RadiationTherapyGenerator(tdgProfile);
        episodeGenerator = new EpisodeGenerator(tdgProfile);
        surgeryGenerator = new SurgeryGenerator(tdgProfile);
        idGenerator = new IdGenerator(tdgProfile);
        dateGenerator = new DateGenerator();

    }


    public GTDSTumourEIType generateProgressListAndAddToTumour(GTDSTumourEIType tumour, PatientDataSetType patient) {

        if (tumour != null) {

            List<GTDSProgressEIType> progressList = tumour.getProgress();
            ArrayList<EpisodeType> episodeList = episodeGenerator.generate(patient);

            for (EpisodeType episode : episodeList){
                GTDSProgressEIType progress = generateProgress(tumour, episode);
                progressList.add(progress);
            }

        }

        return tumour;

    }

    private GTDSProgressEIType generateProgress (GTDSTumourEIType tumour, EpisodeType episode){

        GTDSProgressEIType progress = new GTDSProgressEIType();


        String progressId = generateProgressId();
        FullAssessmentGTDSEnumType fullAssessment = generateFullAssessment();
        DateType examinationDate = generateExaminationDate(episode);

        StatePrimaryGTDSEnumType primaryAssessment = generatePrimaryAssessment();
        StateMetastasisGTDSEnumType metastasisAssessment = generateMetastasisAssessment();
        StateLymphNodeGTDSEnumType assementLymph = generateAssementLymph();


        TherapyDoneGTDSEnumType radiation = generateRadiation();
        TherapyDoneGTDSEnumType kmt = generateKmt();
        TherapyDoneGTDSEnumType surgery = generateSurgery();
        TherapyDoneGTDSEnumType chemo = generateChemo();
        TherapyDoneGTDSEnumType hormone = generateHormone();
        TherapyDoneGTDSEnumType immune = generateImmune();


        progress.setProgressID(progressId);
        progress.setTherapyStart(episode.getValidFrom());
        progress.setTherapyEnd(episode.getValidUntil());

        // CXX: PROGRESS_FULLASSESSMENT
        progress.setFullAssessment(fullAssessment);
        // CXX: PROGRESS_ASSESSMENTPRIMARY
        progress.setAssessmentPrimary(primaryAssessment);
        // CXX: PROGRESS_ASSESSMENTLYMPH
        progress.setAssessmentLymph(assementLymph);
        // CXX: PROGRESS_ASSESSMENTMETA
        progress.setAssessmentMeta(metastasisAssessment);
        // CXX: PROGRESS_EXAMINATIONDATE
        progress.setExaminationDate(examinationDate);

        // CXX: PROGRESS_CHEMO
        progress.setChemo(chemo);
        // CXX: PROGRESS_HORMONE
        progress.setHormone(hormone);
        // CXX: PROGRESS_IMMUNE
        progress.setImmune(immune);
        // CXX: PROGRESS_KMT
        progress.setKMT(kmt);
        // CXX: PROGRESS_RADIATION
        progress.setRadiation(radiation);
        //CXX: PROGRESS_SURGERY
        progress.setSurgery(surgery);

        if (chemo == TherapyDoneGTDSEnumType.J)  {
            systemTherapyGenerator.generateTherapiesAndAddToProgress(progress, episode);
        }
        if (hormone == TherapyDoneGTDSEnumType.J ){
            systemTherapyGenerator.generateTherapiesAndAddToProgress(progress, episode);
        }
        if (immune == TherapyDoneGTDSEnumType.J){
            systemTherapyGenerator.generateTherapiesAndAddToProgress(progress, episode);
        }
        if (kmt == TherapyDoneGTDSEnumType.J){
            systemTherapyGenerator.generateTherapiesAndAddToProgress(progress, episode);
        }
        if (radiation == TherapyDoneGTDSEnumType.J) {
            radiationTherapyGenerator.generateRadiationTherapiesAndAddToProgress(progress, episode);
        }
        if (surgery == TherapyDoneGTDSEnumType.J){
            surgeryGenerator.generateSurgeriesAndAddToProgress(progress);
        }

        return progress;

    }

    private StateLymphNodeGTDSEnumType generateAssementLymph(){
        return randomOperations.getRandomFromArrayList(primaryAssessmentLymphList);
    }

    private String generateProgressId(){
        return idGenerator.generateId();
    }

    private GtdsDictionaryRefType generateMetastasisAssessmentRef(){

        GtdsDictionaryRefType gtdsDictionaryRefType = new GtdsDictionaryRefType();

        StateMetastasisGTDSEnumType stateMetastasis = generateMetastasisAssessment();
        gtdsDictionaryRefType.setCode(stateMetastasis.value());
        gtdsDictionaryRefType.setVersion(GtdsVersionEnumType.USERDEFINED);

        return gtdsDictionaryRefType;

    }

    private TherapyDoneGTDSEnumType generateSurgery(){
        return generateTherapyDoneGTDSEnum();
    }

    private TherapyDoneGTDSEnumType generateChemo(){
        return generateTherapyDoneGTDSEnum();
    }

    private TherapyDoneGTDSEnumType generateHormone(){
        return generateTherapyDoneGTDSEnum();
    }

    private TherapyDoneGTDSEnumType generateImmune(){
        return generateTherapyDoneGTDSEnum();
    }

    private TherapyDoneGTDSEnumType generateRadiation(){
        return generateTherapyDoneGTDSEnum();
    }

    private TherapyDoneGTDSEnumType generateKmt(){
        return generateTherapyDoneGTDSEnum();
    }

    private TherapyDoneGTDSEnumType generateTherapyDoneGTDSEnum(){
        return randomOperations.getRandomFromEnum(TherapyDoneGTDSEnumType.class, excludedTherapyDoneGTDSEnum);
    }

    private FullAssessmentGTDSEnumType generateFullAssessment(){
        return randomOperations.getRandomFromEnum(FullAssessmentGTDSEnumType.class, excludedFullAssessmentGTDSEnum);
    }

    private StatePrimaryGTDSEnumType generatePrimaryAssessment(){
        return randomOperations.getRandomFromArrayList(primaryAssessmentList);
    }

    private StateMetastasisGTDSEnumType generateMetastasisAssessment(){
        return randomOperations.getRandomFromArrayList(metastasisAssessmentList);
    }

    private DateType generateExaminationDate (EpisodeType episode){

        try {
            return generateExaminationDate_WithoutManagementException(episode);
        } catch (DateGeneratorException e) {
            e.printStackTrace();
            return null;
        }

    }

    private DateType generateExaminationDate_WithoutManagementException(EpisodeType episode) throws DateGeneratorException {

        //TODO
        DateType validFrom = episode.getValidFrom();
        DateType validUntil = episode.getValidUntil();

        return dateGenerator.generateDateBetweenWithNoBounds(validFrom, validUntil);

    }


}
