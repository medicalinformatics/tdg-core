package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.*;
import dktk.testdatengenerator.customelements.Patient;

import java.util.List;

public class CentraXXGenerator {

    private CatalogueDataGenerator catalogueDataGenerator = new CatalogueDataGenerator();

    public CentraXXDataExchange generate (List<Patient> patients){

        CentraXXDataExchange centraXXDataExchange = new CentraXXDataExchange();

        EffectDataType effectDataType = generateEffectData(patients);
        String source = generateSource();
        CatalogueDataType catalogueData = generateCatalogueData();

        centraXXDataExchange.setEffectData(effectDataType);
        centraXXDataExchange.setSource(source);
        centraXXDataExchange.setCatalogueData(catalogueData);

        return centraXXDataExchange;

    }

    private String generateSource (){
        return EntitySourceEnumType.XMLIMPORT.value();
    }

    private EffectDataType generateEffectData (List<Patient> patients){

        EffectDataType effectDataType = new EffectDataType();
        List<PatientDataSetType> patientDataSetList = effectDataType.getPatientDataSet();
        List<FlexibleDataSetInstanceType> flexibleDataSetInstanceList = effectDataType.getFlexibleDataSetInstance();

        for (Patient patient : patients){
            patientDataSetList.add(patient.getPatientDataSet());
            flexibleDataSetInstanceList.addAll(patient.getPatientAdditionalInfo().getAllFlexibleDataSetInstances());
        }

        return effectDataType;

    }

    private CatalogueDataType generateCatalogueData (){
        return catalogueDataGenerator.generateCatalogData();
    }

}
