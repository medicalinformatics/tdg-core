package dktk.testdatengenerator.generator.centraxx;

import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.util.RandomOperations;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

public class IdGenerator {

    private RandomOperations randomOperations;
    private Set<String> idList = new HashSet<>();

    public IdGenerator(TDGProfile tdgProfile) {
        this.randomOperations = new RandomOperations(tdgProfile);
    }

    public String generateId(){
        return generateId(() -> randomOperations.generateBigInteger().toString());
    }

    public String generateAlphanumericId(int size){
        return generateId(() -> randomOperations.generateRandomAlphaNumeric(size));
    }


    private String generateId(Supplier<String> generator){

        String id = null;
        while (id == null) {

            String tempId = generator.get();
            boolean isContained = idList.contains(tempId);
            if (!isContained){
                idList.add(tempId);
                id = tempId;
            }

        }

        return id;

    }


}
