package dktk.testdatengenerator.generator.centraxx;

//import com.sun.org.apache.xerces.internal.jaxp.datatype.DatatypeFactoryImpl;
import de.kairos_med.*;
import dktk.testdatengenerator.customelements.Patient;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.exceptions.DateGeneratorException;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by brennert on 26.07.2017.
 */
public class DKTKConsentGenerator {
    private DateGenerator dateGenerator;
    private TDGProfile tdgProfile;
    private IdGenerator idGenerator;

    public DKTKConsentGenerator(TDGProfile tdgProfile) {
        this.tdgProfile = tdgProfile;
        this.dateGenerator = new DateGenerator();
        this.idGenerator = new IdGenerator(tdgProfile);
    }

    public Patient generateDktkFlagAndAddToPatient (Patient patient, DiagnosisType diagnosis) {

        if ( patient != null) {

            FlexiFlagItemType dktkFlag = generateDktkFlag(patient, diagnosis);
            if (dktkFlag != null) {

                patient.getPatientDataSet().getFlexiFlagItem().add(dktkFlag);
                patient = generateDktkIdIfRequiredAndAddToPatient(patient, dktkFlag);

            }

        }

        return patient;
    }

    public FlexiFlagItemType generateDktkFlag(Patient patient, DiagnosisType diagnosis){

        FlexiFlagItemType dktkFlag = new FlexiFlagItemType();

        dktkFlag.setDefinitionRef("DKTK");
        dktkFlag.setPatientScope(ThreadLocalRandom.current().nextDouble(0, 1) <= tdgProfile.getProbability("pPatientScope").getProbability());

        if (dktkFlag != null) {

            setDKTKFlagValidityTime(dktkFlag, diagnosis);

        }

        return dktkFlag;

    }

    private  Patient generateDktkIdIfRequiredAndAddToPatient(Patient patient, FlexiFlagItemType dktkFlag){

        if (dktkFlag != null && dktkFlag.isPatientScope()){

            FlexibleIDType dktkId = generateDktkId();

            IDContainerType idContainer = patient.getPatientDataSet().getIDContainer();
            if (idContainer == null){
                idContainer = new IDContainerType();
                patient.getPatientDataSet().setIDContainer(idContainer);
            }

            List<FlexibleIDType> flexibleIDList = idContainer.getFlexibleID();

            flexibleIDList.add(dktkId);

        }

        return patient;
    }

    private FlexibleIDType generateDktkId (){

        // Add Sample ID to Patient Data Set
        String patientId = idGenerator.generateAlphanumericId(8);
        patientId = patientId.toUpperCase();

        FlexibleIDType flexibleIDType = new FlexibleIDType();
        flexibleIDType.setKey("DKTK");
        flexibleIDType.setValue(patientId);

        return flexibleIDType;

    }


    private void setDKTKFlagValidityTime(FlexiFlagItemType dktkFlag, DiagnosisType diagnosis) {

        int countDays = ThreadLocalRandom.current().nextInt((int) tdgProfile.getProbability("dktkConsentValidityMin").getProbability(), (int) tdgProfile.getProbability("dktkConsentValidityMax").getProbability());

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.add(GregorianCalendar.DAY_OF_MONTH, -1 * countDays);
        Date currentTimeMinusCountDays = gregorianCalendar.getTime();

        gregorianCalendar = diagnosis.getDate().getDate().toGregorianCalendar();
        Date diagnosisDate = gregorianCalendar.getTime();

        if(Math.signum((currentTimeMinusCountDays.getTime()-diagnosisDate.getTime())) == -1.0f)
            currentTimeMinusCountDays.setTime(diagnosisDate.getTime()+((countDays* 86400000)));

        DateType validFrom = new DateType();
        try {
            validFrom = dateGenerator.generateDateBetweenWithNoBounds(diagnosisDate, currentTimeMinusCountDays);
        } catch (DateGeneratorException dge) {
            System.out.println("This is diagnosis date: " + diagnosisDate + " And this is currentTimeMinusCountDays: " + currentTimeMinusCountDays);
            dge.printStackTrace();
        }

        gregorianCalendar = validFrom.getDate().toGregorianCalendar();
        gregorianCalendar.add(GregorianCalendar.DAY_OF_MONTH, countDays);
        DateType validUntil = new DateType();
        try {
            validUntil.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
        } catch (DatatypeConfigurationException dce){
            System.out.println("Couldnt convertAndAdd validUntil date of DKTK Consent to datetype");
            dce.printStackTrace();
        }
        validUntil.setPrecision(validFrom.getPrecision());

        dktkFlag.setValidFrom(validFrom);
        // dktkFlag.setValidUntil(validUntil);

    }
}
