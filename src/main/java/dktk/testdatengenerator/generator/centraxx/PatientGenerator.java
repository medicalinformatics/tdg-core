package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.*;
import dktk.testdatengenerator.customelements.Patient;
import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.db.model.TDGProfile;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * This class is responsible for generating a List of PatientDataSetTypes
 * Created by brennert on 04.07.2017.
 */
public class PatientGenerator {

    private DKTKConsentGenerator dktkConsentGenerator;
    private TDGProfile tdgProfile;
    private MasterDataGenerator masterDataGenerator;
    private DiagnosisGenerator diagnosisGenerator;
    private EpisodeGenerator episodeGenerator;
    private SampleGenerator sampleGenerator;
    private IdGenerator idGenerator;


    public PatientGenerator(TDGProfile tdgProfile, DBConnector dbConnector ) {
        this.tdgProfile = tdgProfile;
        masterDataGenerator = new MasterDataGenerator(tdgProfile, dbConnector);
        diagnosisGenerator = new DiagnosisGenerator(tdgProfile, dbConnector);
        dktkConsentGenerator = new DKTKConsentGenerator(tdgProfile);
        sampleGenerator = new SampleGenerator(tdgProfile);
        episodeGenerator = new EpisodeGenerator(tdgProfile);
        idGenerator = new IdGenerator(tdgProfile);
    }

    /**
     * @return
     * @see PatientGenerator#generate(int)
     */
    public ArrayList<Patient> generate() {
        return generate(10);
    }

    /**
     * This method will generateTnmAndAddToTumour a list of Patients with a given amount of patients for the patient generators profile. </br>
     *
     * @param amountDataSets decides how many patients will be generated (default: 10)
     * @return the generated List of patients
     */
    public ArrayList<Patient> generate(int amountDataSets) {

        ArrayList<Patient> patientList = new ArrayList<>();

        for (int i = 0; i < amountDataSets; i++) {

            Patient patient = generatePatient();
            patientList.add(patient);

        }

        return patientList;
    }

    private Patient generatePatient(){

        Patient patient = new Patient();

        EntitySourceEnumType entitySource = EntitySourceEnumType.XMLIMPORT;
        patient.getPatientDataSet().setSource(entitySource);

        PatientMasterdataType patientMasterdata = masterDataGenerator.generate();
        patient.getPatientDataSet().setMasterdata(patientMasterdata);

        patient = addEpisodes(patient);
        patient = diagnosisGenerator.generateDiagnosisAndAddToPatient(patient);
        patient = sampleGenerator.generateSamplesAndAddToPatient(patient);
        patient = addDktkConsent(patient);

        List<FlexibleIDType> ids = new ArrayList<>();
        FlexibleIDType patientId = generatePatientId();
        FlexibleIDType dktkStandortId = generateDktkStandortId();
        ids.add(patientId);
        ids.add(dktkStandortId);

        patient = addListOfIds(patient, ids);


        return patient;

    }

    private Patient addListOfIds (Patient patient, List<FlexibleIDType> ids){

        IDContainerType idContainer = patient.getPatientDataSet().getIDContainer();

        if (idContainer == null){
            idContainer = new IDContainerType();
            patient.getPatientDataSet().setIDContainer(idContainer);
        }

        List<FlexibleIDType> flexibleIDs = idContainer.getFlexibleID();
        flexibleIDs.addAll(ids);

        return patient;

    }

    private FlexibleIDType generatePatientId (){

        // Add Sample ID to Patient Data Set
        String patientId = idGenerator.generateId();


        FlexibleIDType flexibleIDType = new FlexibleIDType();
        flexibleIDType.setKey("PATIENTID");
        flexibleIDType.setValue(patientId);

        return flexibleIDType;

    }

    private FlexibleIDType generateDktkStandortId (){

        // Add Sample ID to Patient Data Set
        String patientId = idGenerator.generateAlphanumericId(8);
        patientId = patientId.toUpperCase();

        FlexibleIDType flexibleIDType = new FlexibleIDType();
        flexibleIDType.setKey("Teststandort");
        flexibleIDType.setValue(patientId);

        return flexibleIDType;

    }

    private Patient addDktkConsent(Patient patient) {

        List<DiagnosisType> diagnosisList = patient.getPatientDataSet().getDiagnosis();
        if(diagnosisList.size() > 0 && ThreadLocalRandom.current().nextDouble(0,1) <= tdgProfile.getProbability("pDKTKConsent").getProbability())
            patient = dktkConsentGenerator.generateDktkFlagAndAddToPatient(patient, diagnosisList.get(0));

        return patient;
    }


    private Patient addEpisodes (Patient patient){

        ArrayList<EpisodeType> episodes = episodeGenerator.generate(patient.getPatientDataSet());

        for(EpisodeType episode : episodes){
            patient.getPatientDataSet().getEpisode().add(episode);
            patient = addOrganisationUnitRefs(patient, episode);
        }

        return patient;

    }

    private Patient addOrganisationUnitRefs (Patient patient, EpisodeType episode){

        CatalogueDataRefType episodeHabitationRef = episode.getHabitationRef();
        if (episodeHabitationRef != null) {

            String episodeHabitationRefValue = episodeHabitationRef.getValue();
            List<CatalogueDataRefType> organisationUnitRefs = patient.getPatientDataSet().getOrganisationUnitRefs();

            boolean isContained = false;
            for (CatalogueDataRefType organisationUnitRef : organisationUnitRefs) {
                String value = organisationUnitRef.getValue();
                if (value.equalsIgnoreCase(episodeHabitationRefValue)){
                    isContained = true;
                    break;
                }
            }

            if (!isContained){
                organisationUnitRefs.add(episode.getHabitationRef());
            }

        }

        return patient;

    }

}
