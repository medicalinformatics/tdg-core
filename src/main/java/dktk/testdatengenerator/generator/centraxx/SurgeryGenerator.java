package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.*;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.util.RandomOperations;

import java.util.Arrays;
import java.util.List;

public class SurgeryGenerator {


    private RandomOperations randomOperations;
    private IdGenerator idGenerator;
    private List<IntentionGTDSEnumType> intentionList = Arrays.asList(new IntentionGTDSEnumType[]{IntentionGTDSEnumType.P, IntentionGTDSEnumType.K, IntentionGTDSEnumType.X});
    private List<RClassificationGTDSEnumType> rClassificationList = Arrays.asList(new RClassificationGTDSEnumType[]{RClassificationGTDSEnumType.R0, RClassificationGTDSEnumType.R1, RClassificationGTDSEnumType.R2, RClassificationGTDSEnumType.RX});

    private final String SURGERY_MIN = "surgeryMin";
    private final String SURGERY_MAX = "surgeryMax";



    public SurgeryGenerator(TDGProfile tdgProfile) {
        randomOperations = new RandomOperations(tdgProfile);
        idGenerator = new IdGenerator(tdgProfile);
    }

    public GTDSProgressEIType generateSurgeriesAndAddToProgress (GTDSProgressEIType progress){

        if (progress != null){

            List<GTDSTherapy> therapyList = progress.getTherapy();
            int numberOfSurgeriesToBeGenerated = getNumberOfSurgeriesToBeGenerated();
            for (int i=0; i< numberOfSurgeriesToBeGenerated; i++){
                GTDSSurgeryEIType surgery = generateSurgery(progress);
                therapyList.add(surgery);
            }

        }

        return progress;
    }

    private int getNumberOfSurgeriesToBeGenerated(){
        return randomOperations.generateNumberOfItemsToBeGenerated(SURGERY_MIN, SURGERY_MAX);
    }

    private GTDSSurgeryEIType generateSurgery (GTDSProgressEIType progress){

        GTDSSurgeryEIType surgery = new GTDSSurgeryEIType();


        // SURGERY_RCLASSIFIC_LOCAL
        RClassificationGTDSEnumType rClassification = generateRClassification();
        // SURGERY_RCLASSIFIC
        RClassificationGTDSEnumType rClassificationLocal = generateRClassificationLocal();
        // SURGERY_INTENTION
        IntentionGTDSEnumType intention = generateIntention();
        String surgeryId = generateSurgeryId();

        // CXX: SURGERY_RCLASSIFIC
        surgery.setRClassification(rClassification);
        // CXX: SURGERY_RCLASSIFIC_LOCAL
        surgery.setRClassificationLocal(rClassificationLocal);
        // CXX: SURGERY_INTENTION
        surgery.setIntention(intention);
        surgery.setSurgeryID(surgeryId);

        return surgery;

    }

    private String generateSurgeryId(){
        return idGenerator.generateId();
    }

    private RClassificationGTDSEnumType generateRClassification(){
        return randomOperations.getRandomFromArrayList(rClassificationList);
    }

    private RClassificationGTDSEnumType generateRClassificationLocal(){
        return randomOperations.getRandomFromArrayList(rClassificationList);
    }

    private IntentionGTDSEnumType generateIntention(){
        return randomOperations.getRandomFromArrayList(intentionList);
    }

}
