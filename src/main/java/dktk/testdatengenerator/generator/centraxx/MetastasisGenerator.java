package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.*;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.exceptions.DateGeneratorException;
import dktk.testdatengenerator.util.RandomOperations;

import java.util.Arrays;
import java.util.List;

public class MetastasisGenerator {

    private RandomOperations randomOperations;
    private DateGenerator dateGenerator;
    private IdGenerator idGenerator;

    private final String METASTASIS_MIN = "metastasisMin";
    private final String METASTASIS_MAX = "metastasisMax";

    private final List<LocalisationGTDSEnumType> excludedLocalisationCodes = Arrays.asList(LocalisationGTDSEnumType.X);


    public MetastasisGenerator(TDGProfile tdgProfile) {

        randomOperations = new RandomOperations(tdgProfile);
        dateGenerator = new DateGenerator();
        idGenerator = new IdGenerator(tdgProfile);
    }

    public GTDSTumourEIType generateMetastasisAndAddToTumour (GTDSTumourEIType tumour){

        if (tumour != null){

            List<GTDSMetastasisEIType> metastasisList = tumour.getMetastasis();
            int numberOfMetastasisToBeGenerated = getNumberOfMetastasisToBeGenerated();
            for (int i= 0; i< numberOfMetastasisToBeGenerated; i++){

                GTDSMetastasisEIType metastasis = generateMetastasis(tumour);
                metastasisList.add(metastasis);

            }

        }

        return tumour;

    }

    private int getNumberOfMetastasisToBeGenerated(){
        return randomOperations.generateNumberOfItemsToBeGenerated(METASTASIS_MIN, METASTASIS_MAX);
    }

    private GTDSMetastasisEIType generateMetastasis (GTDSTumourEIType tumour) {

        GTDSMetastasisEIType metastasis = new GTDSMetastasisEIType();

        DateType date = generateDate(tumour);
        LocalisationGTDSEnumType localisationCode = generateLocalisationCode();
        String metastatsisID = generateMetastatsisID();

        // CXX: METASTASIS_METASDATE
        metastasis.setDate(date);
        // CXX: METASTASIS_LOCALISATIONCODE
        metastasis.setLocalisationCode(localisationCode);
        metastasis.setMetastasisID(metastatsisID);


        return metastasis;

    }

    private String generateMetastatsisID(){
        return idGenerator.generateId();
    }

    private DateType generateDate (GTDSTumourEIType tumour){

        try {

            return generateDate_WithoutExceptionManagement(tumour);

        } catch (DateGeneratorException e) {
            e.printStackTrace();
            return null;
        }

    }

    private DateType generateDate_WithoutExceptionManagement(GTDSTumourEIType tumour) throws DateGeneratorException {

        DateType minDate = getMinDate(tumour);
        DateType maxDate = getMaxDate(tumour);

        return dateGenerator.generateDateBetween(minDate, maxDate);

    }

    private DateType getMinDate (GTDSTumourEIType tumour){

        List<GTDSProgressEIType> progressList = tumour.getProgress();
        DateType minDate = null;

        for (GTDSProgressEIType progress : progressList){

            DateType therapyStart = progress.getTherapyStart();
            if (minDate == null || therapyStart.getDate().compare(minDate.getDate()) < 0 ){
                minDate = therapyStart;
            }

        }

        return minDate;

    }

    private DateType getMaxDate (GTDSTumourEIType tumour){

        List<GTDSProgressEIType> progressList = tumour.getProgress();
        DateType maxDate = null;

        for (GTDSProgressEIType progress : progressList){

            DateType therapyEnd = progress.getTherapyEnd();
            if (maxDate == null || therapyEnd.getDate().compare(maxDate.getDate()) > 0 ){
                maxDate = therapyEnd;
            }

        }

        return maxDate;

    }

    private LocalisationGTDSEnumType generateLocalisationCode(){
        return randomOperations.getRandomFromEnum(LocalisationGTDSEnumType.class, excludedLocalisationCodes);
    }

}
