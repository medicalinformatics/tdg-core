package dktk.testdatengenerator.generator.centraxx;

/**
 * Created by brennert on 04.07.2017.
 */
public class HospitalLocation {
    private double latitude;
    private double longitude;

    private double radius;

    public HospitalLocation(double latitude, double longitude, double radius){
        setLatitude(latitude);
        setLongitude(longitude);
        setRadius(radius);
    }

    public double getLatitude() {
        return latitude;
    }

    private void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    private void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getRadius() {
        return radius;
    }

    private void setRadius(double radius) {
        this.radius = radius;
    }
}
