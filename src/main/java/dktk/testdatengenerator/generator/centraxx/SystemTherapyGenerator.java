package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.*;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.util.RandomOperations;

import java.util.Arrays;
import java.util.List;

public class SystemTherapyGenerator {

    private RandomOperations randomOperations;
    private IdGenerator idGenerator;
    // TODO : move to DB
    // TODO: Kairos: introduce S
    private List<IntentionGTDSEnumType> intentionList = Arrays.asList(new IntentionGTDSEnumType[]{IntentionGTDSEnumType.P, IntentionGTDSEnumType.K, IntentionGTDSEnumType.X});
    private List<SystemTherapyKindGTDSEnumType> therapyKindList = Arrays.asList(new SystemTherapyKindGTDSEnumType[]{SystemTherapyKindGTDSEnumType.PR, SystemTherapyKindGTDSEnumType.P, SystemTherapyKindGTDSEnumType.N, SystemTherapyKindGTDSEnumType.I});
    // TODO: Kairos: introduce S, R
    private List<String> therapyKindRefList = Arrays.asList(new String[]{"P", "N", "I"});

    private final String SYSTEM_THERAPY_MIN = "systherapyMin";
    private final String SYSTEM_THERAPY_MAX = "systherapyMax";

    private List<TherapyTypeGTDSEnumType> excludedTherapyTypeGTDSEnum = Arrays.asList(TherapyTypeGTDSEnumType.X);


    public SystemTherapyGenerator(TDGProfile tdgProfile) {
        randomOperations = new RandomOperations(tdgProfile);
        idGenerator = new IdGenerator(tdgProfile);
    }

    public GTDSProgressEIType generateTherapiesAndAddToProgress (GTDSProgressEIType progress, EpisodeType episode) {

        if (progress != null){

            List<GTDSTherapy> therapyList = progress.getTherapy();

            int numberOfTherapies = getNumberOfTherapies();
            for (int i = 0; i < numberOfTherapies ;  i++){
                GTDSSystemTherapyEIType therapy = generateTherapy(progress, episode);
                therapyList.add(therapy);
            }

        }

        return progress;

    }

    private int getNumberOfTherapies(){
        return randomOperations.generateNumberOfItemsToBeGenerated(SYSTEM_THERAPY_MIN, SYSTEM_THERAPY_MAX);
    }

    private GTDSSystemTherapyEIType generateTherapy (GTDSProgressEIType progress, EpisodeType episode) {

        GTDSSystemTherapyEIType therapy = new GTDSSystemTherapyEIType();


        IntentionGTDSEnumType therapyIntention = generateTherapyIntention();
        GtdsDictionaryRefType therapyKindRef = generateTherapyKindRef();
        DateType therapieStart = getTherapieStart(episode);
        DateType therapieEnd = getTherapieEnd(episode);
        String protocolId = generateProtocolId();
        String description = generateDescription();
        SelectionListGTDSEnumType adverseEffects = generateAdverseEffects();
        TherapyTypeGTDSEnumType therapyType = generateTherapyType();
        String therapyId = generateTherapyId();


        // CXX: SYSTHPY_THERAPYSTART
        therapy.setTherapyStart(therapieStart);
        // CXX: SYSTHPY_THERAPYEND
        therapy.setTherapyEnd(therapieEnd);
        therapy.setTherapyType(therapyType);
        therapy.setAdverseEffectsEnum(adverseEffects);
        // CXX: SYSTHPY_INTENTION
        therapy.setIntention(therapyIntention);
        // CXX: SYSTHPY_THERAPYKIND
        therapy.setTherapyKindRef(therapyKindRef);
        // CXX: SYSTHPY_PROTOCOLID
        therapy.setProtocolID(protocolId);
        // CXX: SYSTHPY_DESCRIPTION
        therapy.setDescription(description);
        therapy.setSystemTherapyID(therapyId);

        return therapy;

    }

    private String generateTherapyId (){
        return idGenerator.generateId();
    }

    private SystemTherapyKindGTDSEnumType generateTherapyKind(){
        return randomOperations.getRandomFromArrayList(therapyKindList);
    }

    private String generateProtocolId(){
        return randomOperations.generateBigInteger().toString();
    }

    private String generateDescription(){
        //TODO
        return "description_" + randomOperations.generateInteger(1, 1000000);
    }

    private TherapyTypeGTDSEnumType generateTherapyType(){
        return randomOperations.getRandomFromEnum(TherapyTypeGTDSEnumType.class, excludedTherapyTypeGTDSEnum);
    }

    private SelectionListGTDSEnumType generateAdverseEffects(){
        return randomOperations.getRandomFromEnum(SelectionListGTDSEnumType.class);
    }

    private DateType getTherapieStart(EpisodeType episode){
        //TODO
        return episode.getValidFrom();
    }

    private DateType getTherapieEnd (EpisodeType episode){
        //TODO
        return episode.getValidUntil();
    }

    private IntentionGTDSEnumType generateTherapyIntention(){
        return randomOperations.getRandomFromArrayList(intentionList);
    }

    /*
    private GtdsDictionaryRefType generateTherapyIntentionRef(){

        GtdsDictionaryRefType intentionRef = new GtdsDictionaryRefType();


        intentionRef.setCode(generateTherapyIntention().value());
        intentionRef.setVersion(GtdsVersionEnumType.ORIGINAL);

        //TODO : define ref somewhere else
        intentionRef.setRefUrl("/rest/export/catalogdata/ProgressTherapyIntentionDictionary/374");

        return intentionRef;

    }
*/

    private GtdsDictionaryRefType generateTherapyKindRef(){

        GtdsDictionaryRefType therapyKindRef = new GtdsDictionaryRefType();


        String code = randomOperations.getRandomFromArrayList(therapyKindRefList);
        therapyKindRef.setCode(code);
        therapyKindRef.setVersion(GtdsVersionEnumType.ORIGINAL);

        //TODO : define ref somewhere else
        therapyKindRef.setRefUrl("/rest/export/catalogdata/SystemTherapyKindDictionary/253");

        return therapyKindRef;

    }






}
