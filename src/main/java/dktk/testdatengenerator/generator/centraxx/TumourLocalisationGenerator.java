package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.*;
import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.util.RandomOperations;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is responsible for creating a tumour location for a given tumour DataType.
 * Created by brennert on 25.07.2017.
 */
public class TumourLocalisationGenerator {

    private DBConnector dbConnector;
    private RandomOperations randomOperations;
    private IdGenerator idGenerator;

    private final String TUMOUR_LOC_MIN = "tumourLocMin";
    private final String TUMOUR_LOC_MAX = "tumourLocMax";

    public TumourLocalisationGenerator(TDGProfile tdgProfile, DBConnector dbConnector) {

        this.dbConnector = dbConnector;
        randomOperations = new RandomOperations(tdgProfile);
        idGenerator = new IdGenerator(tdgProfile);

    }

    public GTDSTumourEIType generateTumourLocalisationsAndAddToTumour(GTDSTumourEIType tumour){

        if (tumour != null){

            List<GTDSTumourLocalisationEIType> tumourLocalisationList = tumour.getTumourLocalisation();
            int numberOfTumorLocalisationsToBeGenerated = getNumberOfTumorLocalisationsToBeGenerated();
            for (int i=0; i< numberOfTumorLocalisationsToBeGenerated; i++){
                GTDSTumourLocalisationEIType tumourLocalisation = generateTumourLocalisation(tumour);
                tumourLocalisationList.add(tumourLocalisation);
            }

        }

        return tumour;

    }

    private int getNumberOfTumorLocalisationsToBeGenerated(){
        return randomOperations.generateNumberOfItemsToBeGenerated(TUMOUR_LOC_MIN, TUMOUR_LOC_MAX);
    }

    private GTDSTumourLocalisationEIType generateTumourLocalisation(GTDSTumourEIType tumour) {

        GTDSTumourLocalisationEIType tumourLocalisation = new GTDSTumourLocalisationEIType();

        String localisationCode = generateLocalisationCode(tumour);
        String localisationText = generateLocalisationText(localisationCode);
        SideGTDSEnumType side = generateSide();
        String version = generateVersion();
        //IcdEntryRefType icdEntryRef = generateICDEntryRef(localisationCode);
        String tumourLocalisationId = generateTumourLocalisationId();

        tumourLocalisation.setLocalisationCode(localisationCode);
        tumourLocalisation.setLocalisationText(localisationText);
        // CXX: TLOC_SIDE
        tumourLocalisation.setSide(side);
        //tumourLocalisation.setComplexIcdEntryRef(icdEntryRef);
        tumourLocalisation.setXmlId(tumourLocalisationId);
        // CXX: TLOC_VERSION
        tumourLocalisation.setVersion(version);

        return tumourLocalisation;

    }

    private String generateTumourLocalisationId(){
        return idGenerator.generateId();
    }

    private String generateLocalisationCode(GTDSTumourEIType tumour){
        return tumour.getICD10();
    }


    private String generateVersion (){
        return "ICD-O3";

    }

    private IcdEntryRefType generateICDEntryRef(String localisationCode) {

        IcdEntryRefType icdEntryRef = new IcdEntryRefType();

        IcdCatalogRefType icdCatalogRef = generateIcdCatalogRef();


        // CXX: TLOC_LOCALISATIONCODE
        icdEntryRef.setCode(localisationCode);
        icdEntryRef.setKind("code");
        // CXX: CATALOG_VERSION
        icdEntryRef.setIcdCatalogRef(icdCatalogRef);

        return icdEntryRef;

    }

    private IcdCatalogRefType generateIcdCatalogRef (){

        IcdCatalogRefType icdCatalogRef = new IcdCatalogRefType();

        icdCatalogRef.setKind("ICD-General");
        icdCatalogRef.setVersion("2017");

        return icdCatalogRef;

    }

     private String generateLocalisationText(String localisationCode) {

        try {

            return getLocalisationText_WithoutManagemenetException(localisationCode);

        } catch (SQLException sqle) {
            System.out.println("Couldn't read localisation text from database");
            sqle.printStackTrace();
            return null;
        }

    }

    private String getLocalisationText_WithoutManagemenetException(String localisationCode) throws SQLException {

        ResultSet resultSet = dbConnector.sendQuery("SELECT * FROM icd_o_codes WHERE icd_o_code like '" + localisationCode + "%';");
        ArrayList<String> localisations = new ArrayList<>();
        while (resultSet.next())
            localisations.add(resultSet.getString("icd_o_location"));
        resultSet.close();

        String localisationText = randomOperations.getRandomFromArrayList(localisations);

        return (localisationText == null) ? "unknown" : localisationText;

    }

    private SideGTDSEnumType generateSide(){
        return randomOperations.getRandomFromEnum(SideGTDSEnumType.class, "TumourSide", "");
    }
}
