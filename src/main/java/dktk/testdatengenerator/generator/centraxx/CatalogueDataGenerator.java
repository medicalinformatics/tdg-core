package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.*;
import dktk.testdatengenerator.util.MdrUtils;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class CatalogueDataGenerator {

    /*private MdrUtils mdrUtils;

    public CatalogueDataGenerator(MdrUtils mdrUtils) {
        this.mdrUtils = mdrUtils;
    }*/

    public CatalogueDataType generateCatalogData (){

        //TODO
        CatalogueDataType catalogueData = new CatalogueDataType();

        //catalogueData = addStateMetastatsisDictionary(catalogueData);



        return catalogueData;

    }

    private CatalogueDataType addStateMetastatsisDictionary (CatalogueDataType catalogueData){

        List<GtdsDictionaryType> stateMetastasisDictionary = catalogueData.getStateMetastasisDictionary();

        String[][] dictionaryEntries = getDictionaryEntries(StateMetastasisGTDSEnumType.class);
        List<GtdsDictionaryType> dictionaryList = generateDictionaryList(dictionaryEntries);

        stateMetastasisDictionary.addAll(dictionaryList);

        return catalogueData;

    }

    private <T extends Enum<T>> String[][] getDictionaryEntries (Class<T> enumType){

        EnumSet<T> enumSets = EnumSet.allOf(enumType);

        String[][] dictionaryEntries = new String[enumSets.size()][3];
        int i= 0;
        for ( T enumElement : enumSets){
            for (int j = 0; j < 3 ; j++) {
                dictionaryEntries[i][j] = enumElement.toString();
            }
            i++;
        }

        return dictionaryEntries;

    }

    private List<GtdsDictionaryType> generateDictionaryList(String[][] dictionaryEntries){

        List<GtdsDictionaryType> dictionaryList = new ArrayList<>();

        for (String[] dictionaryEntry : dictionaryEntries){

            GtdsDictionaryType dictionary = createDictionary(dictionaryEntry[0], dictionaryEntry[1], dictionaryEntry[2]);
            dictionaryList.add(dictionary);

        }

        return dictionaryList;

    }

    private GtdsDictionaryType createDictionary (String code, String enValue, String deValue){

        GtdsDictionaryType dictionary = new GtdsDictionaryType();

        MultilingualEntryType enMultilingualEntry = createMultilingualEntry(enValue, LangEnumType.en);
        MultilingualEntryType deMultilingualEntry = createMultilingualEntry(deValue, LangEnumType.de);

        dictionary.setCode(code);
        dictionary.setVersion(GtdsVersionEnumType.USERDEFINED);

        List<MultilingualEntryType> nameMultilingualEntries = dictionary.getNameMultilingualEntries();
        nameMultilingualEntries.add(enMultilingualEntry);
        nameMultilingualEntries.add(deMultilingualEntry);


        return dictionary;

    }

    private MultilingualEntryType createMultilingualEntry (String value, LangEnumType language){

        MultilingualEntryType multilingualEntry = new MultilingualEntryType();

        multilingualEntry.setLang(language);
        multilingualEntry.setValue(value);

        return multilingualEntry;

    }

}
