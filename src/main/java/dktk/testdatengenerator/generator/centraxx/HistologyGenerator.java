package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.GTDSHistologyType;
import de.kairos_med.GTDSTumourEIType;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.util.MdrUtils;
import dktk.testdatengenerator.util.RandomOperations;

import java.util.List;

public class HistologyGenerator {


    private RandomOperations randomOperations;
    private MdrUtils mdrUtils;
    private IdGenerator idGenerator;


    private final String GRADING_MDR_ID = "urn:dktk:dataelement:9:2";
    private final String HISTOLOGY_MIN = "histologyMin";
    private final String HISTOLOGY_MAX = "histologyMax";


    public HistologyGenerator (TDGProfile tdgProfile){
        randomOperations = new RandomOperations(tdgProfile);
        mdrUtils = new MdrUtils(tdgProfile);
        idGenerator = new IdGenerator(tdgProfile);
    }

    public GTDSTumourEIType generateHistologyAndAddToTumour (GTDSTumourEIType tumour){

        if (tumour != null) {

            List<GTDSHistologyType> histologyList = tumour.getHistology();
            int numberOfHistologies = getNumberOfHistologies();
            for (int i=0; i<numberOfHistologies; i++){
                GTDSHistologyType histology = generateHistology(tumour);
                histologyList.add(histology);
            }

        }
        return tumour;

    }

    private int getNumberOfHistologies(){
        return randomOperations.generateNumberOfItemsToBeGenerated(HISTOLOGY_MIN, HISTOLOGY_MAX);
    }

    private GTDSHistologyType generateHistology (GTDSTumourEIType tumour){

        GTDSHistologyType histology = new GTDSHistologyType();

        String code = generateCode();
        String version = generateVersion();
        String grading = generateGrading();
        String histologyId = generateHistologyId();

        // CXX: HISTO_CODE
        histology.setCode(code);
        // CXX: HISTO_VERSION
        histology.setVersion(version);
        // CXX: HISTO_GRADING
        histology.setGrading(grading);
        histology.setHistologyID(histologyId);


        return histology;

    }

    private String generateHistologyId(){
        return idGenerator.generateId();
    }

    private String generateGrading(){
        return mdrUtils.generateMdrEnumeratedValue(GRADING_MDR_ID);
    }

    private String generateVersion(){
        return "8";
    }

    private String generateCode(){

        StringBuilder stringBuilder = new StringBuilder();
        int part1 = randomOperations.generateInteger(1000,9999);
        int part2 = randomOperations.generateInteger(0,9);

        stringBuilder.append(part1);
        stringBuilder.append('/');
        stringBuilder.append(part2);

        return stringBuilder.toString();

    }


}
