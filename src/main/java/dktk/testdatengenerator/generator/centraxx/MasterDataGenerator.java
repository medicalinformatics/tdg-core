package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.CatalogueDataRefType;
import de.kairos_med.DateType;
import de.kairos_med.GenderTypeEnum;
import de.kairos_med.PatientMasterdataType;
import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.db.PatientQueries;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.enums.AgeGroupEnum;
import dktk.testdatengenerator.enums.MaritalStatusEnum;
import dktk.testdatengenerator.enums.TitleEnum;
import dktk.testdatengenerator.exceptions.AddressGeneratorException;
import dktk.testdatengenerator.exceptions.DateGeneratorException;
import dktk.testdatengenerator.util.RandomOperations;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.ThreadLocalRandom;

/**
 * This class is responsible for generating the MasterData of PatientDataSets.
 * Created by BrennerT on 20.06.2017.
 */
public class MasterDataGenerator {

    private DBConnector dbConnector;
    private DateGenerator dateGenerator;
    private AddressGenerator addressGenerator;
    private TDGProfile tdgProfile;
    private RandomOperations randomOperations;
    private PatientQueries patientQueries;

    public MasterDataGenerator(TDGProfile tdgProfile, DBConnector dbConnector) {
        try {
            addressGenerator = new AddressGenerator(100, new String[]{"DKTK-Testdatengenerator"}, dbConnector);
        } catch (AddressGeneratorException age) {
            System.out.println("Couldn't initialize AddressGenerator");
            age.printStackTrace();
        }
        dateGenerator = new DateGenerator();
        this.tdgProfile = tdgProfile;
        randomOperations = new RandomOperations(tdgProfile);
        patientQueries = new PatientQueries(dbConnector);
        this.dbConnector = dbConnector;
    }

    public PatientMasterdataType generate (){

        try {
            return generate_WithoutManagementException();
        } catch (AddressGeneratorException e) {
            e.printStackTrace();
            return null;
        }

    }

    private PatientMasterdataType generate_WithoutManagementException() throws AddressGeneratorException {

        PatientMasterdataType patient = new PatientMasterdataType();

        // CXX: PAT_GENDER_TYPE
        patient.setGender(getRandomGender());
        AgeGroupEnum patientAgeGroup = randomOperations.getRandomFromEnum(AgeGroupEnum.class, "Age", "");
        if (randomOperations.getBinarRandomFromProbabilty("pDead")) {
            setDateOfDeath(patient, patientAgeGroup);
        }
        setPatientBirthdate(patient, patientAgeGroup);

        patient.setFirstName(patientQueries.queryRandomFirstName(patient.getGender()));

        setMaritalStatus(patient, patientAgeGroup);

        setPatientLastName(patient, patientQueries);

        // patientMasterdataType.setTitleTypeRef(getRandomTitleEnum());

        CatalogueDataRefType citizenShipCatalogueType = new CatalogueDataRefType();
        citizenShipCatalogueType.setValue("DEU");
        patient.setCitizenshipTypeRef(citizenShipCatalogueType);
        // Because it is currently not known how centraxx accepts the ethnicity
//      setPatientEthnicity(patientMasterdataType);
        String hospital = addressGenerator.getRandomHospital();
        patient.setPatientAddress(addressGenerator.takeRandomAddressFromList(hospital));
        patient.setBirthPlace(addressGenerator.findBirthplaceForHospital(hospital));

        // TODO : Remove Line.
        patient.setMaritalStatusTypeRef(null);

        return patient;

    }

    private void setDateOfDeath(PatientMasterdataType patient, AgeGroupEnum ageGroup) {
        // TODO: set manually the minimum date of the DateGenerator
        GregorianCalendar patientDeathCalendar = dateGenerator.getMinDateAsGregorianCalendar();
        patientDeathCalendar.add(Calendar.YEAR, ageGroup.getMax());
        DateType patientDeathDate = null;
        try {
            patientDeathDate = dateGenerator.genereateDateAfter(patientDeathCalendar.getTime());
        } catch (DateGeneratorException dge) {
            dge.printStackTrace();
        }
        patient.setDateOfDeath(patientDeathDate);
    }

    private void setPatientBirthdate(PatientMasterdataType patient, AgeGroupEnum patientAgeGroup) {
        GregorianCalendar patientBirthDayCalendar = (patient.getDateOfDeath() != null) ? patient.getDateOfDeath().getDate().toGregorianCalendar() : new GregorianCalendar();
        Date testDate = patientBirthDayCalendar.getTime();

        patientBirthDayCalendar.add(GregorianCalendar.YEAR, patientAgeGroup.getMax() * -1);
        Date minBirthdayDate = patientBirthDayCalendar.getTime();

        patientBirthDayCalendar.add(GregorianCalendar.YEAR, patientAgeGroup.getMax() - patientAgeGroup.getMin());
        Date maxBirthdayDate = patientBirthDayCalendar.getTime();

        DateType patientBirthday = null;
        try {
            patientBirthday = dateGenerator.generateDateBetween(minBirthdayDate, maxBirthdayDate);
        } catch (DateGeneratorException dge) {
            dge.printStackTrace();
        }

        // CXX: PAT_BIRTHDATE
        patient.setDateOfBirth(patientBirthday);
    }

    private void setPatientLastName(PatientMasterdataType patientMasterdataType, PatientQueries patientQueries) {
        if (patientMasterdataType.getMaritalStatusTypeRef().getValue() == "Married") {
            if (patientMasterdataType.getGender() == GenderTypeEnum.FEMALE) {
                if (ThreadLocalRandom.current().nextDouble(0, 1) <= tdgProfile.getProbability("marriedLastnameFemale").getProbability())
                    patientMasterdataType.setLastName(patientQueries.queryRandomLastNameMarried());
                else
                    patientMasterdataType.setLastName(patientQueries.queryRandomLastName());
            } else {
                if (ThreadLocalRandom.current().nextDouble(0, 1) <= tdgProfile.getProbability("marriedLastnameMale").getProbability())
                    patientMasterdataType.setLastName(patientQueries.queryRandomLastNameMarried());
                else
                    patientMasterdataType.setLastName(patientQueries.queryRandomLastName());
            }
        } else {
            patientMasterdataType.setLastName(patientQueries.queryRandomLastName());
        }
    }

    private void setPatientEthnicity(PatientMasterdataType patientMasterdataType, PatientQueries patientQueries) {

        String randomEthnicity = patientQueries.queryRandomEthnicity(patientMasterdataType.getCitizenshipTypeRef().getValue());
        CatalogueDataRefType randomEthnicityCatalogueRef = new CatalogueDataRefType();
        randomEthnicityCatalogueRef.setValue(randomEthnicity);
        patientMasterdataType.getEthnicityTypeRef().add(randomEthnicityCatalogueRef);

    }

    private void setMaritalStatus(PatientMasterdataType patient, AgeGroupEnum patientAgeGroup) {

        if (patientAgeGroup != patientAgeGroup.Under10 && patientAgeGroup != AgeGroupEnum.Between10And19) {
            MaritalStatusEnum maritalStatus = randomOperations.getRandomFromEnum(MaritalStatusEnum.class, "", "");
            CatalogueDataRefType maritalStatusTypeRef = new CatalogueDataRefType();
            maritalStatusTypeRef.setValue(maritalStatus.getCode());
            patient.setMaritalStatusTypeRef(maritalStatusTypeRef);
        } else {
            CatalogueDataRefType mariatalStatusTypeRef = new CatalogueDataRefType();
            mariatalStatusTypeRef.setValue(MaritalStatusEnum.NeverMarried.getCode());
            patient.setMaritalStatusTypeRef(mariatalStatusTypeRef);
        }

    }

    /**
     * This method uses the properties table to determine a random gender with given probabilities.
     *
     * @return a random Gender
     */
    private GenderTypeEnum getRandomGender() {
        GenderTypeEnum gender = randomOperations.getRandomFromEnum(GenderTypeEnum.class, "", "");
        if (gender != null)
            return gender;
        return GenderTypeEnum.UNDEFINED;
    }

    private String getRandomTitleEnum() {
        if (ThreadLocalRandom.current().nextDouble(0, 1) <= tdgProfile.getProbability("pTitle").getProbability()) {
            TitleEnum title = randomOperations.getRandomFromEnum(TitleEnum.class, "", "");
            return title.value();
        } else {
            return null;
        }
    }

}
