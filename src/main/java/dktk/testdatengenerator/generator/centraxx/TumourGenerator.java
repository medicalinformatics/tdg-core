package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.*;
import dktk.testdatengenerator.customelements.Patient;
import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.enums.GradingEnum;
import dktk.testdatengenerator.util.RandomOperations;

import javax.xml.datatype.DatatypeConstants;
import java.util.List;

/**
 * This class should generateTnmAndAddToTumour a new Tumour DataSet for a Patient
 * Created by brennert on 20.07.2017.
 */
public class TumourGenerator {

    private RandomOperations randomOperations;
    private TumourLocalisationGenerator tumourLocalisationGenerator;
    private ProgressGenerator progressGenerator;
    private MetastasisGenerator metastasisGenerator;
    private TnmGenerator tnmGenerator;
    private HistologyGenerator histologyGenerator;
    private IdGenerator idGenerator;
    private MolecularMarkerGenerator molecularMarkerGenerator;

    private final String TUMOUR_MIN = "tumourMin";
    private final String TUMOUR_MAX = "tumourMax";


    public TumourGenerator(TDGProfile tdgProfile, DBConnector dbConnector){

        randomOperations = new RandomOperations(tdgProfile);
        tumourLocalisationGenerator = new TumourLocalisationGenerator(tdgProfile, dbConnector);
        progressGenerator = new ProgressGenerator(tdgProfile);
        metastasisGenerator = new MetastasisGenerator(tdgProfile);
        tnmGenerator = new TnmGenerator(tdgProfile);
        histologyGenerator = new HistologyGenerator(tdgProfile);
        idGenerator = new IdGenerator(tdgProfile);
        molecularMarkerGenerator = new MolecularMarkerGenerator(tdgProfile);

    }


    public DiagnosisType generateTumourAndAddToDiagnosis (DiagnosisType diagnosis, Patient patient){

        if (diagnosis != null){

            List<GTDSTumourEIType> tumourList = diagnosis.getTumour();

            int numberOfTumoursToBeGenerated = getNumberOfTumoursToBeGenerated();
            for (int i=0; i < numberOfTumoursToBeGenerated; i++){
                GTDSTumourEIType tumour = generateTumour(diagnosis, patient);
                tumourList.add(tumour);
            }

        }

        return diagnosis;
    }

    private int getNumberOfTumoursToBeGenerated(){
        //return randomOperations.generateInteger(1,3);
        //return randomOperations.generateNumberOfItemsToBeGenerated(TUMOUR_MIN, TUMOUR_MAX);
        return 1; // in der Praxis gibt es nur ein Tumour pro Diagnose
    }


    private GTDSTumourEIType generateTumour (DiagnosisType diagnosis, Patient patient) {

        GTDSTumourEIType tumour = new GTDSTumourEIType();

        String tumourId = generateTumourId();
        DateType diagnosisDate = generateDiagnosisDate(diagnosis);
        String icd10 = generateIcd10(diagnosis);
        String diagnosisText = generateDiagnosisText(diagnosis);
        String episodeRef = generateEpisodeRef(diagnosis);
        TumourCaptureCauseGTDSEnumType captureCause = generateCaptureCause();
        YesNoXGTDSEnumType captureFinishState = generateCaptureFinishState();
        CodeTypeGTDSEnumType codeType = generateCodeType();
        ExpansionGeneralGTDSEnumType expansionGeneral = generateExpansionGeneral();
        SourceTypeGTDSEnumType source = generateSource();
        VisitCauseGTDSEnumType visitCause = generateVisitCause();
        String grading = generateGrading();
        PatientEnlightenStatusGTDSEnumType patientEnglihtenStatus = generatePatientEnglihtenStatus();

        tumour.setTumorId(tumourId);
        tumour.setDiagnosisDate(diagnosisDate);
        tumour.setICD10(icd10);
        tumour.setDiagnosisText(diagnosisText);
        tumour.setEpisodeRef(episodeRef);
        tumour.setCaptureCause(captureCause);
        tumour.setCaptureFinishState(captureFinishState);
        tumour.setCodeType(codeType);
        tumour.setExpansionGeneral(expansionGeneral);
        tumour.setSource(source);
        tumour.setVisitCause(visitCause);
        tumour.setGrading(grading);
        tumour.setPatientEnlightenStatus(patientEnglihtenStatus);
        tumour.setICD10(diagnosis.getDiagnosisCode());
        tumour.setDiagnosisText(diagnosis.getText());
        tumour.setDiagnosisTextCatalogue(diagnosis.getICDCatalogType().value());

        tumour = tumourLocalisationGenerator.generateTumourLocalisationsAndAddToTumour(tumour);
        String localisationText = generateLocalisationText(tumour.getTumourLocalisation());
        tumour.setLocalisationText(localisationText);


        tumour = progressGenerator.generateProgressListAndAddToTumour(tumour, patient.getPatientDataSet());

        DateType admissionDate = generateAdmissionDate(tumour.getProgress());
        tumour.setAdmissionDate(admissionDate);
        tumour = metastasisGenerator.generateMetastasisAndAddToTumour(tumour);
        tumour = histologyGenerator.generateHistologyAndAddToTumour(tumour);
        tumour = tnmGenerator.generateTnmAndAddToTumour(tumour);
        tumour = molecularMarkerGenerator.generateMolecularMarkersAndAddToTumour(tumour, patient.getPatientAdditionalInfo());


        return tumour;

    }


    private DateType generateDiagnosisDate (DiagnosisType diagnosis) {
        return diagnosis.getDate();
    }

    private String generateIcd10 (DiagnosisType diagnosis) {
        return diagnosis.getDiagnosisCode();
    }

    private String generateDiagnosisText (DiagnosisType diagnosis) {
        return diagnosis.getText();
    }

    private String generateEpisodeRef(DiagnosisType diagnosis) {
        return diagnosis.getEpisodeRef();
    }

    private TumourCaptureCauseGTDSEnumType generateCaptureCause(){
        return randomOperations.getRandomFromEnum(TumourCaptureCauseGTDSEnumType.class, "CaptureCause", "Tumour");
    }

    private YesNoXGTDSEnumType generateCaptureFinishState(){
        return randomOperations.getRandomFromEnum(YesNoXGTDSEnumType.class, "", "FinishState");
    }

    private CodeTypeGTDSEnumType generateCodeType() {
        return randomOperations.getRandomFromEnum(CodeTypeGTDSEnumType.class, "Tumour", "CodeType");
    }

    private ExpansionGeneralGTDSEnumType generateExpansionGeneral() {
        return randomOperations.getRandomFromEnum(ExpansionGeneralGTDSEnumType.class, "TumourExpansion", "");
    }

    private SourceTypeGTDSEnumType generateSource() {
        return randomOperations.getRandomFromEnum(SourceTypeGTDSEnumType.class, "TumourSource", "");
    }

    private VisitCauseGTDSEnumType generateVisitCause() {
        return randomOperations.getRandomFromEnum(VisitCauseGTDSEnumType.class, "VisitCauseRef", "");
    }

    private String generateGrading() {
        return randomOperations.getRandomFromEnum(GradingEnum.class, "Grading", "").getDefition();
    }

    private PatientEnlightenStatusGTDSEnumType generatePatientEnglihtenStatus () {
        return randomOperations.getRandomFromEnum(PatientEnlightenStatusGTDSEnumType.class, "EnlightenStatus", "");
    }

    private String generateTumourId(){
        return idGenerator.generateId();
    }

    private String generateLocalisationText(List<GTDSTumourLocalisationEIType> tumourLocalisationList){

        String localisationText = null;

        for (GTDSTumourLocalisationEIType tumourLocalisation : tumourLocalisationList){
             localisationText = tumourLocalisation.getLocalisationText();
             if (localisationText != null){
                 break;
             }
        }

        return localisationText;

    }

    private DateType generateAdmissionDate(List<GTDSProgressEIType> progressList) {

        DateType minDate = progressList.get(0).getExaminationDate();
        for(GTDSProgressEIType progress : progressList){
            if(progress.getExaminationDate().getDate().compare(minDate.getDate()) == DatatypeConstants.LESSER) {
                minDate = progress.getExaminationDate();
            }
        }

        return minDate;

    }

}
