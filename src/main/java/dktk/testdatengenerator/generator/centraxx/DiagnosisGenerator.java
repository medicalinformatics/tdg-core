package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.*;
import dktk.testdatengenerator.customelements.Patient;
import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.db.ICDCodeQueries;
import dktk.testdatengenerator.db.model.ICDCode;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.enums.AgeGroupEnum;
import dktk.testdatengenerator.enums.ICDCodeGroups;
import dktk.testdatengenerator.exceptions.DateGeneratorException;
import dktk.testdatengenerator.util.RandomOperations;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by brennert on 03.07.2017.
 */
public class DiagnosisGenerator {

    private TDGProfile tdgProfile;
    private DateGenerator dateGenerator;
    private RandomOperations randomOperations;
    private TumourGenerator tumourGenerator;
    private ICDCodeQueries icdCodeQueries;
    private IdGenerator idGenerator;

    private final String DIAGNOSIS_MIN = "diagnosisMin";
    private final String DIAGNOSIS_MAX = "diagnosisMax";


    public DiagnosisGenerator(TDGProfile tdgProfile, DBConnector dbConnector) {

        this.tdgProfile = tdgProfile;
        dateGenerator = new DateGenerator();
        randomOperations = new RandomOperations(tdgProfile);
        tumourGenerator = new TumourGenerator(tdgProfile, dbConnector);
        icdCodeQueries = new ICDCodeQueries(dbConnector);
        idGenerator = new IdGenerator(tdgProfile);

    }

    public Patient generateDiagnosisAndAddToPatient (Patient patient){

        if (patient != null) {

            List<DiagnosisType> diagnosisList = patient.getPatientDataSet().getDiagnosis();

            int numberOfDiagnosis = getNumberOfDiagnosisToBeCreated();
            for (int i = 0; i < numberOfDiagnosis; i++) {

                DiagnosisType diagnosis = generateDiagnosis(patient);
                if (diagnosis != null) {
                    diagnosisList.add(diagnosis);
                }

            }

        }

        return patient;

    }

    private int getNumberOfDiagnosisToBeCreated(){
        return randomOperations.generateNumberOfItemsToBeGenerated(DIAGNOSIS_MIN, DIAGNOSIS_MAX);
    }

    private DiagnosisType generateDiagnosis (Patient patient) {

        DiagnosisType diagnosis = null;
        AgeGroupEnum ageGroup = getPatientAgeGroup(getPatientAge(patient.getPatientDataSet()));
        AgeGroupEnum sickeningAge = getSickeningAge(patient.getPatientDataSet(), ageGroup);

        if (sickeningAge == null){
            sickeningAge = ageGroup;
        }

        if (sickeningAge != null) {

            diagnosis = new DiagnosisType();

            String diagnosisId = generateDiagnosisId();
            Boolean admission = generateAdmission();
            Boolean discharge = generateDischarge();
            Boolean medDepartment = generateMedDepartment();
            Boolean postOperative = generatePostOperative();
            Boolean transfer = generateTransfer();

            diagnosis.setDiagnosisId(diagnosisId);
            setDiagnosisDate(patient.getPatientDataSet(), diagnosis, sickeningAge);

            diagnosis = setICDCode(diagnosis, patient.getPatientDataSet());
            diagnosis.setAdmission(admission);
            diagnosis.setDischarge(discharge);
            diagnosis.setMedDepartment(medDepartment);
            diagnosis.setPostOperative(postOperative);
            diagnosis.setTransfer(transfer);

            diagnosis = tumourGenerator.generateTumourAndAddToDiagnosis(diagnosis, patient);

        }

        return diagnosis;

    }

    private String generateDiagnosisId(){
        return idGenerator.generateId();
    }

    private Boolean generateAdmission(){
        return ThreadLocalRandom.current().nextDouble(0, 1) <= tdgProfile.getProbability("pAdmissionDiagnosis").getProbability();
    }

    private Boolean generateDischarge(){
        return ThreadLocalRandom.current().nextDouble(0, 1) <= tdgProfile.getProbability("pDischargeDiagnosis").getProbability();
    }

    private Boolean generateMedDepartment(){
        return ThreadLocalRandom.current().nextDouble(0, 1) <= tdgProfile.getProbability("pMedDepartmentDiagnosis").getProbability();
    }

    private Boolean generatePostOperative(){
        return ThreadLocalRandom.current().nextDouble(0, 1) <= tdgProfile.getProbability("pPostOperativeDiagnosis").getProbability();
    }

    private Boolean generateTransfer(){
        return ThreadLocalRandom.current().nextDouble(0, 1) <= tdgProfile.getProbability("pTransferDiagnosis").getProbability();
    }

    private AgeGroupEnum getSickeningAge (PatientDataSetType patient, AgeGroupEnum ageGroup){

        //TODO: @Torben: Kannst Du es erklären?
        double probability = tdgProfile.getProbability("allPatientsCancer").getProbability();

        return (probability == 1.0) ?
            getPatientAgeGroupThenSickeningOnCancer(patient, ageGroup) :
            getPatientAgeGroupThenSickening(patient, ageGroup);

    }

    private DiagnosisType setICDCode(DiagnosisType diagnosis, PatientDataSetType patient) {

        double randomNumber = ThreadLocalRandom.current().nextDouble(0, 1);
        double probabilitySum = 0.0;
        ICDCode randomICD = null;
        for (ICDCodeGroups icdGroup : ICDCodeGroups.values()) {
            probabilitySum += icdCodeQueries.getICDCodeProbability(patient.getMasterdata().getGender(), icdGroup);
            if (randomNumber <= probabilitySum) {
                randomICD = getRandomICD(icdCodeQueries.getICDCodeGroupValues(icdGroup));
                break;
            }
        }
        if (randomICD == null) {
            randomICD = getRandomICD(icdCodeQueries.getMissingICDCodes());
        }
        if(randomICD != null){

            IcdEntryRefType icdEntryRef = generateIcdEntryRef(randomICD);

            diagnosis.setDiagnosisCode(randomICD.getCode());
            diagnosis.setText(randomICD.getDescription());
            // CXX: DIAG_DIAGNOSISCODE
            diagnosis.setIcdEntryRef(icdEntryRef);
            diagnosis.setICDCatalogType(CatalogueKindEnumType.ICD_GENERAL);

        }

        return diagnosis;

    }

    private IcdEntryRefType generateIcdEntryRef (ICDCode icdCode){

        // TODO: This block needed?
        IcdEntryRefType icdEntryRefType = new IcdEntryRefType();

        icdEntryRefType.setCode(icdCode.getCode());
        icdEntryRefType.setKind("code");
        IcdCatalogRefType icdCatalogRefType = new IcdCatalogRefType();
        icdCatalogRefType.setKind("ICD-General");
        icdCatalogRefType.setVersion("2017");
        icdEntryRefType.setIcdCatalogRef(icdCatalogRefType);

        return icdEntryRefType;

    }

    private ICDCode getRandomICD(ArrayList<ICDCode> icdCodeGroupValues) {

        int randomNumber = ThreadLocalRandom.current().nextInt(0, icdCodeGroupValues.size() - 1);
        return icdCodeGroupValues.get(randomNumber);

    }

    /**
     * This method should set the diagnosis date for a patient.
     *
     * @param diagnosis
     * @param sickeningAge
     */
    private void setDiagnosisDate(PatientDataSetType patient, DiagnosisType diagnosis, AgeGroupEnum sickeningAge) {

        PatientMasterdataType patientMasterData = patient.getMasterdata();
        GregorianCalendar gregorianCalendar = patientMasterData.getDateOfBirth().getDate().toGregorianCalendar();
        gregorianCalendar.roll(GregorianCalendar.YEAR, sickeningAge.getMin());
        Date minimumDate = gregorianCalendar.getTime();
        gregorianCalendar.roll(GregorianCalendar.YEAR, (sickeningAge.getMax() - sickeningAge.getMin()));
        Date maximumDate = gregorianCalendar.getTime();

        if (patientMasterData.getDateOfDeath() != null)
            if (maximumDate.compareTo(patientMasterData.getDateOfDeath().getDate().toGregorianCalendar().getTime()) == DatatypeConstants.GREATER)
                maximumDate = patientMasterData.getDateOfDeath().getDate().toGregorianCalendar().getTime();
        if (new Date().compareTo(maximumDate) == DatatypeConstants.LESSER)
            maximumDate = new Date();

        DateType diagnosisDate = null;
        try{
            diagnosisDate = dateGenerator.generateDateBetween(minimumDate, maximumDate);
        }catch (DateGeneratorException dge){
            dge.printStackTrace();
        }
        // CXX: DIAG_DIAG_DATE
        diagnosis.setDate(diagnosisDate);
    }

    private YesNoXGTDSEnumType getRandomEarlierTumors() {

        YesNoXGTDSEnumType yesNoXValue = randomOperations.getRandomFromEnum(YesNoXGTDSEnumType.class, "","EarlierTumours");
        return yesNoXValue;

    }

    /**
     * This method returns the ageGroup in which a person sickens. </br>
     * It is also possible that a person doesn't sicken.
     *
     * @param patient
     * @param age
     * @return ageGroup or null
     */
    private AgeGroupEnum getPatientAgeGroupThenSickening(PatientDataSetType patient, AgeGroupEnum age) {

        PatientMasterdataType patientMasterdata = patient.getMasterdata();
        if(patientMasterdata.getGender() == GenderTypeEnum.FEMALE || patientMasterdata.getGender() == GenderTypeEnum.MALE){
            double random = ThreadLocalRandom.current().nextDouble(0, 1);
            for (AgeGroupEnum ageGroup : AgeGroupEnum.values()) {
                if(ageGroup.getMin() >= age.getMax()) {
                    break;
                }
                if (random <= tdgProfile.getProbability("p" + patientMasterdata.getGender() + "Tumour" + ageGroup.name()).getProbability()) {
                    return ageGroup;
                }
            }
        }
        return null;
    }

    /**
     * This method should return the Age Group then the patient will sicken on cancer. </br>
     *
     * @param patient dataset
     * @param age     of the patient
     * @return AgeGroupEnum for the Age then patient sickened. If not sickening return null.
     */
    private AgeGroupEnum getPatientAgeGroupThenSickeningOnCancer(PatientDataSetType patient, AgeGroupEnum age) {

        PatientMasterdataType patientMasterdata = patient.getMasterdata();
        if (patientMasterdata.getGender() == GenderTypeEnum.FEMALE || patientMasterdata.getGender() == GenderTypeEnum.MALE) {
            double probabilitySum = tdgProfile.querySum(AgeGroupEnum.class, age, "p" + patientMasterdata.getGender() + "Tumour", "");
            double multiplyFactor = 1 / probabilitySum;
            probabilitySum = 0.0;
            double random = ThreadLocalRandom.current().nextDouble(0, 1);
            for (AgeGroupEnum ageGroup : AgeGroupEnum.values()) {
                probabilitySum = probabilitySum + (multiplyFactor * tdgProfile.getProbability("p" + patientMasterdata.getGender() + "Tumour" + ageGroup.name()).getProbability());
                if (random <= probabilitySum)
                    return ageGroup;
            }
            return null;
        }
        return age;
    }

    /**
     * This method gets the age of a patient.
     *
     * @param patient the patient
     * @return the age of the patient
     */
    private int getPatientAge(PatientDataSetType patient) {

        PatientMasterdataType patientMasterdata = patient.getMasterdata();
        XMLGregorianCalendar dateOfBirth = patientMasterdata.getDateOfBirth().getDate();
        XMLGregorianCalendar lastLivingDate = null;
        if (patientMasterdata.getDateOfDeath() == null) {
            GregorianCalendar gregorianCalendar = new GregorianCalendar();
            gregorianCalendar.setTime(new Date());
            try {
                lastLivingDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
            } catch (DatatypeConfigurationException dce) {
                System.out.println("Couldnt initialize XML Gregorian Calendar");
                dce.printStackTrace();
            }
        } else {
            lastLivingDate = patientMasterdata.getDateOfDeath().getDate();
        }
        return lastLivingDate.getYear() - dateOfBirth.getYear();
    }

    /**
     * This method converts a age into an age group
     *
     * @param patientAge
     * @return AgeGroupEnum value or null then the patient age group couldn't be determined
     */
    private AgeGroupEnum getPatientAgeGroup(int patientAge) {
        for (AgeGroupEnum ageGroup : AgeGroupEnum.values()) {
            if (ageGroup.getMin() <= patientAge && patientAge <= ageGroup.getMax())
                return ageGroup;
        }
        return null;
    }

}
