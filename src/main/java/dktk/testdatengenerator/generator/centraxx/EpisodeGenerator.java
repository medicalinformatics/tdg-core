package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.*;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.exceptions.DateGeneratorException;
import dktk.testdatengenerator.util.RandomOperations;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by brennert on 11.07.2017.
 */
public class EpisodeGenerator {
    private TDGProfile tdgProfile;
    private DateGenerator dateGenerator;
    private RandomOperations randomOperations;
    private int episodeCounter;

    public EpisodeGenerator(TDGProfile tdgProfile) {
        dateGenerator = new DateGenerator();
        randomOperations = new RandomOperations(tdgProfile);
        this.tdgProfile = tdgProfile;
    }

    public ArrayList<EpisodeType> generate(PatientDataSetType patient) {
        ArrayList<EpisodeType> episodes = new ArrayList<>();
        List<DiagnosisType> diagnosisList = patient.getDiagnosis();

        int numberOfEpisodesToBeGenerated = getMinNumberOfEpisodesToBeGenerated();

        for (int i=0; i< numberOfEpisodesToBeGenerated; i++ ) {
            episodeCounter++;
            EpisodeType episode = generateEpisode(patient);
            // episode.setHabitationRef(randomOperations.getRandomFromEnum(OrganisationUnitsEnum.class, "", "").name());
            CatalogueDataRefType habitationCatalogueType = new CatalogueDataRefType();
            habitationCatalogueType.setValue("CENTRAXX");
            episode.setHabitationRef(habitationCatalogueType);
            episodes.add(episode);
        }

        int maxNumberOfEpisodesToBeGenerated = getMaxNumberOfEpisodesToBeGenerated();

        if (episodeCounter < maxNumberOfEpisodesToBeGenerated) {

            for (int i = episodes.size(); i < maxNumberOfEpisodesToBeGenerated; i++) {
                double random = ThreadLocalRandom.current().nextDouble(0, 1);
                if (random <= tdgProfile.getProbability("pOneMoreEpisode").getProbability()) {
                    episodeCounter++;
                    EpisodeType episode = generateEpisode(patient);
                    // episode.setHabitationRef(randomOperations.getRandomFromEnum(OrganisationUnitsEnum.class, "","").name());

                    CatalogueDataRefType habitationCatalogueType = new CatalogueDataRefType();
                    habitationCatalogueType.setValue("CENTRAXX");
                    episode.setHabitationRef(habitationCatalogueType);

                    episodes.add(episode);
                }
            }
        }

        return episodes;
    }

    private int getMinNumberOfEpisodesToBeGenerated(){


        Double episodeMin = tdgProfile.getProbability("episodeMin").getProbability();
        return episodeMin.intValue();

//        return 1;
    }

    private int getMaxNumberOfEpisodesToBeGenerated(){


        Double episodeMax = tdgProfile.getProbability("episodeMax").getProbability();
        return episodeMax.intValue();

//        return 3;

    }

    private EpisodeType generateEpisode(PatientDataSetType patient) {
        EpisodeType episode = new EpisodeType();
        episode.setSource(patient.getSource());
        episode.setEpisodeRef(String.valueOf(episodeCounter));

        CatalogueDataRefType stayTypeCatalogueRef = new CatalogueDataRefType();
        stayTypeCatalogueRef.setValue("GTDS");
        episode.setStayTypeRef(stayTypeCatalogueRef);

        if (patient.getMasterdata().getDateOfDeath() != null){
            DateType validFrom = null;
            try{
                validFrom = dateGenerator.generateDateBetween(patient.getMasterdata().getDateOfBirth(), patient.getMasterdata().getDateOfDeath());
            }catch (DateGeneratorException dge){
                dge.printStackTrace();
            }
            episode.setValidFrom(validFrom);
        }
        else {
            DateType validFrom = null;
            try {
                DateType currentDate = new DateType();
                currentDate.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()));
                currentDate.setPrecision(DatePrecision.DAY);
                validFrom = dateGenerator.generateDateBetween(patient.getMasterdata().getDateOfBirth(), currentDate);
            } catch (DatatypeConfigurationException dce) {
                System.out.println("Couldn't create gregorian calendar for current date");
            } catch (DateGeneratorException dge){
                dge.printStackTrace();
            }
            episode.setValidFrom(validFrom);
        }

        double episodeMinLength = tdgProfile.getProbability("episodeMinLength").getProbability();
        double episodeMaxLength = tdgProfile.getProbability("episodeMaxLength").getProbability();

        int timeSpan = ThreadLocalRandom.current().nextInt((int) episodeMinLength, (int) episodeMaxLength);

        GregorianCalendar gregorianCalendar = episode.getValidFrom().getDate().toGregorianCalendar();
        gregorianCalendar.add(GregorianCalendar.DAY_OF_MONTH, timeSpan);
        DateType validUntil = new DateType();
        validUntil.setPrecision(DatePrecision.DAY);
        try {
            validUntil.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
        } catch (DatatypeConfigurationException dce) {
            System.out.println("Couldn't convertAndAdd the date for validUntil to DateType");
        }
        episode.setValidUntil(validUntil);

        return episode;
    }

    private EpisodeType generateEpisodeFromDiagnosis(DiagnosisType diagnosisType) {

        EpisodeType episode = new EpisodeType();
        episode.setSource(diagnosisType.getSource());
        episode.setEpisodeRef(String.valueOf(episodeCounter));
        // This cant be random generated because only patients with habitation ref centraxx are visible
        // episode.setHabitationRef(randomOperations.getRandomFromEnum(OncologyOrganisationUnitsEnum.class, "","Tumour").name());
        CatalogueDataRefType stayTypeCatalogueRef = new CatalogueDataRefType();
        stayTypeCatalogueRef.setValue("GTDS");
        episode.setStayTypeRef(stayTypeCatalogueRef);

        CatalogueDataRefType habitationCatalogueRef = new CatalogueDataRefType();
        habitationCatalogueRef.setValue("CENTRAXX");
        episode.setHabitationRef(habitationCatalogueRef);

        GregorianCalendar gregorianCalendar = diagnosisType.getDate().getDate().toGregorianCalendar();

        double episodeMinLength = tdgProfile.getProbability("episodeMinLength").getProbability();
        double episodeMaxLength = tdgProfile.getProbability("episodeMaxLength").getProbability();
        int randomTimeFrame = ThreadLocalRandom.current().nextInt((int) episodeMinLength, (int) episodeMaxLength);
        int beforeTime = ThreadLocalRandom.current().nextInt(0, randomTimeFrame);
        gregorianCalendar.add(GregorianCalendar.DAY_OF_MONTH, -1 * beforeTime);
        DateType validFrom = new DateType();
        validFrom.setPrecision(DatePrecision.DAY);
        try {
            validFrom.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
        } catch (DatatypeConfigurationException dce) {
            System.out.println("Couldn't convertAndAdd the date for validFrom to DateType");
        }
        episode.setValidFrom(validFrom);
        gregorianCalendar.add(GregorianCalendar.DAY_OF_MONTH, (int) (episodeMaxLength-episodeMinLength));
        DateType validUntil = new DateType();
        validUntil.setPrecision(DatePrecision.DAY);
        try {
            validUntil.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
        } catch (DatatypeConfigurationException dce) {
            System.out.println("Couldn't convertAndAdd the date for validUntil to DateType");
        }
        episode.setValidUntil(validUntil);

        return episode;
    }
}
