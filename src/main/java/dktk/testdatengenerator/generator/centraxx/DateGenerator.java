package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.DatePrecision;
import de.kairos_med.DateType;
import dktk.testdatengenerator.exceptions.DateGeneratorException;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by BrennerT on 20.06.2017.
 */
public class DateGenerator {

    private GregorianCalendar gregorianCalendar;

    private Date minDate;

    private Date currentDate;
    DateGenerator() {
        gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(1900, 00, 01, 00, 00, 00);
        minDate = gregorianCalendar.getTime();
        currentDate = new Date();
    }

    /**
     * This generates a random DateType.
     * The Date and Precision of this Type should be random.
     *
     * @return
     */
    public DateType generate() {
        return generate(getRandomPrecision());
    }

    public DateType generate(DatePrecision datePrecision) {

        Date randomDate = new Date();
        long randomTime = ThreadLocalRandom.current().nextLong(minDate.getTime(), currentDate.getTime());
        randomDate.setTime(randomTime);
        DateType dateType = new DateType();
        dateType.setPrecision(datePrecision);
        gregorianCalendar.setTime(getFormattedDate(randomDate, dateType.getPrecision()));
        dateType.setDate(getXmlGregorianCalendar());
        return dateType;

    }

    /**
     * This generates a DateType which Date is after the date given as parameter.</br>
     * If the parameter date is after than the current date the result will be null.</br>
     * The DateType will have the precision exact
     *
     * @param parameterDate
     * @return
     */
    public DateType generateDateAfter(DateType parameterDate) throws DateGeneratorException{

        if (checkAfter(parameterDate))
            throw new DateGeneratorException("The parameter date was after the current date");

        DateType dateType = generateDateAfter_WithoutExceptionManagement(parameterDate);


        return dateType;
    }

    private DateType generateDateAfter_WithoutExceptionManagement(DateType parameterDate) throws DateGeneratorException{

        long minTime = parameterDate.getDate().toGregorianCalendar().getTime().getTime();
        long maxTime = new Date().getTime();

        DateType dateType = new DateType();
        dateType.setPrecision(parameterDate.getPrecision());

        long randomTime = ThreadLocalRandom.current().nextLong(minTime, maxTime);
        gregorianCalendar.setTime(getFormattedDate(new Date(randomTime), dateType.getPrecision()));
        dateType.setDate(getXmlGregorianCalendar());

        checkDataType(randomTime, dateType, parameterDate);
        
        return dateType;
    }

    private void checkDataType ( long randomTime, DateType dateType, DateType parameterDate){

        if(checkAfter(dateType))
            // TODO: substitute with exception
            System.out.println("The generated date is after current date. Here is date Time: " + parameterDate.getDate().toGregorianCalendar().getTime().getTime() + " Here is random time: " + randomTime);
        if(checkBefore(dateType))
            System.out.println("The generated date is before 1900. Here is date Time: " + parameterDate.getDate().toGregorianCalendar().getTime().getTime() + " Here is random time: " + randomTime);

    }


    public DateType genereateDateAfter(Date beforeDate) throws DateGeneratorException{
        DateType beforeDateType = new DateType();

        gregorianCalendar.setTime(beforeDate);
        beforeDateType.setDate(getXmlGregorianCalendar());
        beforeDateType.setPrecision(getRandomPrecision());

        return generateDateAfter(beforeDateType);
    }

    private boolean checkAfter(DateType parameterDate) {
        gregorianCalendar.setTime(new Date());
        if (gregorianCalendar.getTime().compareTo(parameterDate.getDate().toGregorianCalendar().getTime()) == DatatypeConstants.LESSER)
            return true;
        return false;
    }

    /**
     * This generates a Date which is before the date given as parameter.</br>
     * If the parameter date is before the minimum Date (1900-01-01T00:00:00) the result will be null.</br>
     *
     * @param date
     * @return
     */
    public DateType generateDateBefore(DateType date) throws DateGeneratorException {
        if (checkBefore(date))
            throw new DateGeneratorException("The first date was before the 01.01.1900");
        long minTime = gregorianCalendar.getTime().getTime();
        long maxTime = date.getDate().toGregorianCalendar().getTime().getTime();
        long randomTime = ThreadLocalRandom.current().nextLong(minTime, maxTime);
        DateType dateType = new DateType();
        dateType.setPrecision(date.getPrecision());
        gregorianCalendar.setTime(getFormattedDate(new Date(randomTime), dateType.getPrecision()));
        dateType.setDate(getXmlGregorianCalendar());
        if(checkAfter(dateType))
            System.out.println("The generated date is after current date. Here is date Time: " + date.getDate().toGregorianCalendar().getTime().getTime() + " Here is random time: " + randomTime);
        if(checkBefore(dateType))
            System.out.println("The generated date is before 1900. Here is date Time: " + date.getDate().toGregorianCalendar().getTime().getTime() + " Here is random time: " + randomTime);
        return dateType;
    }

    public DateType generateDateBefore(Date date) throws DateGeneratorException{
        DateType afterDateType = new DateType();

        gregorianCalendar.setTime(date);
        afterDateType.setDate(getXmlGregorianCalendar());
        afterDateType.setPrecision(getRandomPrecision());

        return generateDateBefore(afterDateType);
    }

    /**
     * This method generates a {@link DateType} which has a {@link DateType#date} set between the two parameter {@code DateTypes}.
     * @param beforeDate
     * @param afterDate
     * @return
     * @throws DateGeneratorException
     *          then the before date is before 01.01.1900
     *          then the after date is later than the current date
     *          then the before date is after the after date
     */
    public DateType generateDateBetween(DateType beforeDate, DateType afterDate) throws DateGeneratorException {
        if (checkBefore(beforeDate))
            throw new DateGeneratorException("The first date was before the 01.01.1900");
        if (checkAfter(afterDate))
            throw new DateGeneratorException("The second date was after the current date");
        if (beforeDate.getDate().compare(afterDate.getDate()) == DatatypeConstants.GREATER)
            throw new DateGeneratorException("The second date was before the first date");
        long minTime = beforeDate.getDate().toGregorianCalendar().getTimeInMillis();
        long maxTime = afterDate.getDate().toGregorianCalendar().getTimeInMillis();
        long randomTime = ThreadLocalRandom.current().nextLong(minTime, maxTime);

        DateType dateType = new DateType();
        dateType.setPrecision(beforeDate.getPrecision());
        gregorianCalendar.setTime(getFormattedDate(new Date(randomTime), dateType.getPrecision()));
        dateType.setDate(getXmlGregorianCalendar());
        if(checkAfter(dateType))
            System.out.println("The generated date is after current date. Here is beforeDate Time: " + beforeDate.getDate().toGregorianCalendar().getTime() + " Here is afterDate Time: " + afterDate.getDate().toGregorianCalendar().getTime() + " Here is random time: " + new Date(randomTime));
        if(checkBefore(dateType))
            System.out.println("The generated date is before 1900. Here is beforeDate Time: " + beforeDate.getDate().toGregorianCalendar().getTime() + " Here is afterDate Time: " + afterDate.getDate().toGregorianCalendar().getTime() + " Here is random time: " + new Date(randomTime));
        return dateType;
    }

    /**
     * This method does the same as {@link DateGenerator#generateDateBetween(DateType, DateType)} only with other parameter type
     * @param beforeDate
     * @param afterDate
     * @return
     * @throws DateGeneratorException
     */
    public DateType generateDateBetween(Date beforeDate, Date afterDate) throws DateGeneratorException{
        DateType beforeDateType = new DateType();
        DateType afterDateType = new DateType();

        gregorianCalendar.setTime(beforeDate);
        beforeDateType.setDate(getXmlGregorianCalendar());
        beforeDateType.setPrecision(getRandomPrecision());

        gregorianCalendar.setTime(afterDate);
        afterDateType.setDate(getXmlGregorianCalendar());
        afterDateType.setPrecision(beforeDateType.getPrecision());

        return generateDateBetween(beforeDateType, afterDateType);
    }

    /**
     * This method generates a {@link DateType} which has a {@link DateType#date} set between the two parameter {@code DateTypes}.
     * It doesn't recognize the date bounds of 01.01.1900 and the current date
     * @param beforeDate
     * @param afterDate
     * @return
     * @throws DateGeneratorException
     *          then the beforeDate is after the afterDate
     */
    public DateType generateDateBetweenWithNoBounds(DateType beforeDate, DateType afterDate) throws DateGeneratorException {

        if (beforeDate.getDate().compare(afterDate.getDate()) == DatatypeConstants.GREATER)
            throw new DateGeneratorException("The second date was before the first date");
        long minTime = beforeDate.getDate().toGregorianCalendar().getTimeInMillis();
        long maxTime = afterDate.getDate().toGregorianCalendar().getTimeInMillis();
        long randomTime = ThreadLocalRandom.current().nextLong(minTime, maxTime);

        DateType dateType = new DateType();
        dateType.setPrecision(beforeDate.getPrecision());
        gregorianCalendar.setTime(getFormattedDate(new Date(randomTime), dateType.getPrecision()));
        dateType.setDate(getXmlGregorianCalendar());

        return dateType;
    }

    /**
     * This method does the same as {@link DateGenerator#generateDateBetweenWithNoBounds(DateType, DateType)}
     * @param beforeDate
     * @param afterDate
     * @return
     * @throws DateGeneratorException
     */
    public DateType generateDateBetweenWithNoBounds(Date beforeDate, Date afterDate) throws DateGeneratorException{
        DateType beforeDateType = new DateType();
        DateType afterDateType = new DateType();

        gregorianCalendar.setTime(beforeDate);
        beforeDateType.setDate(getXmlGregorianCalendar());
        beforeDateType.setPrecision(getRandomPrecision());

        gregorianCalendar.setTime(afterDate);
        afterDateType.setDate(getXmlGregorianCalendar());
        afterDateType.setPrecision(beforeDateType.getPrecision());

        return generateDateBetweenWithNoBounds(beforeDateType, afterDateType);
    }

    /**
     * Converts the current GregorianCalender into an XMLGregorianCalender
     *
     * @return
     */
    private XMLGregorianCalendar getXmlGregorianCalendar() {
        XMLGregorianCalendar xmlGregorianCalendar = null;
        try {
            xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        } catch (DatatypeConfigurationException dce) {
            System.out.println("Couldn't create new XMLGregorianCalendar");
            dce.printStackTrace();
        }
        return xmlGregorianCalendar;
    }

    /**
     * Converts a Date dependent to its DatePrecision.
     *
     * @param randomDate
     * @param datePrecision
     * @return
     */
    private Date getFormattedDate(Date randomDate, DatePrecision datePrecision) {
        DateFormat dateFormat = null;
        switch (datePrecision) {
            case DAY:
                dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                break;
            case YEAR:
                dateFormat = new SimpleDateFormat("yyyy");
                break;
            case MONTH:
                dateFormat = new SimpleDateFormat("yyyy/MM");
                break;
            default:
                dateFormat = new SimpleDateFormat("yyyy/MM/dd hh/mm/ss/SS");
                break;
        }
        String formatString = dateFormat.format(randomDate);
        Date parsedDate = null;
        try {
            parsedDate = dateFormat.parse(formatString);
        } catch (ParseException pe) {
            System.out.println("Some error occurred while parsing DateString");
            pe.printStackTrace();
        }
        return parsedDate;
    }

    private DatePrecision getRandomPrecision() {
        DatePrecision datePrecision = null;
        int random = ThreadLocalRandom.current().nextInt() % 4;
        if (Integer.signum(random) == -1)
            random = random * (-1);
        switch (random) {
            case 0:
                datePrecision = DatePrecision.DAY;
                break;
            case 1:
                datePrecision = DatePrecision.MONTH;
                break;
            case 2:
                datePrecision = DatePrecision.YEAR;
                break;
            case 3:
                datePrecision = DatePrecision.EXACT;
                break;
        }
        return datePrecision;
    }

    private boolean checkBefore(DateType parameterDate) {
        gregorianCalendar.set(1900, Calendar.JANUARY, 0, 0, 0, 0);
        if (gregorianCalendar.getTime().getTime() > parameterDate.getDate().toGregorianCalendar().getTime().getTime()){
            System.out.println("The check before method returns true for date: " + parameterDate.getDate().toGregorianCalendar().getTime().getTime() + " and gregorian calendar is: " + gregorianCalendar.getTime().getTime());
            return true;
        }
        return false;
    }

    // getter setter section

    public Date getMinDate() {
        return minDate;
    }

    public GregorianCalendar getMinDateAsGregorianCalendar(){
        return convertTo(minDate);
    }

    private GregorianCalendar convertTo (Date date){
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTimeInMillis(date.getTime());
        return gregorianCalendar;
    }
}
