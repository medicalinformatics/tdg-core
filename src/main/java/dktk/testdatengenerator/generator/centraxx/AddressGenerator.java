package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.PatientAddressType;
import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.exceptions.AddressGeneratorException;
import dktk.testdatengenerator.http.NominatiReverseRequest;
import dktk.testdatengenerator.http.NominatiReverseRequestException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//import javax.xml.ws.http.HTTPException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * This class generates a given number of random addresses for the hospitals in the database.</br>
 * The constructor Accepts a maxAmount of Patients as an Integer.</br>
 * For each hospital the generator will generateTnmAndAddToTumour a AddressList of this size.
 * The generator should start with the hospital DKTK-Testdatagenerator and should work in a round robin manner.
 * Created by brennert on 27.06.2017.
 */
public class AddressGenerator {

    /**
     * this stores the ArrayList for each hospital
     */
    private Map<String, ArrayList<PatientAddressType>> addressListMapHospitals;
    /**
     * This stores latitude and longitude for hospitals
     */
    private HashMap<String, HospitalLocation> coordinatesMapHospitals;
    private int maxAmountPatients;
    private DBConnector dbConnector;

    private static final Logger logger = LogManager.getLogger(AddressGenerator.class);


    public AddressGenerator(int maxAmountAddresses, DBConnector dbConnector) throws AddressGeneratorException {
        this(maxAmountAddresses, null, dbConnector);
    }

    public AddressGenerator(int maxAmountAddresses, String[] hospitals, DBConnector dbConnector) throws AddressGeneratorException {

        this.maxAmountPatients = maxAmountAddresses;
        coordinatesMapHospitals = new HashMap<>();
        addressListMapHospitals = new ConcurrentHashMap<>();
        this.dbConnector = dbConnector;

        if (hospitals != null){
            initializeMaps(hospitals);
        }else{
            initializeMaps();
        }

        createWorkProcess();

    }

    /**
     * This method initializes the hashmaps with all hospitals available in the database.
     */
    private void initializeMaps() {

        try{
            ResultSet resultSet = dbConnector.sendQuery("SELECT * FROM hospital;");
            while (resultSet.next()) {
                addressListMapHospitals.put(resultSet.getString("name"), new ArrayList<>());
                HospitalLocation hospitalLocation = new HospitalLocation(resultSet.getDouble("latitude"), resultSet.getDouble("longitude"), 0.2);
                coordinatesMapHospitals.put(resultSet.getString("name"), hospitalLocation);
            }
            resultSet.close();
        } catch (SQLException sqle) {
            System.out.println("Couldn't request locations for hospitals from database");
            sqle.printStackTrace();
        }

    }

    /**
     * This methods initializes the hashmaps for a given list of hospitals. </br>
     *
     * @param hospitals a list of hospital names
     * @throws AddressGeneratorException then hospital was not found
     */
    private void initializeMaps(String[] hospitals) throws AddressGeneratorException {
        try {
            String queryString = hospitalsToString(hospitals);
            ResultSet resultSet = dbConnector.sendQuery(queryString);
            if (!resultSet.isBeforeFirst())
                throw new AddressGeneratorException("Couldn't find any of the given hospitals in database");
            while (resultSet.next()) {
                if (resultSet.getString("name") == null)
                    throw new AddressGeneratorException("One of the given hospitals was not found in database");
                addressListMapHospitals.put(resultSet.getString("name"), new ArrayList<>());
                HospitalLocation hospitalLocation = new HospitalLocation(resultSet.getDouble("latitude"), resultSet.getDouble("longitude"), 0.2);
                coordinatesMapHospitals.put(resultSet.getString("name"), hospitalLocation);
            }
            resultSet.close();
        } catch (SQLException sqle) {
            System.out.println("Couldn't request locations for hospitals from database");
            sqle.printStackTrace();
        }

    }

    /**
     * converts a given array of hospitals into a string for a database query
     *
     * @param hospitals
     * @return
     */
    private String hospitalsToString(String[] hospitals) {
        String hospitalString = "(";
        for (String h : hospitals) {
            hospitalString += "'" + h + "',";
        }
        hospitalString = hospitalString.substring(0, hospitalString.length() - 1);
        hospitalString += ")";
        return "SELECT * FROM hospital WHERE name IN " + hospitalString + ";";
    }

    /**
     * This method initializes a new Thread which generates the AddressLists.
     */
    private void createWorkProcess() {

        Runnable task = () -> {generateAddresses(); };

        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();

    }

    private void generateAddresses (){

        try {
            generateAddresses_WithoutExceptionManagement();
        } catch (InterruptedException e) {
            logger.error("failed", e);
        }

    }

    private void generateAddresses_WithoutExceptionManagement () throws InterruptedException {

        for (int i = 0; i < maxAmountPatients; i++) {
            for (String hospital : addressListMapHospitals.keySet()) {

                PatientAddressType addressType = generateRandomAddress(coordinatesMapHospitals.get(hospital).getLatitude(), coordinatesMapHospitals.get(hospital).getLongitude(), coordinatesMapHospitals.get(hospital).getRadius());
                addressListMapHospitals.get(hospital).add(addressType);

                TimeUnit.SECONDS.sleep(1);

            }
        }

    }

    /**
     * this method calls the Nominati API to find a random address in a given radius around a given longitude and latitude.
     *
     * @param latitude
     * @param longitude
     * @param radius
     * @return
     */
    private PatientAddressType generateRandomAddress(double latitude, double longitude, double radius) {
        try{
            return generateRandomAddress_withoutManagementException(latitude, longitude, radius);
        } catch (NominatiReverseRequestException e) {
            e.printStackTrace(); //TODO
            return new PatientAddressType();
        }
    }

    private PatientAddressType generateRandomAddress_withoutManagementException(double latitude, double longitude, double radius) throws NominatiReverseRequestException {
        double randomLatitude = ThreadLocalRandom.current().nextDouble(latitude - radius, latitude + radius);
        double randomLongitude = ThreadLocalRandom.current().nextDouble(longitude - radius, longitude + radius);
        NominatiReverseRequest nominatiReverseRequest = new NominatiReverseRequest(randomLatitude, randomLongitude);
        return nominatiReverseRequest.getAddressType();
    }

    /**
     * This method takes a random address from a list for a given hospital. </br>
     *
     * @param hospital in which radius the address should be
     * @return a random address. Fallback is to return only the city.
     * @throws AddressGeneratorException then no AddressList found for the given hospital
     */
    public PatientAddressType takeRandomAddressFromList(String hospital) throws AddressGeneratorException {

        ArrayList<PatientAddressType> addressList = addressListMapHospitals.get(hospital);

        String birthplaceForHospital = findBirthplaceForHospital(hospital);

        if (addressList == null) {
            throw new AddressGeneratorException("For the given hospital was no AddressList found. Please check if your input is spelled correctly");
        } else if (addressList.size() > 0) {
            int randomNumber = ThreadLocalRandom.current().nextInt(0, addressList.size());
            PatientAddressType patientAddressFromList = addressList.get(randomNumber);
            // this is really necessary because otherwise all patients will have the same address, because their referencing to the same PatientAddressType object.
            PatientAddressType randomAddress = new PatientAddressType();
            if (patientAddressFromList.getCity() == null)
                randomAddress.setCity(birthplaceForHospital);
            else
                randomAddress.setCity(patientAddressFromList.getCity());
            randomAddress.setStreet(patientAddressFromList.getStreet());
            randomAddress.setCountry(patientAddressFromList.getCountry());
            randomAddress.setZipcode(patientAddressFromList.getZipcode());
            if (randomAddress.getStreet() != null)
                randomAddress.setStreetNo(String.valueOf(ThreadLocalRandom.current().nextInt(0, 50)));
            return randomAddress;
        } else {
            PatientAddressType defaultRandomAddress = new PatientAddressType();
            defaultRandomAddress.setCity(birthplaceForHospital);
            return defaultRandomAddress;
        }
    }

    public String findBirthplaceForHospital(String hospital) throws AddressGeneratorException {
        if (coordinatesMapHospitals.get(hospital) == null)
            throw new AddressGeneratorException("The given hospital was not found");
        String resultString = "";
        try {
            ResultSet resultSet = dbConnector.sendQuery("SELECT city FROM hospital WHERE name='" + hospital + "';");
            if (resultSet.next())
                resultString = resultSet.getString("city");
            resultSet.close();
        } catch (SQLException sqle) {
            System.out.println("Couldnt send query for birthplace");
        }
        return resultString;
    }

    /**
     * This method gets a random hospital from coordinates HashMap.
     *
     * @return a hospital name, Fallback is 'DKTK-Testdatengenerator'
     */
    public String getRandomHospital() {
        Set<String> keyObjects = coordinatesMapHospitals.keySet();
        String[] keys = keyObjects.toArray(new String[keyObjects.size()]);
        if (keys == null || keys.length - 1 > 0)
            return keys[ThreadLocalRandom.current().nextInt(0, keys.length - 1)];
        else
            return "DKTK-Testdatengenerator";
    }

}
