package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.*;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.customelements.PatientAdditionalInfo;
import dktk.testdatengenerator.util.RandomOperations;

import java.util.List;

public class MolecularMarkerGenerator {

    private IdGenerator idGenerator;
    private RandomOperations randomOperations;
    private DateGenerator dateGenerator;

    //TODO : Move to DB
    private final String MOLECULAR_MARKER_TYPE_REF = "CCP_MOLECULAR_MARKER";
    private final String MOLECULAR_MARKER_DATE_VALUE_TYPE_REF = "CCP_MOLECULAR_MARKER_DATE";
    //private final String MOLECULAR_MARKER_DATE_VALUE_TYPE_REF_URL = "/rest/export/catalogdata/LaborValueDate/13115";
    private final String MOLECULAR_MARKER_NAME_VALUE_TYPE_REF = "CCP_MOLECULAR_MARKER_NAME";
    //private final String MOLECULAR_MARKER_NAME_VALUE_TYPE_REF_URL = "/rest/export/catalogdata/LaborValueString/13116";
    private final String MOLECULAR_MARKER_STATUS_VALUE_TYPE_REF = "CCP_MOLECULAR_MARKER_STATUS";
    //private final String MOLECULAR_MARKER_STATUS_VALUE_TYPE_REF_URL = "/rest/export/catalogdata/LaborValueString/13117";
    private final String MOLECULAR_MARKER_ADDITIONAL_VALUE_TYPE_REF = "CCP_MOLECULAR_MARKER_ADDITIONAL";
    //private final String MOLECULAR_MARKER_ADDITIONAL_VALUE_TYPE_REF_URL = "/rest/export/catalogdata/LaborValueString/13118";

    private final String MOLECULAR_MARKER_MIN = "molMarkerMin";
    private final String MOLECULAR_MARKER_MAX = "molMarkerMax";

    public MolecularMarkerGenerator(TDGProfile tdgProfile) {
        this.idGenerator = new IdGenerator(tdgProfile);
        this.randomOperations = new RandomOperations(tdgProfile);
        this.dateGenerator = new DateGenerator();
    }

    public GTDSTumourEIType generateMolecularMarkersAndAddToTumour(GTDSTumourEIType tumour, PatientAdditionalInfo patientAdditionalInfo){

        if (tumour != null){

            int numberOfMolecularMarkersToBeGenerated = generateNumberOfMolecularMarkersToBeGenerated();
            for (int i=0; i< numberOfMolecularMarkersToBeGenerated; i++){

                FlexibleDataSetInstanceType molecularMarker = generateMolecularMarker();

                List<String> flexibleDataSetRefList = tumour.getFlexibleDataSetRef();
                String instanceRef = molecularMarker.getFlexibleDataSetInstanceRef();
                flexibleDataSetRefList.add(instanceRef);

                patientAdditionalInfo.addMolecularMarker(molecularMarker);

            }

        }

        return tumour;

    }

    private int generateNumberOfMolecularMarkersToBeGenerated(){
        return randomOperations.generateNumberOfItemsToBeGenerated(MOLECULAR_MARKER_MIN, MOLECULAR_MARKER_MAX);
    }

    private FlexibleDataSetInstanceType generateMolecularMarker(){

        FlexibleDataSetInstanceType flexibleDataSetInstance = new FlexibleDataSetInstanceType();

        List<FlexibleDataType> molecularMarkerDataelementList = flexibleDataSetInstance.getStringValueOrLongStringValueOrDateValue();

        String instanceRef = generateFlexibleDataSetInstanceRef();
        String typeRef = generateFlexibleDataSetTypeRef();
        String instanceName = generateInstanceName();
        FlexibleDateDataType molecularMarkerDate = generateMolecularMarkerDate();
        FlexibleStringDataType molecularMarkerName = generateMolecularMarkerName();
        FlexibleStringDataType molecularMarkerStatus = generateMolecularMarkerStatus();
        FlexibleStringDataType molecularMarkerAdditional = generateMolecularMarkerAdditional();


        flexibleDataSetInstance.setFlexibleDataSetInstanceRef(instanceRef);
        flexibleDataSetInstance.setFlexibleDataSetTypeRef(typeRef);
        flexibleDataSetInstance.setInstanceName(instanceName);
        // CXX: MOLECULAR_MARKER_DATE
        molecularMarkerDataelementList.add(molecularMarkerDate);
        // CXX: MOLECULAR_MARKER_NAME
        molecularMarkerDataelementList.add(molecularMarkerName);
        // CXX: MOLECULAR_MARKER_STATUS
        molecularMarkerDataelementList.add(molecularMarkerStatus);
        // CXX: MOLECULAR_MARKER_ADDITIONAL
        molecularMarkerDataelementList.add(molecularMarkerAdditional);


        return flexibleDataSetInstance;

    }

    private String generateFlexibleDataSetInstanceRef(){
        return idGenerator.generateId();
    }

    private String generateFlexibleDataSetTypeRef(){
        return MOLECULAR_MARKER_TYPE_REF;
    }

    private String generateInstanceName(){
        //TODO
        return "Molecular Marker-" + randomOperations.generateBigInteger();
    }

    private FlexibleDateDataType generateMolecularMarkerDate(){

        String cxxDatelement = MOLECULAR_MARKER_DATE_VALUE_TYPE_REF;
        //String refUrl = MOLECULAR_MARKER_DATE_VALUE_TYPE_REF_URL;
        String refUrl = null;
        //TODO
        DateType value = dateGenerator.generate(DatePrecision.DAY);

        return generateFlexibleDate(cxxDatelement, refUrl, value);

    }

    private FlexibleStringDataType generateMolecularMarkerName(){

        String cxxDatelement = MOLECULAR_MARKER_NAME_VALUE_TYPE_REF;
        //String refUrl = MOLECULAR_MARKER_NAME_VALUE_TYPE_REF_URL;
        String refUrl = null;
        //TODO
        String value = "name-" + randomOperations.generateBigInteger();

        return generateFlexibleString(cxxDatelement, refUrl, value);

    }

    private FlexibleStringDataType generateMolecularMarkerStatus(){

        String cxxDatelement = MOLECULAR_MARKER_STATUS_VALUE_TYPE_REF;
        //String refUrl = MOLECULAR_MARKER_STATUS_VALUE_TYPE_REF_URL;
        String refUrl = null;
        //TODO
        String value = "status-" + randomOperations.generateBigInteger();

        return generateFlexibleString(cxxDatelement, refUrl, value);

    }

    private FlexibleStringDataType generateMolecularMarkerAdditional(){

        String cxxDatelement = MOLECULAR_MARKER_ADDITIONAL_VALUE_TYPE_REF;
        //String refUrl = MOLECULAR_MARKER_ADDITIONAL_VALUE_TYPE_REF_URL;
        String refUrl = null;
        //TODO
        String value = "additional-" + randomOperations.generateBigInteger();

        return generateFlexibleString(cxxDatelement, refUrl, value);

    }

    private FlexibleStringDataType generateFlexibleString(String cxxDataelement, String refUrl, String value){

        FlexibleStringDataType flexibleString = new FlexibleStringDataType();

        CatalogueDataRefType catalogueDataRef = generateFlexibleValueRefType(cxxDataelement, refUrl);

        flexibleString.setValue(value);
        flexibleString.setFlexibleValueTypeRef(catalogueDataRef);

        return flexibleString;

    }

    private FlexibleDateDataType generateFlexibleDate (String cxxDataelement, String refUrl, DateType value){

        FlexibleDateDataType flexibleDate = new FlexibleDateDataType();

        CatalogueDataRefType catalogueDataRef = generateFlexibleValueRefType(cxxDataelement, refUrl);

        flexibleDate.setValue(value);
        flexibleDate.setFlexibleValueTypeRef(catalogueDataRef);

        return flexibleDate;

    }

    private CatalogueDataRefType generateFlexibleValueRefType(String cxxDataelement, String refUrl){

        CatalogueDataRefType catalogueDataRefType = new CatalogueDataRefType();

        catalogueDataRefType.setValue(cxxDataelement);
        catalogueDataRefType.setRefUrl(refUrl);

        return catalogueDataRefType;

    }


}
