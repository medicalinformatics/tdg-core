package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.*;
import dktk.testdatengenerator.customelements.Patient;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.exceptions.DateGeneratorException;
import dktk.testdatengenerator.util.RandomOperations;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class SampleGenerator {

    private RandomOperations randomOperations;
    private DateGenerator dateGenerator;
    private IdGenerator idGenerator;

    private final String MIN_DB = "sampleMin";
    private final String MAX_DB = "sampleMax";


    //TODO: Move to DB
    private List<String> sampleTypes = Arrays.asList(new String[]{"BMA","CSF","DNA","NGW","NRT","PL1","PROTEIN"});
    //TODO: Add to sample types,"PTM","RNA","SER","TGW","TIS","URN","VBL"

    //TODO: Move to DB
    private List<String> sampleStock = Arrays.asList(new String[]{"NBF","SNP"});

    public SampleGenerator(TDGProfile tdgProfile) {
        randomOperations = new RandomOperations(tdgProfile);
        dateGenerator = new DateGenerator();
        idGenerator = new IdGenerator(tdgProfile);
    }

    public Patient generateSamplesAndAddToPatient(Patient patient) {

        if (patient != null){

            SampleDataType samples = generateSamples(patient.getPatientDataSet());
            patient.getPatientDataSet().setSampleData(samples);

        }

        return patient;

    }

    public SampleDataType generateSamples (PatientDataSetType patient){

        SampleDataType sampleData = new SampleDataType();

        List<SampleClassType> sampleList = sampleData.getMasterSampleOrAliquotGroupOrDerivedSample();

        int numberOfSamplesToBeGenerated = getNumberOfSamplesToBeGenerated();
        for (int i= 0; i< numberOfSamplesToBeGenerated;i++){

            MasterSampleType sample = generateSample(patient);
            sampleList.add(sample);

        }

        return sampleData;
    }

    private int getNumberOfSamplesToBeGenerated (){
        return randomOperations.generateNumberOfItemsToBeGenerated(MIN_DB, MAX_DB);
    }

    private MasterSampleType generateSample (PatientDataSetType patient){

        MasterSampleType sample = new MasterSampleType();

        SampleKindEnumType sampleKind = generateSampleKind();
        CatalogueDataRefType sampleType = generateSampleType();
        DateType sampleDate = generateDate(patient);
        CatalogueDataRefType stockType = generateStockType();
        SampleIDContainerType sampleId = generateSampleId(patient);
        VolumeType initialAmount = generateInitialAmount();

        // CXX: SAMPLE_SAMPLEKIND
        sample.setSampleKind(sampleKind);
        // CXX: SAMPLE_SAMPLETYPE
        sample.setSampleTypeCatalogueTypeRef(sampleType);
        // CXX: SAMPLE_SAMPLINGDATE
        sample.setSamplingDate(sampleDate);
        // CXX: SAMPLE_STOCKTYPE
        sample.setStockTypeCatalogueTypeRef(stockType);
        sample.setSampleIDContainer(sampleId);
        sample.setInitialAmount(initialAmount);


        return sample;

    }


    private VolumeType generateInitialAmount(){

        VolumeType initialAmount = new VolumeType();

        //TODO
        AmountUnitEnumType unit = randomOperations.getRandomFromEnum(AmountUnitEnumType.class);
        BigDecimal volume = randomOperations.generateRandomBigDecimal(0, 100);

        initialAmount.setVolume(volume);
        initialAmount.setUnit(unit);

        return initialAmount;

    }

    private SampleIDContainerType generateSampleId (PatientDataSetType patient){

        String id = idGenerator.generateId();

        SampleIDContainerType sampleIDContainerType = new SampleIDContainerType();
        FlexibleIDType sampleFlexibleIDType = new FlexibleIDType();

        sampleFlexibleIDType.setKey("SAMPLEID");
        sampleFlexibleIDType.setValue(id);
        sampleIDContainerType.getFlexibleID().add(sampleFlexibleIDType);

        return sampleIDContainerType;

    }


    private SampleKindEnumType generateSampleKind(){
        return randomOperations.getRandomFromEnum(SampleKindEnumType.class);
    }

    private CatalogueDataRefType generateSampleType(){

        CatalogueDataRefType sampleType = new CatalogueDataRefType();
        String value = randomOperations.getRandomFromArrayList(sampleTypes);
        sampleType.setValue(value);

        return sampleType;

    }

    private DateType generateDate (PatientDataSetType patient){
        try {

            return generateDate_WithoutManagementException(patient);

        } catch (DateGeneratorException e) {
            e.printStackTrace();
            return dateGenerator.generate();
        }
    }

    private DateType generateDate_WithoutManagementException (PatientDataSetType patient) throws DateGeneratorException {
        //TODO

        DateType result = null;

        PatientMasterdataType patientMasterdata = patient.getMasterdata();
        DateType dateOfBirth = null;
        DateType dateOfDeath = null;
        if (patientMasterdata != null){
            dateOfBirth = patientMasterdata.getDateOfBirth();
            dateOfDeath = patientMasterdata.getDateOfDeath();
        }

        if (dateOfBirth != null && dateOfDeath == null){
            result = dateGenerator.generateDateAfter(dateOfBirth);
        } else if (dateOfBirth != null && dateOfDeath != null){
            result = dateGenerator.generateDateBetween(dateOfBirth, dateOfDeath);
        } else{
            result = dateGenerator.generate();
        }

        return result;

    }

    private CatalogueDataRefType generateStockType (){

        CatalogueDataRefType stockType = new CatalogueDataRefType();

        String value = randomOperations.getRandomFromArrayList(sampleStock);
        stockType.setValue(value);

        return stockType;

    }

}
