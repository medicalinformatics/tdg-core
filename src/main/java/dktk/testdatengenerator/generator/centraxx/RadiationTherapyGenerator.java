package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.*;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.util.MdrValidationConverter;
import dktk.testdatengenerator.util.RandomOperations;

import java.util.Arrays;
import java.util.List;

public class RadiationTherapyGenerator {

    private RandomOperations randomOperations;
    private IdGenerator idGenerator;
    private MdrValidationConverter mdrValidationConverter;
    // TODO : move to DB
    // TODO : Ask Kairos how to allow S
    private List<IntentionGTDSEnumType> intentionList = Arrays.asList(new IntentionGTDSEnumType[]{IntentionGTDSEnumType.P, IntentionGTDSEnumType.K, IntentionGTDSEnumType.X});
    private final String RADIATION_MIN = "radiationMin";
    private final String RADIATION_MAX = "radiationMax";

    private List<RadiationTherapyKindGTDSEnumType> excludedRadiationTherapyKindGTDSEnum = Arrays.asList(RadiationTherapyKindGTDSEnumType.X, RadiationTherapyKindGTDSEnumType.PR);



    public RadiationTherapyGenerator(TDGProfile tdgProfile) {
        randomOperations = new RandomOperations(tdgProfile);
        idGenerator = new IdGenerator(tdgProfile);
        mdrValidationConverter = new MdrValidationConverter();
    }


    public GTDSProgressEIType generateRadiationTherapiesAndAddToProgress(GTDSProgressEIType progress, EpisodeType episode){

        if (progress != null){

            List<GTDSTherapy> therapyList = progress.getTherapy();
            int numberOfTherapiesToBeGenerated = getNumberOfTherapiesToBeGenerated();
            for (int i=0; i < numberOfTherapiesToBeGenerated; i++){

                GTDSRadiationTherapyEIType radiationTherapy = generateRadiationTherapy(progress, episode);
                therapyList.add(radiationTherapy);

            }

        }

        return progress;

    }

    private int getNumberOfTherapiesToBeGenerated(){
        return randomOperations.generateNumberOfItemsToBeGenerated(RADIATION_MIN, RADIATION_MAX);
    }

    private GTDSRadiationTherapyEIType generateRadiationTherapy (GTDSProgressEIType progress, EpisodeType episode) {

        GTDSRadiationTherapyEIType radiationTherapy = new GTDSRadiationTherapyEIType();

        RadiationTherapyKindGTDSEnumType therapyKind = generateTherapyKind();
        SelectionListGTDSEnumType adverseEffects = generateAdverseEffects();
        DateType therapyStart = generateTherapyStart(episode);
        DateType therapyEnd = generateTherapyEnd(episode);
        String radiationId = generateRadiationId();
        IntentionGTDSEnumType intention = generateIntention();


        // CXX: RAD_INTENTION
        radiationTherapy.setIntention(intention);
        // CXX: RAD_THERAPYKIND
        radiationTherapy.setTherapyKind(therapyKind);
        // CXX: RAD_THERAPYSTART
        radiationTherapy.setTherapyStart(therapyStart);
        // CXX: RAD_THERAPYEND
        radiationTherapy.setTherapyEnd(therapyEnd);
        radiationTherapy.setAdverseEffectsEnum(adverseEffects);
        radiationTherapy.setRadiationTherapyID(radiationId);



        return radiationTherapy;

    }

    private String generateRadiationId(){
        return idGenerator.generateId();
    }


    private RadiationTherapyKindGTDSEnumType generateTherapyKind(){
        return randomOperations.getRandomFromEnum(RadiationTherapyKindGTDSEnumType.class, excludedRadiationTherapyKindGTDSEnum);
    }

    private SelectionListGTDSEnumType generateAdverseEffects(){
        return randomOperations.getRandomFromEnum(SelectionListGTDSEnumType.class);
    }

    private DateType generateTherapyStart(EpisodeType episode){
        return episode.getValidFrom();
    }

    private DateType generateTherapyEnd(EpisodeType episode){
        return episode.getValidUntil();
    }

/*
    private GtdsDictionaryRefType generateIntentionRef(){

        GtdsDictionaryRefType intentionRef = new GtdsDictionaryRefType();


        intentionRef.setCode(generateIntention().value());
        intentionRef.setVersion(GtdsVersionEnumType.ORIGINAL);

        //TODO : define ref somewhere else
        //intentionRef.setRefUrl("/rest/export/catalogdata/ProgressTherapyIntentionDictionary/374");
        intentionRef.setRefUrl("/rest/export/catalogdata/TherapyIntentionDictionary/160");

        return intentionRef;

    }
*/
    private IntentionGTDSEnumType generateIntention(){
        return randomOperations.getRandomFromArrayList(intentionList);
    }



}
