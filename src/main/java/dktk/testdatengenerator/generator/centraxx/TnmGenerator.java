package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.*;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.exceptions.DateGeneratorException;
import dktk.testdatengenerator.util.MdrUtils;
import dktk.testdatengenerator.util.RandomOperations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TnmGenerator {


    private DateGenerator dateGenerator;
    private MdrUtils mdrUtils;
    private RandomOperations randomOperations;
    private IdGenerator idGenerator;


    //TODO : use latest and not explicit version for mdrId
    private final String T_MDR_ID = "urn:dktk:dataelement:100:1";
    private final String N_MDR_ID = "urn:dktk:dataelement:101:1";
    private final String M_MDR_ID = "urn:dktk:dataelement:99:1";
    private final String TNM_RECIDIV_CLASSIFICATION_MDR_ID = "urn:dktk:dataelement:81:1";
    private final String TNM_Y_SYMBOL_MDR_ID = "urn:dktk:dataelement:82:1";
    private final String TNM_STADIUM_MDR_ID = "urn:dktk:dataelement:89:1";
    private final String TNM_PRAEFIX_T = "urn:dktk:dataelement:78:1";
    private final String TNM_PRAEFIX_N = "urn:dktk:dataelement:79:1";
    private final String TNM_PRAEFIX_M = "urn:dktk:dataelement:80:1";
    private final String TNM_MIN = "tnmMin";
    private final String TNM_MAX = "tnmMax";

    private List<TNMPTypeGTDSEnumType> excludedTNMPTypeGTDSEnum = Arrays.asList(TNMPTypeGTDSEnumType.a);

    public TnmGenerator (TDGProfile tdgProfile){

        mdrUtils = new MdrUtils(tdgProfile);
        dateGenerator = new DateGenerator();
        randomOperations = new RandomOperations(tdgProfile);
        idGenerator = new IdGenerator(tdgProfile);
    }


    public GTDSTumourEIType generateTnmAndAddToTumour(GTDSTumourEIType tumour){

        if (tumour != null){

            List<GTDSTNMEIType> newTnmList = generateTnmList(tumour);
            List<GTDSTNMEIType> tnmList = tumour.getTnm();
            tnmList.addAll(newTnmList);

        }

        return tumour;

    }


    private List<GTDSTNMEIType> generateTnmList(GTDSTumourEIType tumour){

        List<GTDSTNMEIType> tnmList = new ArrayList<>();
        int numberOfTnmToBeGenerated = getNumberOfTnmToBeGenerated();

        for (int i=0; i < numberOfTnmToBeGenerated ; i++){

            GTDSTNMEIType tnm = generateTnm(tumour);
            tnmList.add(tnm);

        }

        return tnmList;

    }

    private int getNumberOfTnmToBeGenerated (){
        return randomOperations.generateNumberOfItemsToBeGenerated(TNM_MIN, TNM_MAX);
    }

    private GTDSTNMEIType generateTnm (GTDSTumourEIType tumour){

        GTDSTNMEIType tnm = new GTDSTNMEIType();

        DateType tnmDate = generateTnmDate(tumour);
        String t = generateT();
        String n = generateN();
        String m = generateM();
        //GtdsDictionaryRefType prefixT = generatePrefixT();
        //GtdsDictionaryRefType prefixN = generatePrefixN();
        //GtdsDictionaryRefType prefixM = generatePrefixM();
        TNMPTypeGTDSEnumType prefixT = generatePrefixT();
        TNMPTypeGTDSEnumType prefixN = generatePrefixN();
        TNMPTypeGTDSEnumType prefixM = generatePrefixM();
        String stadium = generateStadium();
        String multiple = generateTnmMultiple();
        String tnmVersion = generateTnmVersion();
        String recidivClassification = generateRecidivClassification();
        String ysymbol = generateYsymbol();
        String tnmId = generateTnmId();


        // CXX: TNM_CLASSDATE
        tnm.setDate(tnmDate);
        // CXX: TNM_T
        tnm.setT(t);
        // CXX: TNM_N
        tnm.setN(n);
        // CXX: TNM_M
        tnm.setM(m);
        //tnm.setPraefixTRef(prefixT);
        //tnm.setPraefixNRef(prefixN);
        //tnm.setPraefixMRef(prefixM);
        // CXX: TNM_PRAEFIX_T
        tnm.setPraefixT(prefixT);
        // CXX: TNM_PRAEFIX_N
        tnm.setPraefixN(prefixN);
        // CXX: TNM_PRAEFIX_M
        tnm.setPraefixM(prefixM);
        // CXX: TNM_STADIUM
        tnm.setStadium(stadium);
        // CXX: TNM_MULTIPLE
        tnm.setMultiple(multiple);
        // CXX: TNM_VERSION
        tnm.setVersion(tnmVersion);
        // CXX: TNM_RECIDIV_CLASSIFICATION
        tnm.setRecidivClassification(recidivClassification);
        // CXX: TNM_Y_SYMBOL
        tnm.setYSymbol(ysymbol);
        tnm.setTNMID(tnmId);

        return tnm;

    }

    private String generateTnmId(){
        return idGenerator.generateId();
    }

    private DateType generateTnmDate (GTDSTumourEIType tumour){

        try {

            return generateTnmDate_WithoutManagementException(tumour);

        } catch (DateGeneratorException e) {
            e.printStackTrace();
            return null;
        }
    }

    private DateType generateTnmDate_WithoutManagementException (GTDSTumourEIType tumour) throws DateGeneratorException {

        DateType admissionDate = tumour.getAdmissionDate();
        DateType tnmDate = null;

        if (admissionDate != null) {
            tnmDate = dateGenerator.generateDateAfter(admissionDate);
        }else{
            //TODO: ???
            tnmDate = dateGenerator.generate();
        }

        return tnmDate;

    }

    private String generateT (){
        return mdrUtils.generateMdrEnumeratedValue(T_MDR_ID);
    }

    private String generateN (){
        return mdrUtils.generateMdrEnumeratedValue(N_MDR_ID);
    }

    private String generateM (){
        return mdrUtils.generateMdrEnumeratedValue(M_MDR_ID);
    }

    private String generateRecidivClassification (){
        return mdrUtils.generateMdrEnumeratedValue(TNM_RECIDIV_CLASSIFICATION_MDR_ID);
    }

    private String generateYsymbol(){
        return mdrUtils.generateMdrEnumeratedValue(TNM_Y_SYMBOL_MDR_ID);
    }

    private String generateStadium(){
        return mdrUtils.generateMdrEnumeratedValue(TNM_STADIUM_MDR_ID);
    }

/*
    private GtdsDictionaryRefType generatePrefixT(){
        return mdrUtils.generateMdrEnumeratedValueInCentraxxFormat(TNM_PRAEFIX_T);
    }

    private GtdsDictionaryRefType generatePrefixN(){
        return mdrUtils.generateMdrEnumeratedValueInCentraxxFormat(TNM_PRAEFIX_N);
    }

    private GtdsDictionaryRefType generatePrefixM(){
        return mdrUtils.generateMdrEnumeratedValueInCentraxxFormat(TNM_PRAEFIX_M);
    }
*/

    private TNMPTypeGTDSEnumType generatePrefixT(){
        return generateTNMPTypeGTDSEnum();
    }

    private TNMPTypeGTDSEnumType generatePrefixN(){
        return generateTNMPTypeGTDSEnum();
    }

    private TNMPTypeGTDSEnumType generatePrefixM(){
        return generateTNMPTypeGTDSEnum();
    }

    private TNMPTypeGTDSEnumType generateTNMPTypeGTDSEnum(){
        return randomOperations.getRandomFromEnum(TNMPTypeGTDSEnumType.class, excludedTNMPTypeGTDSEnum);
    }


    private String generateTnmVersion(){
        //TODO
        return "8";
    }

    private String generateTnmMultiple(){
        //TODO
        return (randomOperations.throwACoin()) ? "m" : randomOperations.generateInteger(1, 10)+"";
    }


}
