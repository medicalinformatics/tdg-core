package dktk.testdatengenerator.db;

import de.kairos_med.GenderTypeEnum;
import org.apache.commons.math3.util.Precision;

import java.io.Closeable;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by brennert on 22.06.2017.
 */
public class PatientQueries {
    private DBConnector dbConnector;

    public PatientQueries(DBConnector dbConnector) {
        this.dbConnector = dbConnector;
    }

    /**
     * this Method returns a random firstname from database for a given gender.
     *
     * @param gender currently only male and female are represented in database
     * @return
     */
    public String queryRandomFirstName(GenderTypeEnum gender) {
        double randomProbability = ThreadLocalRandom.current().nextDouble(3.318, 100);
        randomProbability = Precision.round(randomProbability, 3);
        return getNameFromDatabase(gender, randomProbability);
    }

    /**
     * This Method returns a random lastname from database.
     *
     * @return
     */
    public String queryRandomLastName() {
        double randomProbability = ThreadLocalRandom.current().nextDouble(1.006, 100);
        randomProbability = Precision.round(randomProbability, 3);
        return getNameFromDatabase(null, randomProbability);
    }

    /**
     * This Method returns a married lastname.
     * Married means that the lastname is build out of two lastnames and a seperating '-'
     *
     * @return
     */
    public String queryRandomLastNameMarried() {
        String marriedLastName = "";
        String lastname1 = queryRandomLastName();
        String lastname2 = queryRandomLastName();
        marriedLastName = lastname1 + "-" + lastname2;
        return marriedLastName;
    }

    /**
     * This methods returns a random name from the database for the given gender and randomProbability.
     *
     * @param gender            MALE, FEMALE or {@code null}(for lastnames)
     * @param randomProbability
     * @return
     */
    public String getNameFromDatabase(GenderTypeEnum gender, double randomProbability) {
        String firstname = "";
        if(gender != GenderTypeEnum.MALE && gender != GenderTypeEnum.FEMALE && gender != null)
            if(ThreadLocalRandom.current().nextInt(0,1) == 1)
                gender = GenderTypeEnum.FEMALE;
            else
                gender = GenderTypeEnum.MALE;
        try {
            ResultSet resultSet;
            if (gender != null) {
                resultSet = dbConnector.sendQuery("SELECT firstname, nametoplimit FROM namelist WHERE nametoplimit<=" + randomProbability + " AND firstname_gender='" + gender.value() + "' ORDER BY nametoplimit DESC LIMIT 10;");
            } else {
                // the lastname will also be returned as firstname
                resultSet = dbConnector.sendQuery("SELECT lastname AS firstname, nametoplimit FROM namelist WHERE nametoplimit<=" + randomProbability + " AND firstname_gender IS NULL ORDER BY nametoplimit DESC LIMIT 10;");
            }
            ArrayList<String> candidates = new ArrayList<>();
            if (resultSet.next()) {
                candidates.add(resultSet.getString("firstname"));
                while (resultSet.next()) {
                    candidates.add(resultSet.getString("firstname"));
                }
                if (candidates.size() - 1 > 0) {
                    firstname = candidates.get(ThreadLocalRandom.current().nextInt(0, (candidates.size() - 1)));
                } else {
                    firstname = candidates.get(0);
                }
            } else {
                ResultSet resultSet2;
                if (gender != null) {
                    resultSet2 = dbConnector.sendQuery("SELECT firstname, nametoplimit FROM namelist WHERE nametoplimit >= " + randomProbability + " AND firstname_gender='" + gender.value() + "' ORDER BY nametoplimit ASC LIMIT 1;");
                } else {
                    resultSet2 = dbConnector.sendQuery("SELECT firstname, nametoplimit FROM namelist WHERE nametoplimit >= " + randomProbability + " AND firstname_gender IS NULL ORDER BY nametoplimit ASC LIMIT 1;");
                }
                if (resultSet2.next())
                    firstname = resultSet2.getString("firstname");
            }
            resultSet.close();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
        if(firstname == null)
            System.out.println("Query Random name is returning null for gender:" + gender + " with probability " + randomProbability);
        return firstname;
    }

    public String queryRandomEthnicity(String citizenship) {
        String randomEthnicity = "";
        double randomDouble = Precision.round(ThreadLocalRandom.current().nextDouble(0, 100), 3);
        try{
            ResultSet resultSet = dbConnector.sendQuery("SELECT eth.name FROM citizenship_ethnicity ce \n" +
                                                                    "INNER JOIN ethnicity eth ON eth.e_id=ce.ethnicity_id \n" +
                                                                    "INNER JOIN citizenship cit ON cit.c_id=ce.citizenship_id\n" +
                                                                    "WHERE ce.ethnicityTopLimit>="+randomDouble+" AND cit.code='"+ citizenship +"' LIMIT 1");
            if(resultSet.next())
                randomEthnicity = resultSet.getString("name");
            resultSet.close();
        } catch(SQLException sqle){
            System.out.println("Couldn't send query");
            sqle.printStackTrace();
        }
        return randomEthnicity;
    }



}
