package dktk.testdatengenerator.db;

import java.io.Closeable;
import java.io.IOException;

public class DbConnection implements Closeable {

    private DBConnector dbConnector;

    public DbConnection(DBConnector dbConnector) {
        this.dbConnector = dbConnector;
        dbConnector.openConnection();
    }

    public DBConnector getDbConnector() {
        return dbConnector;
    }

    @Override
    public void close() throws IOException {
        dbConnector.closeConnection();
    }

}
