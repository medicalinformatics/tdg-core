package dktk.testdatengenerator.db;

import de.kairos_med.GenderTypeEnum;
import dktk.testdatengenerator.db.model.ICDCode;
import dktk.testdatengenerator.enums.ICDCodeGroups;

import java.io.Closeable;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * This class is responsible for every Request in connection to ICDCodes.
 * <p>
 * Created by brennert on 06.07.2017.
 */
public class ICDCodeQueries {

    private DBConnector dbConnector;

    public ICDCodeQueries(DBConnector dbConnector) {

        this.dbConnector = dbConnector;

    }

    /**
     * This method gets every Result for a given ICDCodeGroup.
     *
     * @param icdGroup
     * @return
     */
    public ArrayList<ICDCode> getICDCodeGroupValues(ICDCodeGroups icdGroup) {
        String whereStatements = createWhereStatements(icdGroup, "icd_code LIKE", "OR");

        ArrayList<ICDCode> icdCodes = new ArrayList<>();
        try {
            // ResultSet resultSet = dbConnector.sendQuery("SELECT * FROM icdcodes WHERE" + whereStatements + ";");
            ResultSet resultSet = dbConnector.sendQuery("SELECT * FROM icdcodes WHERE" + whereStatements + " AND icd_code not like '%+%'" + ";");
            while (resultSet.next()) {
                icdCodes.add(new ICDCode(resultSet.getString("icd_code"), resultSet.getString("icd_description")));
            }
            resultSet.close();
        } catch (SQLException sqle) {
            System.out.println("Couldn't get results for the string ");
            sqle.printStackTrace();
        }
        return icdCodes;
    }

    /**
     * This method gets The Probability for a given ICDCodeGroup and Gender
     *
     * @param gender
     * @param icdGroup
     * @return
     */
    public double getICDCodeProbability(GenderTypeEnum gender, ICDCodeGroups icdGroup) {
        double probability = 0;
        try {
            String query = "SELECT * FROM properties WHERE pname='p" + gender.name() + icdGroup.name() + "';";
            ResultSet resultSet = dbConnector.sendQuery(query);
            if (resultSet.next())
                probability = resultSet.getDouble("pvalue");
            resultSet.close();
        } catch (SQLException sqle) {
            System.out.println("Couldn't get probability for icd code group: " + icdGroup.name());
            sqle.printStackTrace();
        }
        return probability;
    }

    /**
     * This method returns the ProbabilitySum of all ICD Codes known in {@link ICDCodeGroups}
     *
     * @return
     */
    public double getProbabilitySumICDCodes(GenderTypeEnum gender) {
        double sum = 0.0;
        for (ICDCodeGroups icdGroup : ICDCodeGroups.values()) {
            sum += getICDCodeProbability(gender, icdGroup);
        }
        return sum;
    }

    /**
     * This method gets all missing ICD Codes
     *
     * @return
     */
    public ArrayList<ICDCode> getMissingICDCodes() {
        String whereStatements = createWhereStatements("icd_code NOT LIKE", "AND");
        ArrayList<ICDCode> icdCodes = new ArrayList<>();
        try {
            // Added here filter for all ICD Codes with + in Code, because CentraXX doesn't know them
            ResultSet resultSet = dbConnector.sendQuery("SELECT * FROM icdcodes WHERE" + whereStatements + " AND icd_code not like '%+%'" + ";");
            while (resultSet.next()) {
                icdCodes.add(new ICDCode(resultSet.getString("icd_code"), resultSet.getString("icd_description")));
            }
            resultSet.close();
        } catch (SQLException sqle) {
            System.out.println("Couldn't send query for getting missing ICDCodes");
            sqle.printStackTrace();
        }
        return icdCodes;
    }

    private String createWhereStatements(ICDCodeGroups icdGroup, String matcher, String modifier) {
        String whereStatements = "";
        for (String icdCode : icdGroup.getCodes()) {
            whereStatements += " " + matcher + " '" + icdCode + "%' " + modifier;
        }
        whereStatements = (modifier == "AND") ? whereStatements.substring(0, whereStatements.length() - 3) : whereStatements.substring(0, whereStatements.length() - 2);
        return whereStatements;
    }

    private String createWhereStatements(String matcher, String modifier) {
        String whereStatements = "";
        for (ICDCodeGroups icdGroup : ICDCodeGroups.values()) {
            whereStatements += createWhereStatements(icdGroup, matcher, modifier) + modifier;
        }
        whereStatements = (modifier == "AND") ? whereStatements.substring(0, whereStatements.length() - 3) : whereStatements.substring(0, whereStatements.length() - 2);
        return whereStatements;
    }

}
