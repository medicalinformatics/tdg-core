package dktk.testdatengenerator.db;

import java.io.Closeable;
import java.sql.*;

import java.io.IOException;
import java.io.FileInputStream;

import java.util.Properties;

import properties.PropertiesEnum;

/**
 * Created by brennert on 22.06.2017.
 */
public class DBConnector implements Closeable {

    private Connection connection = null;
    private String url;
    private String user;
    private String password;


    public DBConnector() {
        Properties property = new Properties();

        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String propertyFile = rootPath + "config.properties";
        try {
            property.load(new FileInputStream(propertyFile));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        this.url = property.getProperty((PropertiesEnum.URLDB).toString());
        this.user = property.getProperty((PropertiesEnum.USERDB).toString());
        this.password = property.getProperty((PropertiesEnum.PASSWORDDB).toString());

        //openConnection();
    }

    /**
     * This sends a query to database an delivers the result as a result set
     *
     * @param queryString
     * @return
     * @throws SQLException then the query in the parameter wasn't correct defined
     */
    public ResultSet sendQuery(String queryString) throws SQLException {
        if(queryString.startsWith("INSERT") || queryString.startsWith("UPDATE") || queryString.startsWith("DELETE")) {
            PreparedStatement preparedStatement = connection.prepareStatement(queryString, Statement.RETURN_GENERATED_KEYS);
            int affectedRows = preparedStatement.executeUpdate();
            return preparedStatement.getGeneratedKeys();
        }
        else{
            PreparedStatement preparedStatement = connection.prepareStatement(queryString, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            return preparedStatement.executeQuery();
        }
    }

    public void openConnection() {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(
                    this.url, this.user, this.password
            );
        } catch (ClassNotFoundException cnfe) {
            System.out.println("Couldn't find Driver");
            cnfe.printStackTrace();
        } catch (SQLException sqle) {
            System.out.println("Couldn't connect to Database");
            sqle.printStackTrace();
        }
    }

    /**
     * Closes the connection to the database.
     */
    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException sqle) {
            System.out.println("The connection could not be closed");
            sqle.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }

    @Override
    public void close() throws IOException {
        closeConnection();
    }
}
