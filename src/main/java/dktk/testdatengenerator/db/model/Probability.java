package dktk.testdatengenerator.db.model;

import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.exceptions.ProbabilityException;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by brennert on 13.07.2017.
 */
public class Probability {

    private String prefix, suffix, section, group;
    private boolean modifiedFlag;
    private Enum<?> enumValue;
    private double probability;
    private DBConnector dbConnector;


    public Probability(String prefix, double probability, DBConnector dbConnector) throws ProbabilityException {
        this(prefix, null, "", probability, "", "", dbConnector);
    }


    public Probability(String prefix, double probability, String section, String group, DBConnector dbConnector) throws ProbabilityException {
        this(prefix, null, "", probability, section, group, dbConnector);
    }

    public <T extends Enum<T>> Probability(String prefix, Enum<T> enumValue, double probability, DBConnector dbConnector) throws ProbabilityException {
        this(prefix, enumValue, "", probability, "", "", dbConnector);
    }

    /**
     * This class represents a probability element.
     * The initial Value of this probability could either be a value set by parameter or a value found from database.
     *
     * @param prefix      if no prefix exists just enter ""
     * @param enumValue   if no enumvalue exists just enter null
     * @param suffix      if no suffix exists just enter ""
     * @param probability if no probability exists please enter -1, then the constructor will search for value in properties table
     * @apiNote Because of performance reasons you should always add a value between 0 and 1 to the constructor for parameter probability.
     */
    public <T extends Enum<T>> Probability(String prefix, Enum<T> enumValue, String suffix, double probability, String section, String group, DBConnector dbConnector) throws ProbabilityException {

        this.dbConnector = dbConnector;
        this.prefix = prefix;
        this.enumValue = enumValue;
        this.suffix = suffix;
        this.section = section;
        this.group = group;
        if (probability != -1) {
            this.probability = probability;
        } else {
            try {

                ResultSet resultSet = dbConnector.sendQuery("SELECT pvalue FROM properties WHERE pname='" + getName() + "';");
                if (resultSet.next())
                    this.probability = resultSet.getDouble("pvalue");
                else
                    throw new ProbabilityException("This property couldnt be found");

            } catch (SQLException sqle) {
                System.out.println("The property couldn't be searched in database");
                sqle.printStackTrace();
            }
        }
    }

    public double getProbability() {
        return probability;
    }

    public void setProbability(double value) throws ProbabilityException {
        if (prefix.startsWith("p")) {
            doProbabilityChecks(value);
        }
        probability = value;
    }

    private void doProbabilityChecks(double value) throws ProbabilityException {
        if (value < 0)
            throw new ProbabilityException("The value of the probability " + getName() + " should not be set lower than 0");
        if (value > 1)
            throw new ProbabilityException("The value of the probability " + getName() + " should not be set higher than 1");
    }

    public String getName() {
        if (enumValue == null)
            return prefix + suffix;
        else
            return prefix + enumValue.name() + suffix;
    }

    public boolean isModifiedFlag() {
        return modifiedFlag;
    }

    public void setModifiedFlag() {
        this.modifiedFlag = true;
    }

    public String getSection() {
        return section;
    }

    public String getGroup() {
        return group;
    }
}
