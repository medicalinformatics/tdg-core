package dktk.testdatengenerator.db.model;

/**
 * Created by brennert on 06.07.2017.
 */
public class ICDCode {

    private String code;
    private String description;

    public ICDCode(String code, String description){
        setCode(code);
        setDescription(description);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
