package dktk.testdatengenerator.db.model;

import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.exceptions.ProbabilityException;

import java.io.Closeable;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by brennert on 13.07.2017.
 */
public class TDGProfile implements Closeable{
    private HashMap<String, Probability> probabilityHashMap;
    private CRUDProfileImpl crudProfile;
    private DBConnector dbConnector;


    public TDGProfile(DBConnector dbConnector) {
        this(null, null, dbConnector);
    }

    /**
     * This creates a new TDG Profile object. The Object will manage the Probabilities of the different Properties.
     * The parameter profileName is there to select a profile from Database. Then the parameter is not given, the TDGProfile
     * will load the standard probabilities defined in properties table. Otherwise it will search for a profile with the given name.
     * If no profile is found it will generateTnmAndAddToTumour a new profile and set the profile description to the String of second parameter.
     *
     * @param profileName        the profile name (set {@code null} for standard properties)
     * @param profileDescription the description for a new profile (set {@code null} then you don't want to create a new one)
     */
    public TDGProfile(String profileName, String profileDescription, DBConnector dbConnector) {

        this.dbConnector = dbConnector;
        probabilityHashMap = new HashMap<>();
        if (profileName != null) {
            crudProfile = new CRUDProfileImpl(profileName, profileDescription, dbConnector);
        }
        try {
            ResultSet resultSet = dbConnector.sendQuery("SELECT * FROM properties;");
            // TODO: resultSet = filter(resultSet); No: (profileValue == -1)
            while (resultSet.next()) {
                double profileValue = -1;
                boolean isProfileVar = false;
                if (profileName != null)
                    profileValue = crudProfile.read(resultSet.getString("pname"));
                if (profileValue == -1) // TODO : explain logic
                    profileValue = resultSet.getDouble("pvalue");
                else
                    isProfileVar = true;
                try {
                    Probability profileProperty = new Probability(resultSet.getString("pname"), profileValue, resultSet.getString("psection"), resultSet.getString("pgroup"), dbConnector);
                    if(isProfileVar)
                        profileProperty.setModifiedFlag();
                    probabilityHashMap.put(resultSet.getString("pname"), profileProperty);
                } catch (ProbabilityException pe) {
                    System.out.println("Couldn't create probability " + resultSet.getString("pname") + " from profile " + profileName);
                    pe.printStackTrace();
                }
            }
            resultSet.close();
        } catch (SQLException sqle) {
            System.out.println("Couldn't get all data from properties table");
            sqle.printStackTrace();
        }
    }

    /**
     * This constructor offers a possibility to create a completly new Profile
     * It will try to find the named profile in database.
     * @param probabilities
     * @param profileName
     * @param profileDescription
     */
    public TDGProfile(ArrayList<Probability> probabilities, String profileName, String profileDescription, DBConnector dbConnector){
        this.dbConnector = dbConnector;
        crudProfile = new CRUDProfileImpl(profileName, profileDescription, dbConnector);
        probabilityHashMap = new HashMap<>();
        for (Probability probability: probabilities)
            probabilityHashMap.put(probability.getName(), probability);
    }

    /**
     * This is a new TDGProfile Constructor which will be used to offer the possibility to generateTnmAndAddToTumour Data without using
     * a predefined profile.
     * @param probabilities
     */
    public TDGProfile(ArrayList<Probability> probabilities, DBConnector dbConnector){
        this(probabilities, "", "", dbConnector);
    }

    public Probability getProbability(String probabilityName) {
        return probabilityHashMap.get(probabilityName);
    }

    public <T extends Enum<T>> void setProbability(String propertyName, double newValue) throws ProbabilityException {
        if (probabilityHashMap.get(propertyName).getProbability() != newValue) {
            probabilityHashMap.get(propertyName).setModifiedFlag();
            probabilityHashMap.get(propertyName).setProbability(newValue);
        }
    }

    /**
     * Call this method then you want to update the currently selected profile
     */
    public void safeProfile() {
        probabilityHashMap.forEach((k, v) -> {
            if (v.isModifiedFlag()) {
                if(crudProfile.read(v.getName()) == -1)
                    crudProfile.create(k,v.getProbability());
                else
                    crudProfile.update(k,v.getProbability());
            }
        });
    }

    /**
     * Call this method then you want to create a new Profile with the same name and description as parameters
     * out of the current probabilityHashMap
     *
     * @param profileName
     * @param profileDescription
     */
    public void safeProfile(String profileName, String profileDescription) {
        if (crudProfile == null)
            crudProfile = new CRUDProfileImpl(profileName, profileDescription, dbConnector);
        safeProfile();
    }

    /**
     * This method should query the properties database for the sum of their probabilities.
     * For example:
     * querySum(AgeGroupEnum.class, AgeGroupEnum.Between40And49, "pMALETumour", "") returns 0,018
     *
     * @param enumType     the enum which should be searched
     * @param maxEnumValue the maximum enum value which should be reached
     * @param prefix       prefix for the database values
     * @param suffix       suffix for the database values
     * @param <T>
     * @return
     */
    public <T extends Enum<T>> double querySum(Class<T> enumType, T maxEnumValue, String prefix, String suffix) {
        double sum = 0.0;
        for (T enumValue : enumType.getEnumConstants()) {
            sum = sum + this.getProbability(prefix + enumValue.name() + suffix).getProbability();
            if (enumValue == maxEnumValue)
                break;
        }
        return sum;
    }

    public <T extends Enum<T>> double querySum(Class<T> enumType, String prefix, String suffix) {
        T[] enumConstants = enumType.getEnumConstants();
        return querySum(enumType, enumConstants[enumConstants.length - 1], prefix, suffix);
    }

    public HashMap<String, Probability> getProbabilityHashMap() {
        return probabilityHashMap;
    }


    @Override
    public void close() throws IOException {
        dbConnector.closeConnection();
    }

}
