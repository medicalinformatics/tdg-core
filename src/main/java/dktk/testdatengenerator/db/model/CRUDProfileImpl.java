package dktk.testdatengenerator.db.model;

import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.interfaces.CRUDInterface;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This class implements the CRUD Interface for the database table profile_properties.
 * Created by brennert on 13.07.2017.
 */
public class CRUDProfileImpl implements CRUDInterface {
    DBConnector dbConnector;
    private int profileID;

    public CRUDProfileImpl(String profileName, DBConnector dbConnector) {
        this(profileName, "", dbConnector);
    }

    /**
     * The constructor searches for the given profileName and creates a new profile if it doesn't find the profile in database.
     * Then the profileName has value {@code ""} the CRUDProfileImpl will not create a connection to a profile.
     *
     * @param profileName
     * @param profilDescription
     */
    public CRUDProfileImpl(String profileName, String profilDescription, DBConnector dbConnector) {
        try {
            this.dbConnector = dbConnector;
            if (profileName != "") {
                ResultSet resultSet = dbConnector.sendQuery("SELECT p_id FROM profile WHERE profile_name='" + profileName + "';");
                if (resultSet.next())
                    profileID = resultSet.getInt("p_id");
                else{
                    ResultSet generatedKeys = dbConnector.sendQuery("INSERT INTO profile(profile_name, profile_description) VALUES ('" + profileName + "', '" + profilDescription + "');");
                    boolean nextRow = generatedKeys.next();
                    if (nextRow)
                        profileID = (int) generatedKeys.getLong(1);
                    generatedKeys.close();
                }
                resultSet.close();
            }

        } catch (SQLException sqle) {
            System.out.println("Error while testing if profile exists in database");
            sqle.printStackTrace();
        }
    }

    @Override
    public void create(String name, double value) {
        try {
            dbConnector.sendQuery("INSERT INTO profile_properties(fk_p_id, property_name, profile_value) VALUES (" + getProfileID() + ",'" + name + "'," + value + ");");
        } catch (SQLException sqle) {
            System.out.println("Couldnt create the new value " + value + " for property " + name);
            sqle.printStackTrace();
        }
    }

    @Override
    public double read(String name) {
        try {
            ResultSet resultSet = dbConnector.sendQuery("SELECT * FROM profile_properties WHERE property_name='" + name + "' AND fk_p_id=" + getProfileID() + ";");
            if (resultSet.next())
                return resultSet.getDouble("profile_value");
            resultSet.close();
        } catch (SQLException sqle) {
            System.out.println("Couldn't read any data for property " + name);
            sqle.printStackTrace();
        }
        return -1;
    }

    @Override
    public void update(String name, double value) {
        try {
            dbConnector.sendQuery("UPDATE profile_properties SET profile_value=" + value + " WHERE property_name='" + name + "' AND fk_p_id=" + getProfileID() + ";");
        } catch (SQLException sqle) {
            System.out.println("Couldn't update the value of property " + name + " with the value " + value);
            sqle.printStackTrace();
        }
    }

    @Override
    public void delete(String name) {
        try {
            dbConnector.sendQuery("DELETE FROM profile_properties WHERE property_name='" + name + "' AND fk_p_id=" + getProfileID() + ";");
        } catch (SQLException sqle) {
            System.out.println("Coudn't delete property " + name);
            sqle.printStackTrace();
        }
    }

    /**
     * This method returns the id of the represented profile.
     *
     * @return
     */
    public double getProfileID() {
        return this.profileID;
    }

    @Override
    public void close() throws IOException {
        dbConnector.closeConnection();
    }
}
