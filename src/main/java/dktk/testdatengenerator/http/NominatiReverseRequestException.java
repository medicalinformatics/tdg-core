package dktk.testdatengenerator.http;

public class NominatiReverseRequestException extends Exception {

    public NominatiReverseRequestException() {
    }

    public NominatiReverseRequestException(String message) {
        super(message);
    }

    public NominatiReverseRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public NominatiReverseRequestException(Throwable cause) {
        super(cause);
    }

    public NominatiReverseRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
