package dktk.testdatengenerator.http;

import de.kairos_med.PatientAddressType;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
//import javax.xml.ws.http.HTTPException;
import java.io.*;
import java.net.*;

/**
 * Created by brennert on 27.06.2017.
 */
public class NominatiReverseRequest {
    private HttpURLConnection httpUrlConnection;
    private String response;

    public NominatiReverseRequest(double lat, double lon) throws NominatiReverseRequestException {
        int responseCode = 0;
        try {
            URL url = new URL("https://nominatim.openstreetmap.org/reverse?format=xml&zoom=18&addressdetails=1&lat=" + lat + "&lon=" + lon);
            httpUrlConnection = (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("www-int2.inet.dkfz-heidelberg.de", 3128)));
            httpUrlConnection.setRequestProperty("content-type", "application/xml; charset= utf-8");
            httpUrlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Win98; en-US; rv:1.7.2) Gecko/20040803");
            responseCode = httpUrlConnection.getResponseCode();
        } catch (MalformedURLException mue) {
            System.out.println("The url path is wrong");
            mue.printStackTrace();
        } catch (IOException ioe) {
            System.out.println("The connection couldnt be initialized");
            ioe.printStackTrace();
        }
        if(500 <= responseCode)
            throw new NominatiReverseRequestException(""+responseCode); // TODO
    }

    public HttpURLConnection getConnection() {
        return httpUrlConnection;
    }

    public PatientAddressType getAddressType() {
        String xmlResult = getResponseAsString();
        PatientAddressType addressType = new PatientAddressType();
        try {
            XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(xmlResult));
            while (reader.hasNext()) {
                reader.next();
                if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                    if (reader.getLocalName() == "house_number")
                        addressType.setStreetNo(reader.getElementText());
                    if (reader.getLocalName() == "road")
                        addressType.setStreet(reader.getElementText());
                    if (reader.getLocalName() == "city")
                        addressType.setCity(reader.getElementText());
                    if (reader.getLocalName() == "country")
                        addressType.setCountry(reader.getElementText());
                    if (reader.getLocalName() == "postcode")
                        addressType.setZipcode(reader.getElementText());
                }
            }
            reader.close();
        } catch (XMLStreamException xmlse) {
            System.out.println("Couldnt stream the xml String");
            xmlse.printStackTrace();
        }
        return addressType;
    }

    private String getResponseAsString() {
        StringBuffer response = new StringBuffer();
        try {
            InputStream responseStream = httpUrlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(responseStream));
            String line;
            while ((line = br.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            responseStream.close();
            httpUrlConnection.disconnect();
        } catch (IOException e) {
            System.out.println("IOException is here in getResponse");
            e.printStackTrace();
        }
        return response.toString();
    }
}
