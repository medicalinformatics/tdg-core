
package de.kairos_med;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MedicationServiceTypeEnumType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MedicationServiceTypeEnumType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="VER"/&gt;
 *     &lt;enumeration value="GAB"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "MedicationServiceTypeEnumType")
@XmlEnum
public enum MedicationServiceTypeEnumType {


    /**
     * Verordnung
     * 
     */
    VER,

    /**
     * Gabe
     * 
     */
    GAB;

    public String value() {
        return name();
    }

    public static MedicationServiceTypeEnumType fromValue(String v) {
        return valueOf(v);
    }

}
