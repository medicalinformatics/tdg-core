
package de.kairos_med;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CrfDependencyType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CrfDependencyType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="CrfTemplateRef" type="{http://www.kairos-med.de}CrfTemplateRefType"/&gt;
 *           &lt;element name="CrfTemplateSectionRef" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="Activity" type="{http://www.kairos-med.de}CrfDependencyActivityEnumType"/&gt;
 *         &lt;element name="Operator" type="{http://www.kairos-med.de}CrfDependencyOperatorEnumType"/&gt;
 *         &lt;element name="ValueReference" type="{http://www.kairos-med.de}ValueReferenceType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CrfDependencyType", propOrder = {
    "crfTemplateRef",
    "crfTemplateSectionRef",
    "activity",
    "operator",
    "valueReference"
})
public class CrfDependencyType {

    @XmlElement(name = "CrfTemplateRef")
    protected CrfTemplateRefType crfTemplateRef;
    @XmlElement(name = "CrfTemplateSectionRef")
    protected String crfTemplateSectionRef;
    @XmlElement(name = "Activity", required = true)
    @XmlSchemaType(name = "string")
    protected CrfDependencyActivityEnumType activity;
    @XmlElement(name = "Operator", required = true)
    @XmlSchemaType(name = "string")
    protected CrfDependencyOperatorEnumType operator;
    @XmlElement(name = "ValueReference")
    protected ValueReferenceType valueReference;

    /**
     * Gets the value of the crfTemplateRef property.
     * 
     * @return
     *     possible object is
     *     {@link CrfTemplateRefType }
     *     
     */
    public CrfTemplateRefType getCrfTemplateRef() {
        return crfTemplateRef;
    }

    /**
     * Sets the value of the crfTemplateRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link CrfTemplateRefType }
     *     
     */
    public void setCrfTemplateRef(CrfTemplateRefType value) {
        this.crfTemplateRef = value;
    }

    /**
     * Gets the value of the crfTemplateSectionRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrfTemplateSectionRef() {
        return crfTemplateSectionRef;
    }

    /**
     * Sets the value of the crfTemplateSectionRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrfTemplateSectionRef(String value) {
        this.crfTemplateSectionRef = value;
    }

    /**
     * Gets the value of the activity property.
     * 
     * @return
     *     possible object is
     *     {@link CrfDependencyActivityEnumType }
     *     
     */
    public CrfDependencyActivityEnumType getActivity() {
        return activity;
    }

    /**
     * Sets the value of the activity property.
     * 
     * @param value
     *     allowed object is
     *     {@link CrfDependencyActivityEnumType }
     *     
     */
    public void setActivity(CrfDependencyActivityEnumType value) {
        this.activity = value;
    }

    /**
     * Gets the value of the operator property.
     * 
     * @return
     *     possible object is
     *     {@link CrfDependencyOperatorEnumType }
     *     
     */
    public CrfDependencyOperatorEnumType getOperator() {
        return operator;
    }

    /**
     * Sets the value of the operator property.
     * 
     * @param value
     *     allowed object is
     *     {@link CrfDependencyOperatorEnumType }
     *     
     */
    public void setOperator(CrfDependencyOperatorEnumType value) {
        this.operator = value;
    }

    /**
     * Gets the value of the valueReference property.
     * 
     * @return
     *     possible object is
     *     {@link ValueReferenceType }
     *     
     */
    public ValueReferenceType getValueReference() {
        return valueReference;
    }

    /**
     * Sets the value of the valueReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueReferenceType }
     *     
     */
    public void setValueReference(ValueReferenceType value) {
        this.valueReference = value;
    }

}
