
package de.kairos_med;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CrfDependencyActivityEnumType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CrfDependencyActivityEnumType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ENABLE"/&gt;
 *     &lt;enumeration value="DISABLE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CrfDependencyActivityEnumType")
@XmlEnum
public enum CrfDependencyActivityEnumType {

    ENABLE,
    DISABLE;

    public String value() {
        return name();
    }

    public static CrfDependencyActivityEnumType fromValue(String v) {
        return valueOf(v);
    }

}
