
package de.kairos_med;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Represents a collection of samples that can be saved by a user.
 *         The Export references for each sample the configured prefered sample ID.
 *         The Import handles any of the existing sample IDs. 
 *         If more than one sample is found by a not decisive ID, all samples are added to the set.
 *         If no sample is found by an ID, a XmlValidationException is thrown.
 * 
 *         Created: Always. There is no business key.
 *         Updated: Never.
 *         Skipped: Never.
 *       
 * 
 * <p>Java class for SampleSetType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SampleSetType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="FlexibleId" type="{http://www.kairos-med.de}FlexibleIDType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SampleSetType", propOrder = {
    "name",
    "flexibleId"
})
public class SampleSetType {

    @XmlElement(name = "Name", required = true)
    protected String name;
    @XmlElement(name = "FlexibleId")
    protected List<FlexibleIDType> flexibleId;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the flexibleId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flexibleId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlexibleId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FlexibleIDType }
     * 
     * 
     */
    public List<FlexibleIDType> getFlexibleId() {
        if (flexibleId == null) {
            flexibleId = new ArrayList<FlexibleIDType>();
        }
        return this.flexibleId;
    }

}
