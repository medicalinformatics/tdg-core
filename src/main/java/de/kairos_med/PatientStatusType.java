
package de.kairos_med;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PatientStatusType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PatientStatusType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Active" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="InactiveStatusReasonRef" type="{http://www.kairos-med.de}CatalogueDataRefType" minOccurs="0"/&gt;
 *         &lt;element name="InactiveUntil" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PatientStatusType", propOrder = {
    "active",
    "inactiveStatusReasonRef",
    "inactiveUntil"
})
public class PatientStatusType {

    @XmlElement(name = "Active")
    protected Boolean active;
    @XmlElement(name = "InactiveStatusReasonRef")
    protected CatalogueDataRefType inactiveStatusReasonRef;
    @XmlElement(name = "InactiveUntil")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar inactiveUntil;

    /**
     * Gets the value of the active property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setActive(Boolean value) {
        this.active = value;
    }

    /**
     * Gets the value of the inactiveStatusReasonRef property.
     * 
     * @return
     *     possible object is
     *     {@link CatalogueDataRefType }
     *     
     */
    public CatalogueDataRefType getInactiveStatusReasonRef() {
        return inactiveStatusReasonRef;
    }

    /**
     * Sets the value of the inactiveStatusReasonRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link CatalogueDataRefType }
     *     
     */
    public void setInactiveStatusReasonRef(CatalogueDataRefType value) {
        this.inactiveStatusReasonRef = value;
    }

    /**
     * Gets the value of the inactiveUntil property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInactiveUntil() {
        return inactiveUntil;
    }

    /**
     * Sets the value of the inactiveUntil property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInactiveUntil(XMLGregorianCalendar value) {
        this.inactiveUntil = value;
    }

}
