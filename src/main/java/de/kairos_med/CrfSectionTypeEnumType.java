
package de.kairos_med;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CrfSectionTypeEnumType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CrfSectionTypeEnumType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="PLAIN"/&gt;
 *     &lt;enumeration value="MULTIVALUE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CrfSectionTypeEnumType")
@XmlEnum
public enum CrfSectionTypeEnumType {

    PLAIN,
    MULTIVALUE;

    public String value() {
        return name();
    }

    public static CrfSectionTypeEnumType fromValue(String v) {
        return valueOf(v);
    }

}
