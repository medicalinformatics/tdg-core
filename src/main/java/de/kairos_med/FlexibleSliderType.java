
package de.kairos_med;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FlexibleSliderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FlexibleSliderType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.kairos-med.de}FlexibleValueType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MinRange" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="MaxRange" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlexibleSliderType", propOrder = {
    "minRange",
    "maxRange"
})
public class FlexibleSliderType
    extends FlexibleValueType
{

    @XmlElement(name = "MinRange")
    protected BigDecimal minRange;
    @XmlElement(name = "MaxRange")
    protected BigDecimal maxRange;

    /**
     * Gets the value of the minRange property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinRange() {
        return minRange;
    }

    /**
     * Sets the value of the minRange property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinRange(BigDecimal value) {
        this.minRange = value;
    }

    /**
     * Gets the value of the maxRange property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxRange() {
        return maxRange;
    }

    /**
     * Sets the value of the maxRange property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxRange(BigDecimal value) {
        this.maxRange = value;
    }

}
