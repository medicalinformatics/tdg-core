
package de.kairos_med;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FlexibleDataSetInstanceRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FlexibleDataSetInstanceRefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FlexibleDataSetTypeRef" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="InstanceName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Date" type="{http://www.kairos-med.de}DateType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlexibleDataSetInstanceRefType", propOrder = {
    "flexibleDataSetTypeRef",
    "instanceName",
    "date"
})
public class FlexibleDataSetInstanceRefType {

    @XmlElement(name = "FlexibleDataSetTypeRef", required = true)
    protected String flexibleDataSetTypeRef;
    @XmlElement(name = "InstanceName", required = true)
    protected String instanceName;
    @XmlElement(name = "Date", required = true)
    protected DateType date;

    /**
     * Gets the value of the flexibleDataSetTypeRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlexibleDataSetTypeRef() {
        return flexibleDataSetTypeRef;
    }

    /**
     * Sets the value of the flexibleDataSetTypeRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlexibleDataSetTypeRef(String value) {
        this.flexibleDataSetTypeRef = value;
    }

    /**
     * Gets the value of the instanceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstanceName() {
        return instanceName;
    }

    /**
     * Sets the value of the instanceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstanceName(String value) {
        this.instanceName = value;
    }

    /**
     * Gets the value of the date property.
     * 
     * @return
     *     possible object is
     *     {@link DateType }
     *     
     */
    public DateType getDate() {
        return date;
    }

    /**
     * Sets the value of the date property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateType }
     *     
     */
    public void setDate(DateType value) {
        this.date = value;
    }

}
