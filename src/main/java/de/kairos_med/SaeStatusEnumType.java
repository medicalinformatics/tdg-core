
package de.kairos_med;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SaeStatusEnumType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SaeStatusEnumType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="DOCUMENTED"/&gt;
 *     &lt;enumeration value="SENT"/&gt;
 *     &lt;enumeration value="COMPLETED"/&gt;
 *     &lt;enumeration value="DELETED"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SaeStatusEnumType")
@XmlEnum
public enum SaeStatusEnumType {

    DOCUMENTED,
    SENT,
    COMPLETED,
    DELETED;

    public String value() {
        return name();
    }

    public static SaeStatusEnumType fromValue(String v) {
        return valueOf(v);
    }

}
