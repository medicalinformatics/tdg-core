
package de.kairos_med;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValidationTypeEnumType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ValidationTypeEnumType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="REGEXP"/&gt;
 *     &lt;enumeration value="GROOVY"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ValidationTypeEnumType")
@XmlEnum
public enum ValidationTypeEnumType {

    REGEXP,
    GROOVY;

    public String value() {
        return name();
    }

    public static ValidationTypeEnumType fromValue(String v) {
        return valueOf(v);
    }

}
