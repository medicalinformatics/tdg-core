
package de.kairos_med;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CatalogEntryRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CatalogEntryRefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="UserDefinedCatalogEntry" type="{http://www.kairos-med.de}UserDefinedCatalogEntryRefType" minOccurs="0"/&gt;
 *         &lt;element name="ValueListEntry" type="{http://www.kairos-med.de}ValueListEntryRefType" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CatalogEntryRefType", propOrder = {
    "userDefinedCatalogEntry",
    "valueListEntry"
})
public class CatalogEntryRefType {

    @XmlElement(name = "UserDefinedCatalogEntry")
    protected UserDefinedCatalogEntryRefType userDefinedCatalogEntry;
    @XmlElement(name = "ValueListEntry")
    protected ValueListEntryRefType valueListEntry;

    /**
     * Gets the value of the userDefinedCatalogEntry property.
     * 
     * @return
     *     possible object is
     *     {@link UserDefinedCatalogEntryRefType }
     *     
     */
    public UserDefinedCatalogEntryRefType getUserDefinedCatalogEntry() {
        return userDefinedCatalogEntry;
    }

    /**
     * Sets the value of the userDefinedCatalogEntry property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDefinedCatalogEntryRefType }
     *     
     */
    public void setUserDefinedCatalogEntry(UserDefinedCatalogEntryRefType value) {
        this.userDefinedCatalogEntry = value;
    }

    /**
     * Gets the value of the valueListEntry property.
     * 
     * @return
     *     possible object is
     *     {@link ValueListEntryRefType }
     *     
     */
    public ValueListEntryRefType getValueListEntry() {
        return valueListEntry;
    }

    /**
     * Sets the value of the valueListEntry property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueListEntryRefType }
     *     
     */
    public void setValueListEntry(ValueListEntryRefType value) {
        this.valueListEntry = value;
    }

}
