
package de.kairos_med;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EdcDefinitionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EdcDefinitionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Type" type="{http://www.kairos-med.de}EdcTypeEnumType"/&gt;
 *         &lt;element name="laborValueRef" type="{http://www.kairos-med.de}CatalogueDataRefType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EdcDefinitionType", propOrder = {
    "type",
    "laborValueRef"
})
public class EdcDefinitionType {

    @XmlElement(name = "Type", required = true)
    @XmlSchemaType(name = "string")
    protected EdcTypeEnumType type;
    protected CatalogueDataRefType laborValueRef;

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link EdcTypeEnumType }
     *     
     */
    public EdcTypeEnumType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link EdcTypeEnumType }
     *     
     */
    public void setType(EdcTypeEnumType value) {
        this.type = value;
    }

    /**
     * Gets the value of the laborValueRef property.
     * 
     * @return
     *     possible object is
     *     {@link CatalogueDataRefType }
     *     
     */
    public CatalogueDataRefType getLaborValueRef() {
        return laborValueRef;
    }

    /**
     * Sets the value of the laborValueRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link CatalogueDataRefType }
     *     
     */
    public void setLaborValueRef(CatalogueDataRefType value) {
        this.laborValueRef = value;
    }

}
