
package de.kairos_med;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SampleStatusType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SampleStatusType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="NameMultilingualEntries" type="{http://www.kairos-med.de}MultilingualEntryType" maxOccurs="2" minOccurs="0"/&gt;
 *         &lt;element name="DescMultilingualEntries" type="{http://www.kairos-med.de}MultilingualEntryType" maxOccurs="2" minOccurs="0"/&gt;
 *         &lt;element name="AllowedActions" type="{http://www.kairos-med.de}SampleActionEnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SampleStatusType", propOrder = {
    "code",
    "nameMultilingualEntries",
    "descMultilingualEntries",
    "allowedActions"
})
public class SampleStatusType {

    @XmlElement(name = "Code", required = true)
    protected String code;
    @XmlElement(name = "NameMultilingualEntries")
    protected List<MultilingualEntryType> nameMultilingualEntries;
    @XmlElement(name = "DescMultilingualEntries")
    protected List<MultilingualEntryType> descMultilingualEntries;
    @XmlElement(name = "AllowedActions")
    @XmlSchemaType(name = "string")
    protected List<SampleActionEnumType> allowedActions;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the nameMultilingualEntries property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nameMultilingualEntries property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNameMultilingualEntries().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MultilingualEntryType }
     * 
     * 
     */
    public List<MultilingualEntryType> getNameMultilingualEntries() {
        if (nameMultilingualEntries == null) {
            nameMultilingualEntries = new ArrayList<MultilingualEntryType>();
        }
        return this.nameMultilingualEntries;
    }

    /**
     * Gets the value of the descMultilingualEntries property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the descMultilingualEntries property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDescMultilingualEntries().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MultilingualEntryType }
     * 
     * 
     */
    public List<MultilingualEntryType> getDescMultilingualEntries() {
        if (descMultilingualEntries == null) {
            descMultilingualEntries = new ArrayList<MultilingualEntryType>();
        }
        return this.descMultilingualEntries;
    }

    /**
     * Gets the value of the allowedActions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allowedActions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllowedActions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SampleActionEnumType }
     * 
     * 
     */
    public List<SampleActionEnumType> getAllowedActions() {
        if (allowedActions == null) {
            allowedActions = new ArrayList<SampleActionEnumType>();
        }
        return this.allowedActions;
    }

}
