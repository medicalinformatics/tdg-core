
package de.kairos_med;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValueReferenceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValueReferenceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReferenceType" type="{http://www.kairos-med.de}ValueReferenceTypeEnumType"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="DecimalTrigger" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *           &lt;element name="IntegerTrigger" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *           &lt;element name="BooleanTrigger" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *           &lt;element name="UsageEntryTrigger" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *           &lt;element name="IcdEntryTrigger" type="{http://www.kairos-med.de}CrfItemDefaultIcdEntryRefType"/&gt;
 *           &lt;element name="CatalogEntryTrigger" type="{http://www.kairos-med.de}CatalogEntryRefType"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValueReferenceType", propOrder = {
    "referenceType",
    "decimalTrigger",
    "integerTrigger",
    "booleanTrigger",
    "usageEntryTrigger",
    "icdEntryTrigger",
    "catalogEntryTrigger"
})
public class ValueReferenceType {

    @XmlElement(name = "ReferenceType", required = true)
    @XmlSchemaType(name = "string")
    protected ValueReferenceTypeEnumType referenceType;
    @XmlElement(name = "DecimalTrigger")
    protected BigDecimal decimalTrigger;
    @XmlElement(name = "IntegerTrigger")
    protected BigInteger integerTrigger;
    @XmlElement(name = "BooleanTrigger")
    protected Boolean booleanTrigger;
    @XmlElement(name = "UsageEntryTrigger")
    protected String usageEntryTrigger;
    @XmlElement(name = "IcdEntryTrigger")
    protected CrfItemDefaultIcdEntryRefType icdEntryTrigger;
    @XmlElement(name = "CatalogEntryTrigger")
    protected CatalogEntryRefType catalogEntryTrigger;

    /**
     * Gets the value of the referenceType property.
     * 
     * @return
     *     possible object is
     *     {@link ValueReferenceTypeEnumType }
     *     
     */
    public ValueReferenceTypeEnumType getReferenceType() {
        return referenceType;
    }

    /**
     * Sets the value of the referenceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueReferenceTypeEnumType }
     *     
     */
    public void setReferenceType(ValueReferenceTypeEnumType value) {
        this.referenceType = value;
    }

    /**
     * Gets the value of the decimalTrigger property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDecimalTrigger() {
        return decimalTrigger;
    }

    /**
     * Sets the value of the decimalTrigger property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDecimalTrigger(BigDecimal value) {
        this.decimalTrigger = value;
    }

    /**
     * Gets the value of the integerTrigger property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getIntegerTrigger() {
        return integerTrigger;
    }

    /**
     * Sets the value of the integerTrigger property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setIntegerTrigger(BigInteger value) {
        this.integerTrigger = value;
    }

    /**
     * Gets the value of the booleanTrigger property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBooleanTrigger() {
        return booleanTrigger;
    }

    /**
     * Sets the value of the booleanTrigger property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBooleanTrigger(Boolean value) {
        this.booleanTrigger = value;
    }

    /**
     * Gets the value of the usageEntryTrigger property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsageEntryTrigger() {
        return usageEntryTrigger;
    }

    /**
     * Sets the value of the usageEntryTrigger property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsageEntryTrigger(String value) {
        this.usageEntryTrigger = value;
    }

    /**
     * Gets the value of the icdEntryTrigger property.
     * 
     * @return
     *     possible object is
     *     {@link CrfItemDefaultIcdEntryRefType }
     *     
     */
    public CrfItemDefaultIcdEntryRefType getIcdEntryTrigger() {
        return icdEntryTrigger;
    }

    /**
     * Sets the value of the icdEntryTrigger property.
     * 
     * @param value
     *     allowed object is
     *     {@link CrfItemDefaultIcdEntryRefType }
     *     
     */
    public void setIcdEntryTrigger(CrfItemDefaultIcdEntryRefType value) {
        this.icdEntryTrigger = value;
    }

    /**
     * Gets the value of the catalogEntryTrigger property.
     * 
     * @return
     *     possible object is
     *     {@link CatalogEntryRefType }
     *     
     */
    public CatalogEntryRefType getCatalogEntryTrigger() {
        return catalogEntryTrigger;
    }

    /**
     * Sets the value of the catalogEntryTrigger property.
     * 
     * @param value
     *     allowed object is
     *     {@link CatalogEntryRefType }
     *     
     */
    public void setCatalogEntryTrigger(CatalogEntryRefType value) {
        this.catalogEntryTrigger = value;
    }

}
