
package de.kairos_med;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StudyScheduleType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StudyScheduleType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MinValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="MinUnit" type="{http://www.kairos-med.de}StudyScheduleUnitEnumType"/&gt;
 *         &lt;element name="MaxValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="MaxUnit" type="{http://www.kairos-med.de}StudyScheduleUnitEnumType"/&gt;
 *         &lt;element name="RefPoint" type="{http://www.kairos-med.de}StudyScheduleRefPointEnumType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StudyScheduleType", propOrder = {
    "minValue",
    "minUnit",
    "maxValue",
    "maxUnit",
    "refPoint"
})
public class StudyScheduleType {

    @XmlElement(name = "MinValue")
    protected Integer minValue;
    @XmlElement(name = "MinUnit", required = true)
    @XmlSchemaType(name = "string")
    protected StudyScheduleUnitEnumType minUnit;
    @XmlElement(name = "MaxValue")
    protected Integer maxValue;
    @XmlElement(name = "MaxUnit", required = true)
    @XmlSchemaType(name = "string")
    protected StudyScheduleUnitEnumType maxUnit;
    @XmlElement(name = "RefPoint", required = true)
    @XmlSchemaType(name = "string")
    protected StudyScheduleRefPointEnumType refPoint;

    /**
     * Gets the value of the minValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinValue() {
        return minValue;
    }

    /**
     * Sets the value of the minValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinValue(Integer value) {
        this.minValue = value;
    }

    /**
     * Gets the value of the minUnit property.
     * 
     * @return
     *     possible object is
     *     {@link StudyScheduleUnitEnumType }
     *     
     */
    public StudyScheduleUnitEnumType getMinUnit() {
        return minUnit;
    }

    /**
     * Sets the value of the minUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyScheduleUnitEnumType }
     *     
     */
    public void setMinUnit(StudyScheduleUnitEnumType value) {
        this.minUnit = value;
    }

    /**
     * Gets the value of the maxValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxValue() {
        return maxValue;
    }

    /**
     * Sets the value of the maxValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxValue(Integer value) {
        this.maxValue = value;
    }

    /**
     * Gets the value of the maxUnit property.
     * 
     * @return
     *     possible object is
     *     {@link StudyScheduleUnitEnumType }
     *     
     */
    public StudyScheduleUnitEnumType getMaxUnit() {
        return maxUnit;
    }

    /**
     * Sets the value of the maxUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyScheduleUnitEnumType }
     *     
     */
    public void setMaxUnit(StudyScheduleUnitEnumType value) {
        this.maxUnit = value;
    }

    /**
     * Gets the value of the refPoint property.
     * 
     * @return
     *     possible object is
     *     {@link StudyScheduleRefPointEnumType }
     *     
     */
    public StudyScheduleRefPointEnumType getRefPoint() {
        return refPoint;
    }

    /**
     * Sets the value of the refPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyScheduleRefPointEnumType }
     *     
     */
    public void setRefPoint(StudyScheduleRefPointEnumType value) {
        this.refPoint = value;
    }

}
