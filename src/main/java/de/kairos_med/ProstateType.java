
package de.kairos_med;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ProstateType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProstateType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EpisodeRef" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="GleasonScoreCauseRef" type="{http://www.kairos-med.de}GtdsDictionaryRefType" minOccurs="0"/&gt;
 *         &lt;element name="GleasonScoreResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InfestedPunch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumberOfPunches" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="NumberOfPositivePunches" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="PostOpComplicationRef" type="{http://www.kairos-med.de}GtdsDictionaryRefType" minOccurs="0"/&gt;
 *         &lt;element name="PrimaryGleasonGrading" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Psa" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="PsaDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="PunchDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="secondaryCleasonGrading" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProstateType", propOrder = {
    "episodeRef",
    "gleasonScoreCauseRef",
    "gleasonScoreResult",
    "infestedPunch",
    "numberOfPunches",
    "numberOfPositivePunches",
    "postOpComplicationRef",
    "primaryGleasonGrading",
    "psa",
    "psaDate",
    "punchDate",
    "secondaryCleasonGrading"
})
public class ProstateType {

    @XmlElement(name = "EpisodeRef", required = true)
    protected String episodeRef;
    @XmlElement(name = "GleasonScoreCauseRef")
    protected GtdsDictionaryRefType gleasonScoreCauseRef;
    @XmlElement(name = "GleasonScoreResult")
    protected String gleasonScoreResult;
    @XmlElement(name = "InfestedPunch")
    protected String infestedPunch;
    @XmlElement(name = "NumberOfPunches")
    protected Integer numberOfPunches;
    @XmlElement(name = "NumberOfPositivePunches")
    protected Integer numberOfPositivePunches;
    @XmlElement(name = "PostOpComplicationRef")
    protected GtdsDictionaryRefType postOpComplicationRef;
    @XmlElement(name = "PrimaryGleasonGrading")
    protected String primaryGleasonGrading;
    @XmlElement(name = "Psa")
    protected BigDecimal psa;
    @XmlElement(name = "PsaDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar psaDate;
    @XmlElement(name = "PunchDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar punchDate;
    protected String secondaryCleasonGrading;

    /**
     * Gets the value of the episodeRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEpisodeRef() {
        return episodeRef;
    }

    /**
     * Sets the value of the episodeRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEpisodeRef(String value) {
        this.episodeRef = value;
    }

    /**
     * Gets the value of the gleasonScoreCauseRef property.
     * 
     * @return
     *     possible object is
     *     {@link GtdsDictionaryRefType }
     *     
     */
    public GtdsDictionaryRefType getGleasonScoreCauseRef() {
        return gleasonScoreCauseRef;
    }

    /**
     * Sets the value of the gleasonScoreCauseRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GtdsDictionaryRefType }
     *     
     */
    public void setGleasonScoreCauseRef(GtdsDictionaryRefType value) {
        this.gleasonScoreCauseRef = value;
    }

    /**
     * Gets the value of the gleasonScoreResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGleasonScoreResult() {
        return gleasonScoreResult;
    }

    /**
     * Sets the value of the gleasonScoreResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGleasonScoreResult(String value) {
        this.gleasonScoreResult = value;
    }

    /**
     * Gets the value of the infestedPunch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfestedPunch() {
        return infestedPunch;
    }

    /**
     * Sets the value of the infestedPunch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfestedPunch(String value) {
        this.infestedPunch = value;
    }

    /**
     * Gets the value of the numberOfPunches property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfPunches() {
        return numberOfPunches;
    }

    /**
     * Sets the value of the numberOfPunches property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfPunches(Integer value) {
        this.numberOfPunches = value;
    }

    /**
     * Gets the value of the numberOfPositivePunches property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfPositivePunches() {
        return numberOfPositivePunches;
    }

    /**
     * Sets the value of the numberOfPositivePunches property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfPositivePunches(Integer value) {
        this.numberOfPositivePunches = value;
    }

    /**
     * Gets the value of the postOpComplicationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GtdsDictionaryRefType }
     *     
     */
    public GtdsDictionaryRefType getPostOpComplicationRef() {
        return postOpComplicationRef;
    }

    /**
     * Sets the value of the postOpComplicationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GtdsDictionaryRefType }
     *     
     */
    public void setPostOpComplicationRef(GtdsDictionaryRefType value) {
        this.postOpComplicationRef = value;
    }

    /**
     * Gets the value of the primaryGleasonGrading property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryGleasonGrading() {
        return primaryGleasonGrading;
    }

    /**
     * Sets the value of the primaryGleasonGrading property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryGleasonGrading(String value) {
        this.primaryGleasonGrading = value;
    }

    /**
     * Gets the value of the psa property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPsa() {
        return psa;
    }

    /**
     * Sets the value of the psa property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPsa(BigDecimal value) {
        this.psa = value;
    }

    /**
     * Gets the value of the psaDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPsaDate() {
        return psaDate;
    }

    /**
     * Sets the value of the psaDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPsaDate(XMLGregorianCalendar value) {
        this.psaDate = value;
    }

    /**
     * Gets the value of the punchDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPunchDate() {
        return punchDate;
    }

    /**
     * Sets the value of the punchDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPunchDate(XMLGregorianCalendar value) {
        this.punchDate = value;
    }

    /**
     * Gets the value of the secondaryCleasonGrading property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondaryCleasonGrading() {
        return secondaryCleasonGrading;
    }

    /**
     * Sets the value of the secondaryCleasonGrading property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondaryCleasonGrading(String value) {
        this.secondaryCleasonGrading = value;
    }

}
