
package de.kairos_med;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EdcTypeEnumType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EdcTypeEnumType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ORGANIZATION"/&gt;
 *     &lt;enumeration value="CONSENT"/&gt;
 *     &lt;enumeration value="DIAGNOSIS"/&gt;
 *     &lt;enumeration value="PROCEDURE"/&gt;
 *     &lt;enumeration value="DOCUMENT"/&gt;
 *     &lt;enumeration value="TITLE"/&gt;
 *     &lt;enumeration value="GENDERTYPE"/&gt;
 *     &lt;enumeration value="BIRTHDAY"/&gt;
 *     &lt;enumeration value="DEATHDATE"/&gt;
 *     &lt;enumeration value="MARITALSTATUS"/&gt;
 *     &lt;enumeration value="CITIZENSHIP"/&gt;
 *     &lt;enumeration value="DENOMINATION"/&gt;
 *     &lt;enumeration value="BLOODGROUP"/&gt;
 *     &lt;enumeration value="SPECIES"/&gt;
 *     &lt;enumeration value="PATIENTSTATUS"/&gt;
 *     &lt;enumeration value="LABORVALUE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "EdcTypeEnumType")
@XmlEnum
public enum EdcTypeEnumType {

    ORGANIZATION,
    CONSENT,
    DIAGNOSIS,
    PROCEDURE,
    DOCUMENT,
    TITLE,
    GENDERTYPE,
    BIRTHDAY,
    DEATHDATE,
    MARITALSTATUS,
    CITIZENSHIP,
    DENOMINATION,
    BLOODGROUP,
    SPECIES,
    PATIENTSTATUS,
    LABORVALUE;

    public String value() {
        return name();
    }

    public static EdcTypeEnumType fromValue(String v) {
        return valueOf(v);
    }

}
