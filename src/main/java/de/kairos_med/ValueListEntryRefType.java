
package de.kairos_med;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValueListEntryRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValueListEntryRefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ValueListRef" type="{http://www.kairos-med.de}CatalogueDataRefType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValueListEntryRefType", propOrder = {
    "code",
    "valueListRef"
})
public class ValueListEntryRefType {

    @XmlElement(name = "Code", required = true)
    protected String code;
    @XmlElement(name = "ValueListRef", required = true)
    protected CatalogueDataRefType valueListRef;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the valueListRef property.
     * 
     * @return
     *     possible object is
     *     {@link CatalogueDataRefType }
     *     
     */
    public CatalogueDataRefType getValueListRef() {
        return valueListRef;
    }

    /**
     * Sets the value of the valueListRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link CatalogueDataRefType }
     *     
     */
    public void setValueListRef(CatalogueDataRefType value) {
        this.valueListRef = value;
    }

}
