
package de.kairos_med;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CrfDependencyOperatorEnumType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CrfDependencyOperatorEnumType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="EQ"/&gt;
 *     &lt;enumeration value="NE"/&gt;
 *     &lt;enumeration value="LT"/&gt;
 *     &lt;enumeration value="GT"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CrfDependencyOperatorEnumType")
@XmlEnum
public enum CrfDependencyOperatorEnumType {

    EQ,
    NE,
    LT,
    GT;

    public String value() {
        return name();
    }

    public static CrfDependencyOperatorEnumType fromValue(String v) {
        return valueOf(v);
    }

}
