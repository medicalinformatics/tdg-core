
package de.kairos_med;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for MedicationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MedicationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="FillerOrderNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PlacerOrderNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ServiceType" type="{http://www.kairos-med.de}MedicationServiceTypeEnumType" minOccurs="0"/&gt;
 *         &lt;element name="ObservationBegin" type="{http://www.kairos-med.de}DateType" minOccurs="0"/&gt;
 *         &lt;element name="ObservationEnd" type="{http://www.kairos-med.de}DateType" minOccurs="0"/&gt;
 *         &lt;element name="ResultDate" type="{http://www.kairos-med.de}DateType" minOccurs="0"/&gt;
 *         &lt;element name="ResultStatus" type="{http://www.kairos-med.de}MedicationKindEnumType" minOccurs="0"/&gt;
 *         &lt;element name="Prescription" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Dosis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UnitTypeRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ApplicationForm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Notes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MethodOfApplication" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DosisSchema" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EpisodeRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Agent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AgentGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="isDose" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="trgDose" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="deviationDose" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="trgDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="FlexibleDataSetRef" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MedicationType", propOrder = {
    "code",
    "name",
    "fillerOrderNumber",
    "placerOrderNumber",
    "serviceType",
    "observationBegin",
    "observationEnd",
    "resultDate",
    "resultStatus",
    "prescription",
    "dosis",
    "unitTypeRef",
    "applicationForm",
    "notes",
    "methodOfApplication",
    "dosisSchema",
    "episodeRef",
    "agent",
    "agentGroup",
    "isDose",
    "trgDose",
    "deviationDose",
    "trgDate",
    "flexibleDataSetRef"
})
public class MedicationType {

    @XmlElement(name = "Code", required = true)
    protected String code;
    @XmlElement(name = "Name", required = true)
    protected String name;
    @XmlElement(name = "FillerOrderNumber", required = true)
    protected String fillerOrderNumber;
    @XmlElement(name = "PlacerOrderNumber")
    protected String placerOrderNumber;
    @XmlElement(name = "ServiceType")
    @XmlSchemaType(name = "string")
    protected MedicationServiceTypeEnumType serviceType;
    @XmlElement(name = "ObservationBegin")
    protected DateType observationBegin;
    @XmlElement(name = "ObservationEnd")
    protected DateType observationEnd;
    @XmlElement(name = "ResultDate")
    protected DateType resultDate;
    @XmlElement(name = "ResultStatus")
    @XmlSchemaType(name = "string")
    protected MedicationKindEnumType resultStatus;
    @XmlElement(name = "Prescription")
    protected Boolean prescription;
    @XmlElement(name = "Dosis")
    protected String dosis;
    @XmlElement(name = "UnitTypeRef")
    protected String unitTypeRef;
    @XmlElement(name = "ApplicationForm")
    protected String applicationForm;
    @XmlElement(name = "Notes")
    protected String notes;
    @XmlElement(name = "MethodOfApplication")
    protected String methodOfApplication;
    @XmlElement(name = "DosisSchema")
    protected String dosisSchema;
    @XmlElement(name = "EpisodeRef")
    protected String episodeRef;
    @XmlElement(name = "Agent")
    protected String agent;
    @XmlElement(name = "AgentGroup")
    protected String agentGroup;
    protected String isDose;
    protected String trgDose;
    protected String deviationDose;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar trgDate;
    @XmlElement(name = "FlexibleDataSetRef")
    protected List<String> flexibleDataSetRef;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the fillerOrderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFillerOrderNumber() {
        return fillerOrderNumber;
    }

    /**
     * Sets the value of the fillerOrderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFillerOrderNumber(String value) {
        this.fillerOrderNumber = value;
    }

    /**
     * Gets the value of the placerOrderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlacerOrderNumber() {
        return placerOrderNumber;
    }

    /**
     * Sets the value of the placerOrderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlacerOrderNumber(String value) {
        this.placerOrderNumber = value;
    }

    /**
     * Gets the value of the serviceType property.
     * 
     * @return
     *     possible object is
     *     {@link MedicationServiceTypeEnumType }
     *     
     */
    public MedicationServiceTypeEnumType getServiceType() {
        return serviceType;
    }

    /**
     * Sets the value of the serviceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link MedicationServiceTypeEnumType }
     *     
     */
    public void setServiceType(MedicationServiceTypeEnumType value) {
        this.serviceType = value;
    }

    /**
     * Gets the value of the observationBegin property.
     * 
     * @return
     *     possible object is
     *     {@link DateType }
     *     
     */
    public DateType getObservationBegin() {
        return observationBegin;
    }

    /**
     * Sets the value of the observationBegin property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateType }
     *     
     */
    public void setObservationBegin(DateType value) {
        this.observationBegin = value;
    }

    /**
     * Gets the value of the observationEnd property.
     * 
     * @return
     *     possible object is
     *     {@link DateType }
     *     
     */
    public DateType getObservationEnd() {
        return observationEnd;
    }

    /**
     * Sets the value of the observationEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateType }
     *     
     */
    public void setObservationEnd(DateType value) {
        this.observationEnd = value;
    }

    /**
     * Gets the value of the resultDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateType }
     *     
     */
    public DateType getResultDate() {
        return resultDate;
    }

    /**
     * Sets the value of the resultDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateType }
     *     
     */
    public void setResultDate(DateType value) {
        this.resultDate = value;
    }

    /**
     * Gets the value of the resultStatus property.
     * 
     * @return
     *     possible object is
     *     {@link MedicationKindEnumType }
     *     
     */
    public MedicationKindEnumType getResultStatus() {
        return resultStatus;
    }

    /**
     * Sets the value of the resultStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link MedicationKindEnumType }
     *     
     */
    public void setResultStatus(MedicationKindEnumType value) {
        this.resultStatus = value;
    }

    /**
     * Gets the value of the prescription property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPrescription() {
        return prescription;
    }

    /**
     * Sets the value of the prescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPrescription(Boolean value) {
        this.prescription = value;
    }

    /**
     * Gets the value of the dosis property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDosis() {
        return dosis;
    }

    /**
     * Sets the value of the dosis property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDosis(String value) {
        this.dosis = value;
    }

    /**
     * Gets the value of the unitTypeRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitTypeRef() {
        return unitTypeRef;
    }

    /**
     * Sets the value of the unitTypeRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitTypeRef(String value) {
        this.unitTypeRef = value;
    }

    /**
     * Gets the value of the applicationForm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationForm() {
        return applicationForm;
    }

    /**
     * Sets the value of the applicationForm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationForm(String value) {
        this.applicationForm = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

    /**
     * Gets the value of the methodOfApplication property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMethodOfApplication() {
        return methodOfApplication;
    }

    /**
     * Sets the value of the methodOfApplication property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMethodOfApplication(String value) {
        this.methodOfApplication = value;
    }

    /**
     * Gets the value of the dosisSchema property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDosisSchema() {
        return dosisSchema;
    }

    /**
     * Sets the value of the dosisSchema property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDosisSchema(String value) {
        this.dosisSchema = value;
    }

    /**
     * Gets the value of the episodeRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEpisodeRef() {
        return episodeRef;
    }

    /**
     * Sets the value of the episodeRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEpisodeRef(String value) {
        this.episodeRef = value;
    }

    /**
     * Gets the value of the agent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgent() {
        return agent;
    }

    /**
     * Sets the value of the agent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgent(String value) {
        this.agent = value;
    }

    /**
     * Gets the value of the agentGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentGroup() {
        return agentGroup;
    }

    /**
     * Sets the value of the agentGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentGroup(String value) {
        this.agentGroup = value;
    }

    /**
     * Gets the value of the isDose property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDose() {
        return isDose;
    }

    /**
     * Sets the value of the isDose property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDose(String value) {
        this.isDose = value;
    }

    /**
     * Gets the value of the trgDose property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrgDose() {
        return trgDose;
    }

    /**
     * Sets the value of the trgDose property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrgDose(String value) {
        this.trgDose = value;
    }

    /**
     * Gets the value of the deviationDose property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviationDose() {
        return deviationDose;
    }

    /**
     * Sets the value of the deviationDose property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviationDose(String value) {
        this.deviationDose = value;
    }

    /**
     * Gets the value of the trgDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTrgDate() {
        return trgDate;
    }

    /**
     * Sets the value of the trgDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTrgDate(XMLGregorianCalendar value) {
        this.trgDate = value;
    }

    /**
     * Gets the value of the flexibleDataSetRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flexibleDataSetRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlexibleDataSetRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getFlexibleDataSetRef() {
        if (flexibleDataSetRef == null) {
            flexibleDataSetRef = new ArrayList<String>();
        }
        return this.flexibleDataSetRef;
    }

}
