
package de.kairos_med;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         ReferenceType for StudyVisitTemplate
 *       
 * 
 * <p>Java class for StudyVisitTemplateRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StudyVisitTemplateRefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="StudyVisitTemplateName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="StudyPhaseTemplateName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PatientGroupName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="FlexiStudyRef" type="{http://www.kairos-med.de}FlexiStudyRefType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StudyVisitTemplateRefType", propOrder = {
    "studyVisitTemplateName",
    "studyPhaseTemplateName",
    "patientGroupName",
    "flexiStudyRef"
})
public class StudyVisitTemplateRefType {

    @XmlElement(name = "StudyVisitTemplateName", required = true)
    protected String studyVisitTemplateName;
    @XmlElement(name = "StudyPhaseTemplateName", required = true)
    protected String studyPhaseTemplateName;
    @XmlElement(name = "PatientGroupName", required = true)
    protected String patientGroupName;
    @XmlElement(name = "FlexiStudyRef", required = true)
    protected FlexiStudyRefType flexiStudyRef;

    /**
     * Gets the value of the studyVisitTemplateName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudyVisitTemplateName() {
        return studyVisitTemplateName;
    }

    /**
     * Sets the value of the studyVisitTemplateName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudyVisitTemplateName(String value) {
        this.studyVisitTemplateName = value;
    }

    /**
     * Gets the value of the studyPhaseTemplateName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudyPhaseTemplateName() {
        return studyPhaseTemplateName;
    }

    /**
     * Sets the value of the studyPhaseTemplateName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudyPhaseTemplateName(String value) {
        this.studyPhaseTemplateName = value;
    }

    /**
     * Gets the value of the patientGroupName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientGroupName() {
        return patientGroupName;
    }

    /**
     * Sets the value of the patientGroupName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientGroupName(String value) {
        this.patientGroupName = value;
    }

    /**
     * Gets the value of the flexiStudyRef property.
     * 
     * @return
     *     possible object is
     *     {@link FlexiStudyRefType }
     *     
     */
    public FlexiStudyRefType getFlexiStudyRef() {
        return flexiStudyRef;
    }

    /**
     * Sets the value of the flexiStudyRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexiStudyRefType }
     *     
     */
    public void setFlexiStudyRef(FlexiStudyRefType value) {
        this.flexiStudyRef = value;
    }

}
