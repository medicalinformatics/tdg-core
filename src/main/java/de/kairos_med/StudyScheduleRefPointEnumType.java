
package de.kairos_med;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StudyScheduleRefPointEnumType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="StudyScheduleRefPointEnumType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ONSET"/&gt;
 *     &lt;enumeration value="VISIT"/&gt;
 *     &lt;enumeration value="CRF"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "StudyScheduleRefPointEnumType")
@XmlEnum
public enum StudyScheduleRefPointEnumType {

    ONSET,
    VISIT,
    CRF;

    public String value() {
        return name();
    }

    public static StudyScheduleRefPointEnumType fromValue(String v) {
        return valueOf(v);
    }

}
