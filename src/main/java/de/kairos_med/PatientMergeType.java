
package de.kairos_med;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * During a patient merge all Properties are written from the source to the destination patient. After a
 *         successful merge, the source patient is deleted.
 * 
 * <p>Java class for PatientMergeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PatientMergeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SourcePatient" type="{http://www.kairos-med.de}PatientRefType"/&gt;
 *         &lt;element name="DestinationPatient" type="{http://www.kairos-med.de}PatientRefType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PatientMergeType", propOrder = {
    "sourcePatient",
    "destinationPatient"
})
public class PatientMergeType {

    @XmlElement(name = "SourcePatient", required = true)
    protected PatientRefType sourcePatient;
    @XmlElement(name = "DestinationPatient", required = true)
    protected PatientRefType destinationPatient;

    /**
     * Gets the value of the sourcePatient property.
     * 
     * @return
     *     possible object is
     *     {@link PatientRefType }
     *     
     */
    public PatientRefType getSourcePatient() {
        return sourcePatient;
    }

    /**
     * Sets the value of the sourcePatient property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientRefType }
     *     
     */
    public void setSourcePatient(PatientRefType value) {
        this.sourcePatient = value;
    }

    /**
     * Gets the value of the destinationPatient property.
     * 
     * @return
     *     possible object is
     *     {@link PatientRefType }
     *     
     */
    public PatientRefType getDestinationPatient() {
        return destinationPatient;
    }

    /**
     * Sets the value of the destinationPatient property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientRefType }
     *     
     */
    public void setDestinationPatient(PatientRefType value) {
        this.destinationPatient = value;
    }

}
