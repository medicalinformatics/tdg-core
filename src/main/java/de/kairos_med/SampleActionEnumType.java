
package de.kairos_med;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SampleActionEnumType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SampleActionEnumType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="STORAGE"/&gt;
 *     &lt;enumeration value="ALIQUOTATION"/&gt;
 *     &lt;enumeration value="ABSTRACTION"/&gt;
 *     &lt;enumeration value="MODIFICATION"/&gt;
 *     &lt;enumeration value="DELETION"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SampleActionEnumType")
@XmlEnum
public enum SampleActionEnumType {

    STORAGE,
    ALIQUOTATION,
    ABSTRACTION,
    MODIFICATION,
    DELETION;

    public String value() {
        return name();
    }

    public static SampleActionEnumType fromValue(String v) {
        return valueOf(v);
    }

}
