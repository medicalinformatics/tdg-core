
package de.kairos_med;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DiscrepancyResponseStatusEnumType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DiscrepancyResponseStatusEnumType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="RESPONDED"/&gt;
 *     &lt;enumeration value="RESPONDED_BY_VERIFIER"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "DiscrepancyResponseStatusEnumType")
@XmlEnum
public enum DiscrepancyResponseStatusEnumType {

    RESPONDED,
    RESPONDED_BY_VERIFIER;

    public String value() {
        return name();
    }

    public static DiscrepancyResponseStatusEnumType fromValue(String v) {
        return valueOf(v);
    }

}
