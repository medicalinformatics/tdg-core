
package de.kairos_med;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *       
 * 
 * <p>Java class for ValidationRuleType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidationRuleType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ValidationType" type="{http://www.kairos-med.de}ValidationTypeEnumType"/&gt;
 *         &lt;element name="RegEx" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidationRuleType", propOrder = {
    "validationType",
    "regEx"
})
public class ValidationRuleType {

    @XmlElement(name = "ValidationType", required = true)
    @XmlSchemaType(name = "string")
    protected ValidationTypeEnumType validationType;
    @XmlElement(name = "RegEx", required = true)
    protected String regEx;

    /**
     * Gets the value of the validationType property.
     * 
     * @return
     *     possible object is
     *     {@link ValidationTypeEnumType }
     *     
     */
    public ValidationTypeEnumType getValidationType() {
        return validationType;
    }

    /**
     * Sets the value of the validationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidationTypeEnumType }
     *     
     */
    public void setValidationType(ValidationTypeEnumType value) {
        this.validationType = value;
    }

    /**
     * Gets the value of the regEx property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegEx() {
        return regEx;
    }

    /**
     * Sets the value of the regEx property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegEx(String value) {
        this.regEx = value;
    }

}
