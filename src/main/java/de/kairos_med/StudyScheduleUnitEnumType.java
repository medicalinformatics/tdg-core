
package de.kairos_med;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StudyScheduleUnitEnumType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="StudyScheduleUnitEnumType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="DAY"/&gt;
 *     &lt;enumeration value="HOUR"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "StudyScheduleUnitEnumType")
@XmlEnum
public enum StudyScheduleUnitEnumType {

    DAY,
    HOUR;

    public String value() {
        return name();
    }

    public static StudyScheduleUnitEnumType fromValue(String v) {
        return valueOf(v);
    }

}
