
package de.kairos_med;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValueListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValueListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.kairos-med.de}CatalogueItemType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DescMultilingualEntries" type="{http://www.kairos-med.de}MultilingualEntryType" maxOccurs="2" minOccurs="0"/&gt;
 *         &lt;element name="CustomCatalogEntry" type="{http://www.kairos-med.de}CustomCatalogEntryType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="SortAlphabetical" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="replaceEntries" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValueListType", propOrder = {
    "descMultilingualEntries",
    "customCatalogEntry",
    "sortAlphabetical"
})
public class ValueListType
    extends CatalogueItemType
{

    @XmlElement(name = "DescMultilingualEntries")
    protected List<MultilingualEntryType> descMultilingualEntries;
    @XmlElement(name = "CustomCatalogEntry")
    protected List<CustomCatalogEntryType> customCatalogEntry;
    @XmlElement(name = "SortAlphabetical", defaultValue = "true")
    protected Boolean sortAlphabetical;
    @XmlAttribute(name = "replaceEntries")
    protected Boolean replaceEntries;

    /**
     * Gets the value of the descMultilingualEntries property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the descMultilingualEntries property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDescMultilingualEntries().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MultilingualEntryType }
     * 
     * 
     */
    public List<MultilingualEntryType> getDescMultilingualEntries() {
        if (descMultilingualEntries == null) {
            descMultilingualEntries = new ArrayList<MultilingualEntryType>();
        }
        return this.descMultilingualEntries;
    }

    /**
     * Gets the value of the customCatalogEntry property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customCatalogEntry property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomCatalogEntry().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomCatalogEntryType }
     * 
     * 
     */
    public List<CustomCatalogEntryType> getCustomCatalogEntry() {
        if (customCatalogEntry == null) {
            customCatalogEntry = new ArrayList<CustomCatalogEntryType>();
        }
        return this.customCatalogEntry;
    }

    /**
     * Gets the value of the sortAlphabetical property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSortAlphabetical() {
        return sortAlphabetical;
    }

    /**
     * Sets the value of the sortAlphabetical property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSortAlphabetical(Boolean value) {
        this.sortAlphabetical = value;
    }

    /**
     * Gets the value of the replaceEntries property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isReplaceEntries() {
        if (replaceEntries == null) {
            return false;
        } else {
            return replaceEntries;
        }
    }

    /**
     * Sets the value of the replaceEntries property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReplaceEntries(Boolean value) {
        this.replaceEntries = value;
    }

}
