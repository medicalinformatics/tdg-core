
package de.kairos_med;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValueReferenceTypeEnumType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ValueReferenceTypeEnumType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="STRING"/&gt;
 *     &lt;enumeration value="INTEGER"/&gt;
 *     &lt;enumeration value="BIGDECIMAL"/&gt;
 *     &lt;enumeration value="BOOLEAN"/&gt;
 *     &lt;enumeration value="DATE"/&gt;
 *     &lt;enumeration value="BINARYFILE"/&gt;
 *     &lt;enumeration value="USAGEENTRY"/&gt;
 *     &lt;enumeration value="CONTACT"/&gt;
 *     &lt;enumeration value="ORGANIZATION"/&gt;
 *     &lt;enumeration value="ICD"/&gt;
 *     &lt;enumeration value="OPS"/&gt;
 *     &lt;enumeration value="BLOODGROUP"/&gt;
 *     &lt;enumeration value="TITLE"/&gt;
 *     &lt;enumeration value="MARITALSTATUS"/&gt;
 *     &lt;enumeration value="CITIZENSHIP"/&gt;
 *     &lt;enumeration value="DENOMINATION"/&gt;
 *     &lt;enumeration value="ETHNICITY"/&gt;
 *     &lt;enumeration value="SPECIES"/&gt;
 *     &lt;enumeration value="COUNTRY"/&gt;
 *     &lt;enumeration value="GENDER"/&gt;
 *     &lt;enumeration value="PROJECT"/&gt;
 *     &lt;enumeration value="SAMPLEPARTNER"/&gt;
 *     &lt;enumeration value="RECEPTABLE"/&gt;
 *     &lt;enumeration value="SAMPLEDONATOR"/&gt;
 *     &lt;enumeration value="ATTENDINGDOCTOR"/&gt;
 *     &lt;enumeration value="SAMPLINGMOMENT"/&gt;
 *     &lt;enumeration value="SAMPLELOCALISATION"/&gt;
 *     &lt;enumeration value="SAMPLETYPE"/&gt;
 *     &lt;enumeration value="CATALOGENTRY"/&gt;
 *     &lt;enumeration value="SAMPLEKIND"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ValueReferenceTypeEnumType")
@XmlEnum
public enum ValueReferenceTypeEnumType {

    STRING,
    INTEGER,
    BIGDECIMAL,
    BOOLEAN,
    DATE,
    BINARYFILE,
    USAGEENTRY,
    CONTACT,
    ORGANIZATION,
    ICD,
    OPS,
    BLOODGROUP,
    TITLE,
    MARITALSTATUS,
    CITIZENSHIP,
    DENOMINATION,
    ETHNICITY,
    SPECIES,
    COUNTRY,
    GENDER,
    PROJECT,
    SAMPLEPARTNER,
    RECEPTABLE,
    SAMPLEDONATOR,
    ATTENDINGDOCTOR,
    SAMPLINGMOMENT,
    SAMPLELOCALISATION,
    SAMPLETYPE,
    CATALOGENTRY,
    SAMPLEKIND;

    public String value() {
        return name();
    }

    public static ValueReferenceTypeEnumType fromValue(String v) {
        return valueOf(v);
    }

}
