
package de.kairos_med;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         Data type class for flexible long-string values.
 *       
 * 
 * <p>Java class for FlexibleLongStringType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FlexibleLongStringType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.kairos-med.de}FlexibleValueType"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlexibleLongStringType")
public class FlexibleLongStringType
    extends FlexibleValueType
{


}
