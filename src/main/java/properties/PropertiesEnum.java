
package properties;

/**
 * Created by huberma on 14.02.2019.
 */

public enum PropertiesEnum {

  //  PATH("path"),
    PORT("port"),
    MITRO("mitro"),
    URLDB("urlDB"),
    USER("userFTP"),
    USERDB("userDB"),
    SERVER("server"),
    PASSWORD("passwordFTP"),
    PASSWORDDB("passwordDB"),
    PATH ("path");


    final String key;

    PropertiesEnum(final String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return key;
    }

}
