package properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class GetPropertyValues {

    public String getPropertyConnection() throws IOException{

            Properties property = new Properties();

            String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
            String propertyFile = rootPath + "config.properties";

            property.load(new FileInputStream(propertyFile));

           String server = property.getProperty((PropertiesEnum.MITRO).toString());

           return server;
    }
}
