package dktk.testdatengenerator.http;

import de.kairos_med.AddressType;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.net.HttpURLConnection;

/**
 * Created by brennert on 27.06.2017.
 */
@Ignore
public class NominatiReverseRequestTest {

    @Test
    public void testConstructor() throws Exception {
        NominatiReverseRequest nominatiReverseRequest = new NominatiReverseRequest(48.77919575, 9.1928044337031);

        Assert.assertNotNull("The constructor should initialize the connection", nominatiReverseRequest.getConnection());
        Assert.assertTrue("The connection should be an instance of HTTPUrlConnection", nominatiReverseRequest.getConnection() instanceof HttpURLConnection);
        Assert.assertEquals("The connection should have this url:",
                "http://nominatim.openstreetmap.org/reverse?format=xml&zoom=18&addressdetails=1&lat=48.77919575&lon=9.1928044337031",
                nominatiReverseRequest.getConnection().getURL().toString() );
    }

    @Test
    public void testGetResponse() throws Exception {
        NominatiReverseRequest nominatiReverseRequest = new NominatiReverseRequest(48.77919575, 9.1928044337031);
        AddressType result = nominatiReverseRequest.getAddressType();

        Assert.assertNotNull(result.getCity());
        Assert.assertNotNull(result.getStreet());
        Assert.assertNotNull(result.getCountry());
        Assert.assertNotNull(result.getStreetNo());
    }
}
