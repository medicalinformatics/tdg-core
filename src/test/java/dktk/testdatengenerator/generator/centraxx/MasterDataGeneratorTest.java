package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.GenderTypeEnum;
import de.kairos_med.PatientMasterdataType;
import dktk.testdatengenerator.db.PatientQueries;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.enums.TitleEnum;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.datatype.DatatypeConstants;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.hamcrest.CoreMatchers.*;

/**
 * Created by BrennerT on 20.06.2017.
 */
@Ignore
public class MasterDataGeneratorTest {

//    private static PatientMasterdataType testMasterData;
//
//    @BeforeClass
//    public static void setUp() {
//        MasterDataGenerator masterDataGenerator = new MasterDataGenerator(new TDGProfile());
//        testMasterData = masterDataGenerator.generate();
//    }
//
//    @Test
//    public void testGenerateName() throws Exception {
//        Assert.assertThat("Hello World is no proper name for a person", testMasterData.getFirstName(), is(not("Hello World")));
//        Assert.assertTrue("The FirstName must not be longer than 32 characters", testMasterData.getFirstName().length() <= 31);
//        Assert.assertTrue("The LastName must not be longer than 64 characters", testMasterData.getLastName().length() <= 64);
//        Assert.assertTrue("The whole string should be uppercase", testMasterData.getFirstName().toUpperCase().equals(testMasterData.getFirstName()));
//        Assert.assertTrue("The first character of the LastName must be uppercase", Character.isUpperCase(testMasterData.getLastName().charAt(0)));
//    }
//
//    @Test
//    public void testGetNameFromDatabase() throws Exception {
//        PatientQueries patientQueries = new PatientQueries();
//        String nameFromDatabase = patientQueries.getNameFromDatabase(GenderTypeEnum.MALE, 3.493);
//        // here should not be an error
//        patientQueries.getNameFromDatabase(GenderTypeEnum.MALE, 4.282);
//        patientQueries.getNameFromDatabase(GenderTypeEnum.FEMALE, 0.251);
//
//    }
//
//    @Test
//    public void testGenerateDateOfBirthAndDeath() throws Exception {
//        Date currentDate = new Date();
//        GregorianCalendar gregory = new GregorianCalendar();
//        gregory.setTime(currentDate);
//        if (testMasterData.getDateOfDeath() != null)
//            Assert.assertTrue("The BirthDate shouldn't be after the DeathDate", testMasterData.getDateOfBirth().getDate().compare(testMasterData.getDateOfDeath().getDate()) != DatatypeConstants.GREATER);
//    }
//
//    @Test
//    public void testGenerateCitizenShip() throws Exception {
//        Assert.assertEquals("Citizenship should be DEU(German), currently fixed value", "DEU", testMasterData.getCitizenshipTypeRef());
//    }
//
//    @Test
//    public void testGenerateBirthplace() throws Exception {
//        Assert.assertNotNull("Birthplace should not be null");
//    }
//
//    @Test
//    public void testGenerateMaritalStatus() throws Exception {
//        Assert.assertThat("Must be any of single, married, and divorced",
//                testMasterData.getMaritalStatusTypeRef(), anyOf(is("Annu"), is("Gesch"), is("Interl"), is("Getr"), is("Ver"), is("Poly"), is("Led"), is("Verp"), is("Verw")));
//    }
//
//    @Test
//    public void testGenerateTitle() throws Exception {
//        if(testMasterData.getTitleTypeRef() != null)
//            Assert.assertThat(testMasterData.getTitleTypeRef(),
//                    anyOf(is(TitleEnum.PDDrMedDrMed.value()), is(TitleEnum.DrMed.value()), is(TitleEnum.Prof.value()), is(TitleEnum.ProfDrMed.value())));
//    }
//
//    @Test
//    public void testGenerateGender() throws Exception {
//        Assert.assertThat("Must match any of the Enumvalues", testMasterData.getGender(), is(anyOf(is(GenderTypeEnum.FEMALE), is(GenderTypeEnum.MALE), is(GenderTypeEnum.HERMAPHRODIT), is(GenderTypeEnum.OTHER), is(GenderTypeEnum.UNDEFINED), is(GenderTypeEnum.UNKNOWN))));
//    }
//
//    @Test
//    @Ignore
//    public void testGenerateEthnicityType() throws Exception {
//        Assert.assertNotNull(testMasterData.getEthnicityTypeRef());
//    }

}
