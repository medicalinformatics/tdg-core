package dktk.testdatengenerator.generator.centraxx;

import dktk.testdatengenerator.customelements.Patient;
import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.db.model.TDGProfile;
import org.junit.*;



import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;

/**
 * Created by brennert on 04.07.2017.
 */
@Ignore
public class PatientGeneratorTest {

    private DBConnector dbConnector;

    @Before
    public void setUp() throws Exception {
        dbConnector = new DBConnector();
        dbConnector.openConnection();
    }

    @After
    public void tearDown() throws Exception {
        dbConnector.closeConnection();
    }

    @Test
    public void testGenerateWithSize() throws Exception {

        TDGProfile tdgProfile = new TDGProfile(dbConnector);

        PatientGenerator patientGenerator = new PatientGenerator(tdgProfile, dbConnector);
        ArrayList<Patient> moreSize = patientGenerator.generate(15);
        Assert.assertNotNull(moreSize);
        Assert.assertThat("The generated list should have the given size", moreSize.size(), is(15));

        ArrayList<Patient> emptySize = patientGenerator.generate(0);
        Assert.assertThat("The generated list should have the given size", emptySize.size(), is(0));

        ArrayList<Patient> lowerSize = patientGenerator.generate(5);
        Assert.assertThat("The generated list should have the given size", lowerSize.size(), is(5));

    }

    @Test
    public void testCancerProfile() throws Exception {

        TDGProfile tdgProfile = new TDGProfile("cancerProfile", null, dbConnector);

        PatientGenerator patientGenerator = new PatientGenerator(tdgProfile, dbConnector);
        List<Patient> cancerPatients = patientGenerator.generate();
        Assert.assertNotNull(cancerPatients);
        Assert.assertThat("By default the patient Generator should generateTnmAndAddToTumour 10 Patients", cancerPatients.size(), is(10));
        Assert.assertTrue("In cancer profile, the generator should generateTnmAndAddToTumour cancer data.", cancerPatients.get(0).getPatientDataSet().getDiagnosis().get(0).getDiagnosisCode().matches("[CD]\\d\\d\\.?\\d?\\d?[\\+\\*]?"));


    }

    @Test
    public void testNormalProfile() throws Exception {

        TDGProfile tdgProfile = new TDGProfile(dbConnector);

        PatientGenerator patientGenerator = new PatientGenerator(tdgProfile, dbConnector);
        ArrayList<Patient> normalPatients = patientGenerator.generate();
        Assert.assertNotNull(normalPatients);
        Assert.assertThat("By default the patient Generator should generateTnmAndAddToTumour 10 Patients", normalPatients.size(), is(10));
        Assert.assertFalse("By default the patient Generator should generateTnmAndAddToTumour medical Data", normalPatients.get(0).getPatientDataSet().getDiagnosis().isEmpty());

    }
}
