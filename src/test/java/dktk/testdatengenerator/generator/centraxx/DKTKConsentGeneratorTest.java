package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.FlexiFlagItemType;
import dktk.testdatengenerator.customelements.Patient;
import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.db.model.TDGProfile;
import org.junit.*;



import javax.xml.datatype.DatatypeConstants;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;

/**
 * Created by brennert on 26.07.2017.
 */
@Ignore
public class DKTKConsentGeneratorTest {


    private DBConnector dbConnector;

    @Before
    public void setUp() throws Exception {
        dbConnector = new DBConnector();
        dbConnector.openConnection();
    }

    @After
    public void tearDown() throws Exception {
        dbConnector.closeConnection();
    }

    @Test
    public void testGenerateDKTKConsent() throws Exception {
        TDGProfile tdgProfile = new TDGProfile("cancerProfile", null, dbConnector);
        DKTKConsentGenerator dktkConsentGenerator = new DKTKConsentGenerator(tdgProfile);
        Patient patient = new PatientGenerator(tdgProfile, dbConnector).generate(1).get(0);

        FlexiFlagItemType dktkConsent = dktkConsentGenerator.generateDktkFlag(patient, patient.getPatientDataSet().getDiagnosis().get(0));

        Assert.assertThat(dktkConsent.getDefinitionRef(), is("DKTK"));
        Assert.assertNotNull(dktkConsent.isPatientScope());
        Assert.assertTrue(dktkConsent.getValidFrom().getDate().compare(dktkConsent.getValidUntil().getDate()) == DatatypeConstants.LESSER);
    }

    @Test
    public void testGenerateNoDKTKConsent() throws Exception {
        TDGProfile tdgProfile = new TDGProfile(dbConnector);
        tdgProfile.setProbability("pDKTKConsent", 0.0);

        DKTKConsentGenerator dktkConsentGenerator = new DKTKConsentGenerator(tdgProfile);
        Patient patient = new PatientGenerator(tdgProfile, dbConnector).generate(1).get(0);

        FlexiFlagItemType dktkConsent = dktkConsentGenerator.generateDktkFlag(patient, patient.getPatientDataSet().getDiagnosis().get(0));

        Assert.assertNull(dktkConsent);
    }

}
