package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.GenderTypeEnum;
import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.util.RandomOperations;
import org.junit.*;


import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.*;

/**
 * Created by brennert on 10.07.2017.
 */
@Ignore
public class RandomOperationsTest {

    private DBConnector dbConnector;

    @Before
    public void setUp() throws Exception {
        dbConnector = new DBConnector();
        dbConnector.openConnection();
    }

    @After
    public void tearDown() throws Exception {
        dbConnector.closeConnection();
    }

    @Test
    public void testRandomFromEnum() throws Exception {
        TDGProfile tdgProfile = new TDGProfile(dbConnector);
        tdgProfile.setProbability("pFEMALE", 0.2);
        tdgProfile.setProbability("pMALE", 0.0);
        tdgProfile.setProbability("pOTHER", 0.0);
        tdgProfile.setProbability("pHERMAPHRODIT", 0.0);
        tdgProfile.setProbability("pUNDEFINED", 0.0);
        tdgProfile.setProbability("pUNKNOWN", 0.0);

        RandomOperations randomOperations = new RandomOperations(tdgProfile);

        Assert.assertNotNull(randomOperations.getRandomFromEnum(GenderTypeEnum.class, "",""));

        Assert.assertThat(randomOperations.getRandomFromEnum(GenderTypeEnum.class, "", ""),
                is(GenderTypeEnum.FEMALE));
    }

    @Test
    public void testRandomFromArrayList() throws Exception {
        ArrayList<Integer> numberList = new ArrayList<>();
        numberList.add(0);
        numberList.add(1);
        numberList.add(2);
        numberList.add(3);
        numberList.add(4);
        RandomOperations randomOperations = new RandomOperations(new TDGProfile(dbConnector));
        Assert.assertThat(randomOperations.getRandomFromArrayList(numberList), is(instanceOf(Integer.class)));
        Assert.assertThat(randomOperations.getRandomFromArrayList(numberList), anyOf(is(0), is(1), is(2), is(3), is(4)));
    }

    @Test
    public void testRandomFromArrayListFailing() throws Exception {
        ArrayList<Integer> numberList = new ArrayList<>();
        RandomOperations randomOperations = new RandomOperations(new TDGProfile(dbConnector));
        Assert.assertNull(randomOperations.getRandomFromArrayList(numberList));
    }

}
