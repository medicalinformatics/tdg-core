package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.*;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.enums.GradingEnum;
import dktk.testdatengenerator.enums.OncologyOrganisationUnitsEnum;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.datatype.DatatypeConstants;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;

/**
 * Created by brennert on 20.07.2017.
 */
@Ignore
public class TumourGeneratorTest {

//    @Test
//    public void testTransferDiagnosisData() throws Exception {
//        TDGProfile cancerProfile = new TDGProfile("cancerProfile", null);
//        PatientGenerator patientGenerator = new PatientGenerator(cancerProfile);
//        TumourGenerator tumourGenerator = new TumourGenerator(cancerProfile);
//        PatientDataSetType patient = patientGenerator.generate(1).get(0);
//        GTDSTumourEIType tumour = tumourGenerator.generate(patient).get(0);
//        if (patient.getDiagnosis() == null)
//            Assert.assertNull(tumour);
//        else {
//            Assert.assertNotNull(tumour);
//            DiagnosisType diagnosis = patient.getDiagnosis().get(0);
//            Assert.assertThat(tumour.getEpisodeRef(), is(diagnosis.getEpisodeRef()));
//            Assert.assertThat(tumour.getICDOCode(), is(diagnosis.getICDOCode()));
//            Assert.assertThat(tumour.getDiagnosisText(), is(diagnosis.getText()));
//            EpisodeType diagnosisEpisode = patient.getEpisode().get(0);
//            if (diagnosisEpisode.getEpisodeRef() == diagnosis.getEpisodeRef())
//                System.out.println("This is true");
//            Assert.assertThat(tumour.getAccomplishingOrganisationUnitRef(), is(diagnosisEpisode.getHabitationRef()));
//            Assert.assertThat(tumour.getOrganisationUnitTypeRef(), is(diagnosisEpisode.getHabitationRef()));
//            Assert.assertThat(tumour.getDiagnosisDate(), is(diagnosis.getDate()));
//            Assert.assertThat(tumour.getAccomplishedByText(), is(OncologyOrganisationUnitsEnum.valueOf(diagnosisEpisode.getHabitationRef().getValue()).getDescription()));
//        }
//
//    }
//
//    @Test
//    public void testSetAdmissionDate() throws Exception {
//        TDGProfile cancerProfile = new TDGProfile("cancerProfile", null);
//        PatientGenerator patientGenerator = new PatientGenerator(cancerProfile);
//        TumourGenerator tumourGenerator = new TumourGenerator(cancerProfile);
//        PatientDataSetType patient = patientGenerator.generate(1).get(0);
//        GTDSTumourEIType tumour = tumourGenerator.generate(patient).get(0);
//        if (patient.getDiagnosis() == null)
//            Assert.assertNull(tumour);
//        else {
//            Assert.assertThat(tumour.getAdmissionDate().getDate().compare(patient.getDiagnosis().get(0).getDate().getDate()), anyOf(is(DatatypeConstants.LESSER), is(DatatypeConstants.EQUAL)));
//        }
//    }
//
//    @Test
//    public void setCaptureCause() throws Exception {
//        TDGProfile cancerProfile = new TDGProfile("cancerProfile", null);
//        PatientGenerator patientGenerator = new PatientGenerator(cancerProfile);
//        TumourGenerator tumourGenerator = new TumourGenerator(cancerProfile);
//        PatientDataSetType patient = patientGenerator.generate(1).get(0);
//        GTDSTumourEIType tumour = tumourGenerator.generate(patient).get(0);
//        if (patient.getDiagnosis() == null)
//            Assert.assertNull(tumour);
//        else {
//            Assert.assertThat(tumour.getCaptureCause(), anyOf(
//                    is(TumourCaptureCauseGTDSEnumType.A), is(TumourCaptureCauseGTDSEnumType.D), is(TumourCaptureCauseGTDSEnumType.E), is(TumourCaptureCauseGTDSEnumType.L),
//                    is(TumourCaptureCauseGTDSEnumType.S), is(TumourCaptureCauseGTDSEnumType.W), is(TumourCaptureCauseGTDSEnumType.X), is(TumourCaptureCauseGTDSEnumType.Z)));
//        }
//    }
//
//    @Test
//    public void testFinishState() throws Exception {
//        TDGProfile cancerProfile = new TDGProfile("cancerProfile", null);
//        PatientGenerator patientGenerator = new PatientGenerator(cancerProfile);
//        TumourGenerator tumourGenerator = new TumourGenerator(cancerProfile);
//        PatientDataSetType patient = patientGenerator.generate(1).get(0);
//        GTDSTumourEIType tumour = tumourGenerator.generate(patient).get(0);
//        if (patient.getDiagnosis() == null)
//            Assert.assertNull(tumour);
//        else {
//            Assert.assertThat(tumour.getCaptureFinishState(), anyOf(is(YesNoXGTDSEnumType.J), is(YesNoXGTDSEnumType.N), is(YesNoXGTDSEnumType.X)));
//        }
//    }
//
//    @Test
//    public void testCodeType() throws Exception {
//        TDGProfile cancerProfile = new TDGProfile("cancerProfile", null);
//        PatientGenerator patientGenerator = new PatientGenerator(cancerProfile);
//        TumourGenerator tumourGenerator = new TumourGenerator(cancerProfile);
//        PatientDataSetType patient = patientGenerator.generate(1).get(0);
//        GTDSTumourEIType tumour = tumourGenerator.generate(patient).get(0);
//        if (patient.getDiagnosis() == null)
//            Assert.assertNull(tumour);
//        else {
//            Assert.assertThat(tumour.getCodeType(), anyOf(is(CodeTypeGTDSEnumType.A), is(CodeTypeGTDSEnumType.S), is(CodeTypeGTDSEnumType.T), is(CodeTypeGTDSEnumType.X)));
//        }
//    }
//
//    @Test
//    public void testExpansion() throws Exception {
//        TDGProfile cancerProfile = new TDGProfile("cancerProfile", null);
//        PatientGenerator patientGenerator = new PatientGenerator(cancerProfile);
//        TumourGenerator tumourGenerator = new TumourGenerator(cancerProfile);
//        PatientDataSetType patient = patientGenerator.generate(1).get(0);
//        GTDSTumourEIType tumour = tumourGenerator.generate(patient).get(0);
//        if (patient.getDiagnosis() == null)
//            Assert.assertNull(tumour);
//        else {
//            Assert.assertThat(tumour.getExpansionGeneral(), anyOf(
//                    is(ExpansionGeneralGTDSEnumType.I), is(ExpansionGeneralGTDSEnumType.L), is(ExpansionGeneralGTDSEnumType.M),
//                    is(ExpansionGeneralGTDSEnumType.R), is(ExpansionGeneralGTDSEnumType.S), is(ExpansionGeneralGTDSEnumType.X)));
//        }
//    }
//
//    @Test
//    public void testPatientEnlightenStatus() throws Exception {
//        TDGProfile cancerProfile = new TDGProfile("cancerProfile", null);
//        PatientGenerator patientGenerator = new PatientGenerator(cancerProfile);
//        TumourGenerator tumourGenerator = new TumourGenerator(cancerProfile);
//        PatientDataSetType patient = patientGenerator.generate(1).get(0);
//        GTDSTumourEIType tumour = tumourGenerator.generate(patient).get(0);
//        if (patient.getDiagnosis() == null)
//            Assert.assertNull(tumour);
//        else {
////            Assert.assertNotNull(tumour.getEnlightenDate());
//            Assert.assertThat(tumour.getPatientEnlightenStatus(), anyOf(
//                    is(PatientEnlightenStatusGTDSEnumType.H), is(PatientEnlightenStatusGTDSEnumType.U),
//                    is(PatientEnlightenStatusGTDSEnumType.V), is(PatientEnlightenStatusGTDSEnumType.X)));
//        }
//    }
//
//    @Test
//    public void testTumourSource() throws Exception {
//        TDGProfile cancerProfile = new TDGProfile("cancerProfile", null);
//        PatientGenerator patientGenerator = new PatientGenerator(cancerProfile);
//        TumourGenerator tumourGenerator = new TumourGenerator(cancerProfile);
//        PatientDataSetType patient = patientGenerator.generate(1).get(0);
//        GTDSTumourEIType tumour = tumourGenerator.generate(patient).get(0);
//        if (patient.getDiagnosis() == null)
//            Assert.assertNull(tumour);
//        else {
//            Assert.assertThat(tumour.getSource(), anyOf(
//                    is(SourceTypeGTDSEnumType.A), is(SourceTypeGTDSEnumType.E), is(SourceTypeGTDSEnumType.H), is(SourceTypeGTDSEnumType.K),
//                    is(SourceTypeGTDSEnumType.M), is(SourceTypeGTDSEnumType.R), is(SourceTypeGTDSEnumType.X), is(SourceTypeGTDSEnumType.S)));
//        }
//    }
//
//    @Test
//    public void testVisitCause() throws Exception {
//        TDGProfile cancerProfile = new TDGProfile("cancerProfile", null);
//        PatientGenerator patientGenerator = new PatientGenerator(cancerProfile);
//        TumourGenerator tumourGenerator = new TumourGenerator(cancerProfile);
//        PatientDataSetType patient = patientGenerator.generate(1).get(0);
//        GTDSTumourEIType tumour = tumourGenerator.generate(patient).get(0);
//        if (patient.getDiagnosis() == null)
//            Assert.assertNull(tumour);
//        else {
//            Assert.assertThat(tumour.getVisitCause(), anyOf(
//                    is(VisitCauseGTDSEnumType.A), is(VisitCauseGTDSEnumType.C), is(VisitCauseGTDSEnumType.F), is(VisitCauseGTDSEnumType.L), is(VisitCauseGTDSEnumType.P),
//                    is(VisitCauseGTDSEnumType.S), is(VisitCauseGTDSEnumType.T), is(VisitCauseGTDSEnumType.V), is(VisitCauseGTDSEnumType.X)
//            ));
//        }
//    }
//
//    @Test
//    public void testGrading() throws Exception {
//        TDGProfile cancerProfile = new TDGProfile("cancerProfile", null);
//        PatientGenerator patientGenerator = new PatientGenerator(cancerProfile);
//        TumourGenerator tumourGenerator = new TumourGenerator(cancerProfile);
//        PatientDataSetType patient = patientGenerator.generate(1).get(0);
//        GTDSTumourEIType tumour = tumourGenerator.generate(patient).get(0);
//        if (patient.getDiagnosis() == null)
//            Assert.assertNull(tumour);
//        else {
//            Assert.assertThat(tumour.getGrading(), anyOf(
//                    is(GradingEnum.Zero.getDefition()), is(GradingEnum.B.getDefition()), is(GradingEnum.H.getDefition()), is(GradingEnum.L.getDefition()), is(GradingEnum.Four.getDefition()),
//                    is(GradingEnum.One.getDefition()), is(GradingEnum.M.getDefition()), is(GradingEnum.Three.getDefition()), is(GradingEnum.Two.getDefition()), is(GradingEnum.U.getDefition()),
//                    is(GradingEnum.X.getDefition()), is(GradingEnum.T.getDefition())
//            ));
//        }
//    }
}
