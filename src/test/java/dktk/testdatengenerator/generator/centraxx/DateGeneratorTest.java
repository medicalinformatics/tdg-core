package dktk.testdatengenerator.generator.centraxx;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import de.kairos_med.DatePrecision;
import de.kairos_med.DateType;
import dktk.testdatengenerator.exceptions.DateGeneratorException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;

/**
 * Created by BrennerT on 20.06.2017.
 */
@Ignore
public class DateGeneratorTest {

    @Test
    public void testGenerateDate() throws Exception {

        DateGenerator dateGenerator = new DateGenerator();
        DateType actualDate = dateGenerator.generate();

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        Date currentDate = new Date();
        gregorianCalendar.setTime(currentDate);

        XMLGregorianCalendar testDate = actualDate.getDate();

        Assert.assertTrue("The generated Date shouldn't be before the 01-01-1900",
                testDate.compare(XMLGregorianCalendarImpl.parse("1900-01-01T00:00:00.000+01:00")) != DatatypeConstants.LESSER);
        Assert.assertTrue("The generated Date shouldn't be behind the current date",
                testDate.compare(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar)) != DatatypeConstants.GREATER);

        Assert.assertThat("The DateType should have one of the following four precisions",actualDate.getPrecision(), anyOf(is(DatePrecision.DAY),is(DatePrecision.MONTH),is(DatePrecision.EXACT),is(DatePrecision.YEAR)));

        switch(actualDate.getPrecision()){
            case EXACT: Assert.assertTrue("The milliseconds should be set", testDate.getMillisecond() != 00);
            break;
            case DAY: Assert.assertTrue("The hours and minutes and seconds shouldn't be set", testDate.getHour()==00 && testDate.getMinute()==00 && testDate.getSecond() == 00);
            break;
            case MONTH: Assert.assertTrue("The Day should not be set in the result DateType", testDate.getDay()==01);
            break;
            case YEAR: Assert.assertTrue("The Month and Day should not be set in the Result DateType", testDate.getDay()==01 && testDate.getMonth()==01);
            break;
            default:
                System.out.println("Nothing to do here");
                break;
        }

    }

    @Test
    public void testGenerateDateAfter() throws Exception {
        DateGenerator dateGenerator = new DateGenerator();

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        DateType testDate = dateGenerator.generate();
        gregorianCalendar.setTime(testDate.getDate().toGregorianCalendar().getTime());
        Assert.assertTrue("The generated Date should be greater than the other date",
                dateGenerator.generateDateAfter(testDate).getDate().compare(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar)) == DatatypeConstants.GREATER);
    }

    @Test(expected = DateGeneratorException.class)
    public void testGenerateDateAfterFailing() throws Exception {
        DateGenerator dateGenerator = new DateGenerator();

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        Date futureDate = new Date();
        gregorianCalendar.setTime(futureDate);
        gregorianCalendar.add(Calendar.YEAR, 1);
        DateType failingTest = new DateType();
        failingTest.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
        failingTest.setPrecision(DatePrecision.EXACT);

        dateGenerator.generateDateAfter(failingTest);

    }

    @Test
    public void testGenerateDateBefore() throws Exception {
        DateGenerator dateGenerator = new DateGenerator();

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(1900,00,01,00,00,00);
        gregorianCalendar.add(Calendar.YEAR, -1);
        Date failingDate = gregorianCalendar.getTime();
        gregorianCalendar.setTime(failingDate);

        DateType testDate = dateGenerator.generate();
        gregorianCalendar.setTime(testDate.getDate().toGregorianCalendar().getTime());
        Assert.assertTrue("The generated date should be lower than the other date",
                dateGenerator.generateDateBefore(testDate).getDate().compare(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar)) == DatatypeConstants.LESSER);
    }

    @Test(expected = DateGeneratorException.class)
    public void testGenerateDateBeforeFailing() throws Exception {
        DateGenerator dateGenerator = new DateGenerator();

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(1900,00,01,00,00,00);
        gregorianCalendar.add(Calendar.YEAR, -1);
        Date failingDate = gregorianCalendar.getTime();
        gregorianCalendar.setTime(failingDate);

        DateType failingTest = new DateType();
        failingTest.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
        failingTest.setPrecision(DatePrecision.EXACT);

        dateGenerator.generateDateBefore(failingTest);

    }

    @Test
    public void testGenerateDateBetween() throws Exception {
        DateGenerator dateGenerator = new DateGenerator();

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(1900,00,01,00,00,00);
        gregorianCalendar.add(Calendar.YEAR, -1);
        Date failingDate = gregorianCalendar.getTime();
        gregorianCalendar.setTime(failingDate);

        DateType currentDate = new DateType();
        gregorianCalendar.setTime(new Date());
        currentDate.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
        currentDate.setPrecision(DatePrecision.EXACT);

        gregorianCalendar.set(1900,00,02,00,00,00);
        DateType beforeDate = new DateType();
        beforeDate.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
        beforeDate.setPrecision(DatePrecision.EXACT);

        Assert.assertNotNull("The result should not be null then parameters are correct", dateGenerator.generateDateBetween(beforeDate, currentDate));
        Assert.assertTrue("The result date should be after first date",
                dateGenerator.generateDateBetween(beforeDate, currentDate).getDate().compare(beforeDate.getDate()) == DatatypeConstants.GREATER);
        Assert.assertTrue("The result date should be before second date",
                dateGenerator.generateDateBetween(beforeDate, currentDate).getDate().compare(currentDate.getDate()) == DatatypeConstants.LESSER);
    }

    @Test(expected = DateGeneratorException.class)
    public void testBeforeFailing() throws Exception {
        DateGenerator dateGenerator = new DateGenerator();

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(1900,00,01,00,00,00);
        gregorianCalendar.add(Calendar.YEAR, -1);
        Date failingDate = gregorianCalendar.getTime();
        gregorianCalendar.setTime(failingDate);

        DateType failingTest = new DateType();
        failingTest.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
        failingTest.setPrecision(DatePrecision.EXACT);

        DateType currentDate = new DateType();
        gregorianCalendar.setTime(new Date());
        currentDate.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
        currentDate.setPrecision(DatePrecision.EXACT);

        dateGenerator.generateDateBetween(failingTest, currentDate);
    }

    @Test(expected = DateGeneratorException.class)
    public void testFirstAfterSecondDate() throws Exception {
        DateGenerator dateGenerator = new DateGenerator();

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(1900,00,01,00,00,00);
        gregorianCalendar.add(Calendar.YEAR, -1);
        Date failingDate = gregorianCalendar.getTime();
        gregorianCalendar.setTime(failingDate);

        DateType currentDate = new DateType();
        gregorianCalendar.setTime(new Date());
        currentDate.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
        currentDate.setPrecision(DatePrecision.EXACT);

        DateType failingTest = new DateType();
        gregorianCalendar.set(1900,00,02,00,00,00);
        failingTest.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
        dateGenerator.generateDateBetween(currentDate, failingTest);
    }

    @Test(expected = DateGeneratorException.class)
    public void testAfterFailing() throws Exception {
        DateGenerator dateGenerator = new DateGenerator();

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(1900,00,01,00,00,00);
        gregorianCalendar.add(Calendar.YEAR, -1);
        Date failingDate = gregorianCalendar.getTime();
        gregorianCalendar.setTime(failingDate);


        DateType currentDate = new DateType();
        gregorianCalendar.setTime(new Date());
        currentDate.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
        currentDate.setPrecision(DatePrecision.EXACT);

        DateType failingTest = new DateType();
        failingTest.setPrecision(DatePrecision.EXACT);

        failingDate = new Date();
        gregorianCalendar.setTime(failingDate);
        gregorianCalendar.add(Calendar.YEAR, 1);
        failingTest.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
        dateGenerator.generateDateBetween(currentDate, failingTest);
    }
}
