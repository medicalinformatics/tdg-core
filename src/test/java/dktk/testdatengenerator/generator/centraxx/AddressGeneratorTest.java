package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.AddressType;
import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.exceptions.AddressGeneratorException;
import org.junit.*;


import static org.hamcrest.CoreMatchers.is;

/**
 * Created by brennert on 27.06.2017.
 */
@Ignore
public class AddressGeneratorTest {

    private DBConnector dbConnector;

    @Before
    public void setUp() throws Exception {
        dbConnector = new DBConnector();
        dbConnector.openConnection();
    }

    @After
    public void tearDown() throws Exception {
        dbConnector.closeConnection();
    }

    @Test
    public void testGetRandomElementFromList() throws Exception {
        AddressGenerator addressGenerator = new AddressGenerator(2, dbConnector);

        AddressType addressType = addressGenerator.takeRandomAddressFromList("DKTK-Testdatengenerator");
        Assert.assertNotNull("The DKTK-Testdatagenerator should have a Address in the list", addressType);
    }

    @Test(expected = AddressGeneratorException.class)
    public void testUnknownHospitalFromAddressList() throws Exception {
        AddressGenerator addressGenerator = new AddressGenerator(2, dbConnector);
        addressGenerator.takeRandomAddressFromList("This hospital cant exist");
    }

    @Test
    public void testConstructorWithHospitals() throws Exception {
        AddressGenerator addressGenerator = new AddressGenerator(2, new String[]{"DKTK-Testdatengenerator", "Hospital zum Heiligen Geist"}, dbConnector);
        Assert.assertNotNull("Should return an address for this", addressGenerator.takeRandomAddressFromList("DKTK-Testdatengenerator"));
    }

    @Test(expected = AddressGeneratorException.class)
    public void testWrongHospital() throws Exception {
        new AddressGenerator(2, new String[]{"This is no real hospital"}, dbConnector);
    }

    @Test(expected = AddressGeneratorException.class)
    public void testGetBirthplaceForHospitalFailing() throws Exception {
        new AddressGenerator(2, dbConnector).findBirthplaceForHospital("This is no real hospital");
    }

    @Test
    public void testGetBirthplaceForHospital() throws Exception {
        AddressGenerator addressGenerator = new AddressGenerator(2, dbConnector);
        Assert.assertNotNull(addressGenerator.findBirthplaceForHospital("DKTK-Testdatengenerator"));
        Assert.assertThat(addressGenerator.findBirthplaceForHospital("DKTK-Testdatengenerator"), is("HEIDELBERG"));
    }
}
