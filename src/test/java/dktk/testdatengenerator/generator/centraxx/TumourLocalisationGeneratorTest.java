package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.GTDSTumourLocalisationEIType;
import de.kairos_med.PatientDataSetType;
import de.kairos_med.SideGTDSEnumType;
import dktk.testdatengenerator.db.model.TDGProfile;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;

/**
 * Created by brennert on 25.07.2017.
 */
@Ignore
public class TumourLocalisationGeneratorTest {

//    private static PatientDataSetType patient;
//    private static GTDSTumourLocalisationEIType tumourLocalisation;
//
//    @BeforeClass
//    public static void setUp() {
//        TDGProfile tdgProfile = new TDGProfile("cancerProfile", null);
//        TumourGenerator tumourGenerator = new TumourGenerator(tdgProfile);
//        TumourLocalisationGenerator tumourLocalisationGenerator = new TumourLocalisationGenerator(tdgProfile);
//        patient = new PatientGenerator(tdgProfile).generate(1).get(0);
//        tumourLocalisation = tumourLocalisationGenerator.generate(tumourGenerator.generate(patient).get(0));
//    }
//
//    @Test
//    public void testSetLocalisationCode() throws Exception {
//        if (patient.getDiagnosis() != null) {
//            Assert.assertThat(tumourLocalisation.getLocalisationCode(), is(patient.getDiagnosis().get(0).getDiagnosisCode()));
//            Assert.assertNotNull(tumourLocalisation.getLocalisationText());
//        } else
//            Assert.assertNull(tumourLocalisation);
//    }
//
//    @Test
//    public void testSetComplexICDEntryRef() throws Exception {
//        if (patient.getDiagnosis() != null){
//            Assert.assertThat(tumourLocalisation.getComplexIcdEntryRef().getCode(), is(tumourLocalisation.getLocalisationCode()));
//            Assert.assertThat(tumourLocalisation.getComplexIcdEntryRef().getKind(), is("topology"));
//            Assert.assertThat(tumourLocalisation.getComplexIcdEntryRef().getIcdCatalogRef().getKind(), is("ICD-O"));
//            Assert.assertThat(tumourLocalisation.getComplexIcdEntryRef().getIcdCatalogRef().getVersion(), is("2014"));
//        }
//        else
//            Assert.assertNull(tumourLocalisation);
//    }
//
//    @Test
//    public void testGenerateRandomSide() throws Exception {
//        if (patient.getDiagnosis() != null) {
//            Assert.assertThat(tumourLocalisation.getSide(), anyOf(
//                    is(SideGTDSEnumType.B), is(SideGTDSEnumType.L), is(SideGTDSEnumType.M), is(SideGTDSEnumType.R),
//                    is(SideGTDSEnumType.S), is(SideGTDSEnumType.T), is(SideGTDSEnumType.X)));
//        } else
//            Assert.assertNull(tumourLocalisation);
//    }
}
