package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.*;
import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.db.model.TDGProfile;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.datatype.DatatypeConstants;
import java.util.Date;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;

/**
 * Created by brennert on 03.07.2017.
 */
@Ignore
public class DiagnosisGeneratorTest {

//    private DiagnosisGenerator diagnosisGenerator;
//    private PatientDataSetType patient;
//
//    @Before
//    public void setUp() {
//        TDGProfile tdgProperties = new TDGProfile("cancerProfile", null);
//        DBConnector dbConnector = new DBConnector();
//        diagnosisGenerator = new DiagnosisGenerator(tdgProperties, dbConnector);
//        MasterDataGenerator masterDataGenerator = new MasterDataGenerator(tdgProperties, dbConnector);
//        PatientMasterdataType masterdata = masterDataGenerator.generate();
//    }
//
//    @Test
//    public void testGenerateDate() throws Exception {
//        DiagnosisType diagnosis = diagnosisGenerator.generateDiagnosisAndAddToPatient(patient);
//        Assert.assertNotNull(diagnosis.getDate());
//        Assert.assertTrue(patient.getDateOfBirth().getDate().compare(diagnosis.getDate().getDate()) == DatatypeConstants.LESSER);
//        if (patient.getDateOfDeath() != null)
//            Assert.assertTrue(patient.getDateOfDeath().getDate().compare(diagnosis.getDate().getDate()) == DatatypeConstants.GREATER);
//        else
//            Assert.assertTrue(new Date().compareTo(diagnosis.getDate().getDate().toGregorianCalendar().getTime()) == DatatypeConstants.GREATER);
//    }
//
//    @Test
//    public void testGenerateText() throws Exception {
//        DiagnosisType diagnosis = diagnosisGenerator.generate(patient, EntitySourceEnumType.XMLIMPORT);
//        Assert.assertNotNull(diagnosis.getText());
//    }
//
//    @Test
//    public void testGenerateICDEntryRef() throws Exception {
//        DiagnosisType diagnosis = diagnosisGenerator.generate(patient, EntitySourceEnumType.XMLIMPORT);
//        Assert.assertTrue(diagnosis.getIcdEntryRef().getCode().matches("[CD]\\d\\d\\.?\\d?\\d?[\\+\\*]?"));
//        Assert.assertEquals("code", diagnosis.getIcdEntryRef().getKind());
//        Assert.assertEquals("ICD-General", diagnosis.getIcdEntryRef().getIcdCatalogRef().getKind());
//        Assert.assertEquals("2017", diagnosis.getIcdEntryRef().getIcdCatalogRef().getVersion());
//    }
//
//    @Test
//    public void testGenerateEarlierTumors() throws Exception {
//        DiagnosisType diagnosis = diagnosisGenerator.generate(patient, EntitySourceEnumType.XMLIMPORT);
//        if (diagnosis != null)
//            Assert.assertThat("Must match any of the Enumvalues", patient.getEarlierTumours(), is(anyOf(is(YesNoXGTDSEnumType.J), is(YesNoXGTDSEnumType.N), is(YesNoXGTDSEnumType.X))));
//        else
//            Assert.assertNull("The Earlier Tumour Field should be null then no diagnosis exists", patient.getEarlierTumours());
//    }

}
