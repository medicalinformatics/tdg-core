package dktk.testdatengenerator.generator.centraxx;

import de.kairos_med.EntitySourceEnumType;
import de.kairos_med.EpisodeType;
import de.kairos_med.PatientDataSetType;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.enums.OrganisationUnitsEnum;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 * Created by brennert on 11.07.2017.
 */
@Ignore
public class EpisodeGeneratorTest {

//    private PatientDataSetType patient;
//    private TDGProfile tdgProfile;
//
//    @Before
//    public void setUp() {
//        tdgProfile = new TDGProfile("cancerProfile", null);
//        patient = new PatientDataSetType();
//        MasterDataGenerator masterDataGenerator = new MasterDataGenerator(tdgProfile);
//        DiagnosisGenerator diagnosisGenerator = new DiagnosisGenerator(tdgProfile);
//        patient.setMasterdata(masterDataGenerator.generate());
//        patient.getDiagnosis().add(diagnosisGenerator.generate(patient.getMasterdata(), EntitySourceEnumType.XMLIMPORT));
//    }
//
//    @Test
//    public void testSetAmountToGenerate() throws Exception {
//        tdgProfile.getProbability("episodeMin").setProbability(2);
//        tdgProfile.getProbability("episodeMax").setProbability(5);
//        EpisodeGenerator episodeGenerator = new EpisodeGenerator(tdgProfile);
//        ArrayList<EpisodeType> episodes = episodeGenerator.generate(patient);
//        Assert.assertTrue(episodes.size() >= 2);
//        Assert.assertTrue(episodes.size() <= 5);
//    }
//
//    @Test
//    public void testSetEpisodeLength() throws Exception {
//        tdgProfile.getProbability("episodeMinLength").setProbability(1);
//        tdgProfile.getProbability("episodeMaxLength").setProbability(2);
//        EpisodeGenerator episodeGenerator = new EpisodeGenerator(tdgProfile);
//        ArrayList<EpisodeType> episodes = episodeGenerator.generate(patient);
//        int dayDifference = episodes.get(0).getValidUntil().getDate().getDay() - episodes.get(0).getValidFrom().getDate().getDay();
//        Assert.assertTrue(dayDifference >= tdgProfile.getProbability("episodeMinLength").getProbability());
//        Assert.assertTrue(dayDifference <= tdgProfile.getProbability("episodeMaxLength").getProbability());
//    }
//
//    @Test
//    public void testDateBetweenBirthAndDeath() throws Exception {
//        EpisodeGenerator episodeGenerator = new EpisodeGenerator(tdgProfile);
//        ArrayList<EpisodeType> episodes = episodeGenerator.generate(patient);
//        XMLGregorianCalendar validFrom = episodes.get(0).getValidFrom().getDate();
//        Assert.assertTrue(validFrom.compare(DatatypeFactory.newInstance().newXMLGregorianCalendar(patient.getMasterdata().getDateOfBirth().getDate().toGregorianCalendar())) == DatatypeConstants.GREATER);
//        XMLGregorianCalendar validUntil = episodes.get(0).getValidUntil().getDate();
//        if(patient.getMasterdata().getDateOfDeath() != null)
//            Assert.assertTrue(validUntil.compare(DatatypeFactory.newInstance().newXMLGregorianCalendar(patient.getMasterdata().getDateOfDeath().getDate().toGregorianCalendar())) == DatatypeConstants.LESSER);
//        else{
//            try{
//                Assert.assertTrue(validUntil.compare(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar())) == DatatypeConstants.LESSER);
//            }catch (DatatypeConfigurationException dce){
//                System.out.println("Couldn't convertAndAdd Gregorian Calendar to XMLGregorianCalendar");
//            }
//
//        }
//    }
//
//    @Test
//    public void testSetEpisodeRef() throws Exception {
//        EpisodeGenerator episodeGenerator = new EpisodeGenerator(tdgProfile);
//        ArrayList<EpisodeType> episodes = episodeGenerator.generate(patient);
//        Assert.assertEquals("The first episode should be generated for the diagnosis, every other could have a diagnosis but dont needs to have one", episodes.get(0).getEpisodeRef(), patient.getDiagnosis().get(0).getEpisodeRef());
//    }
//
//    @Test
//    public void testSetHabitationRef() throws Exception {
//        EpisodeGenerator episodeGenerator = new EpisodeGenerator(tdgProfile);
//        ArrayList<EpisodeType> episodes = episodeGenerator.generate(patient);
//        Assert.assertNotNull(episodes.get(0).getHabitationRef());
//    }
}
