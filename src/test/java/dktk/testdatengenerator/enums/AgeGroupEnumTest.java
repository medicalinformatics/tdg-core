package dktk.testdatengenerator.enums;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by brennert on 03.07.2017.
 */
@Ignore
public class AgeGroupEnumTest {

    @Test
    public void testValueMethod() throws Exception {
        Assert.assertThat(AgeGroupEnum.Between20And29.getMin(), is(20));
        Assert.assertThat(AgeGroupEnum.Between10And19.getMax(),is(19));
    }
}