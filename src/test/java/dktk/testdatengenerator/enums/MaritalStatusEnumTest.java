package dktk.testdatengenerator.enums;

import junit.framework.Assert;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Created by brennert on 29.06.2017.
 */
@Ignore
public class MaritalStatusEnumTest {

    @Test
    public void testValueMethod() throws Exception {
        Assert.assertEquals("The value method should convertAndAdd all Statuses divided by underscore", "Domestic Partner", MaritalStatusEnum.DomesticPartner.value());
        //Assert.assertEquals("The value method should convertAndAdd all Statuses divided by underscore", "Legally Separated", MaritalStatusEnum.LegallySeparated.value());
        Assert.assertEquals("The value method should convertAndAdd all Statuses divided by underscore", "Never Married", MaritalStatusEnum.NeverMarried.value());

        Assert.assertEquals("Other Values should be uneffected", "Married", MaritalStatusEnum.Married.value());
    }
}
