package dktk.testdatengenerator.enums;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by brennert on 29.06.2017.
 */
@Ignore
public class TitleEnumTest {
    @Test
    public void value() throws Exception {
        Assert.assertEquals("Dr. med.", TitleEnum.DrMed.value());
        Assert.assertEquals("PD Dr. med. Dr. med.", TitleEnum.PDDrMedDrMed.value());
        Assert.assertEquals("Prof.", TitleEnum.Prof.value());
        Assert.assertEquals("Prof. Dr. med.", TitleEnum.ProfDrMed.value());
    }

}