package dktk.testdatengenerator.db.model;

import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.enums.AgeGroupEnum;
import dktk.testdatengenerator.exceptions.ProbabilityException;
import org.junit.*;


import java.sql.ResultSet;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;

/**
 * Created by brennert on 13.07.2017.
 */
@Ignore
public class TDGProfileTest {

    private DBConnector dbConnector;

    @Before
    public void setUp() throws Exception {
        dbConnector = new DBConnector();
        dbConnector.openConnection();
    }

    @After
    public void tearDown() throws Exception {
        dbConnector.closeConnection();
    }


    @Test
    public void testStandardConstructor() throws Exception {
        TDGProfile tdgProfile = new TDGProfile(dbConnector);
        Assert.assertThat(tdgProfile.getProbability("pAgeUnder10").getProbability(), is(0.085));
        Assert.assertThat(tdgProfile.getProbability("pMALETumourBetween30And39").getProbability(), is(0.001));
    }

    @Test
    public void testProfileConstructor() throws Exception {
        TDGProfile tdgProfile = new TDGProfile("cancerProfile", null, dbConnector);
        Assert.assertThat(tdgProfile.getProbability("pAgeUnder10").getProbability(), is(0.001));
        Assert.assertThat(tdgProfile.getProbability("pMALETumourBetween30And39").getProbability(), is(0.01));
        Assert.assertThat(tdgProfile.getProbability("pUNKNOWN").getProbability(), is(0.007));
    }

    @Test
    public void testGetProperty() throws Exception {
        TDGProfile tdgProfile = new TDGProfile(dbConnector);
        double probabiltyWoman = tdgProfile.getProbability("pFEMALE").getProbability();
        Assert.assertNotNull("The result should not be null", probabiltyWoman);
    }

    @Test
    public void testSetProperty() throws Exception {
        TDGProfile tdgProfile = new TDGProfile("testProfile", null, dbConnector);
        tdgProfile.setProbability("pAgeUnder10", 0.001);
        Assert.assertThat(tdgProfile.getProbability("pAgeUnder10").getProbability(), is(0.001));
    }

    @Test
    public void testSafeProfile() throws Exception {
        TDGProfile tdgProfile = new TDGProfile(null, null);
        setProbabilitiesToOnlyWoman(tdgProfile);
        tdgProfile.safeProfile("safeTestProfile", "This profile is here for the safe and update Tests");
        DBConnector dbConnector = new DBConnector();
        ResultSet resID = dbConnector.sendQuery("SELECT * FROM profile WHERE profile_name='safeTestProfile'");
        Assert.assertTrue(resID.next());
        Assert.assertEquals("safeTestProfile", resID.getString("profile_name"));
        Assert.assertEquals("This profile is here for the safe and update Tests", resID.getString("profile_description"));
        int p_id = resID.getInt("p_id");
        Assert.assertNotNull(p_id);
        ResultSet values = dbConnector.sendQuery("SELECT * FROM profile_properties WHERE fk_p_id=" + p_id + ";");
        while(values.next()){
            Assert.assertThat(values.getDouble("profile_value"), anyOf(is(0.0), is(1.0)));
        }
        dbConnector.closeConnection();
    }

    @Test
    public void testUpdateProfile() throws Exception {
        TDGProfile tdgProfile = new TDGProfile("safeTestProfile", null, dbConnector);
        setProbabilitiesToFiftyFiftyWomanMan(tdgProfile);
        tdgProfile.safeProfile("safeTestProfile", null);
        DBConnector dbConnector = new DBConnector();
        ResultSet resultSet = dbConnector.sendQuery("SELECT * FROM profile WHERE profile_name='safeTestProfile'");
        Assert.assertTrue(resultSet.next());
        ResultSet values = dbConnector.sendQuery("SELECT * FROM profile_properties WHERE fk_p_id=" + resultSet.getInt("p_id") + ";");
        while(values.next()){
            Assert.assertThat(values.getDouble("profile_value"), anyOf(is(0.0), is(0.5)));
        }
        Assert.assertFalse("It should only exist one entry for safeTestProfile", resultSet.next());
        dbConnector.closeConnection();
    }

    @Test
    public void testRequestSumProperties() throws Exception {
        TDGProfile tdgProfile = new TDGProfile(dbConnector);
        double sumProbabilityTumourUntilAge = tdgProfile.querySum(AgeGroupEnum.class, AgeGroupEnum.Between40And49, "pAge", "");
        Assert.assertFalse("the sum should not be 0.0", sumProbabilityTumourUntilAge == 0.0);
        Assert.assertTrue("the sum should never be greater than 1", sumProbabilityTumourUntilAge <= 1.0);

        double sumProbabilityTumourAllAges = tdgProfile.querySum(AgeGroupEnum.class, "pMALETumour", "");
        Assert.assertFalse("the sum should not be 0.0", sumProbabilityTumourAllAges == 0.0);
        Assert.assertTrue("the sum should never be greater than 1", sumProbabilityTumourAllAges <= 1.0);
    }

    private void setProbabilitiesToOnlyWoman(TDGProfile tdgProfile) throws ProbabilityException{
        tdgProfile.setProbability("pFEMALE", 1.0);
        tdgProfile.setProbability("pMALE", 0);
        tdgProfile.setProbability("pHERMAPHRODIT", 0);
        tdgProfile.setProbability("pUNKNOWN", 0);
        tdgProfile.setProbability("pOTHER", 0);
        tdgProfile.setProbability("pUNDEFINED", 0);
    }

    private void setProbabilitiesToFiftyFiftyWomanMan(TDGProfile tdgProfile) throws ProbabilityException{
        tdgProfile.setProbability("pFEMALE", 0.5);
        tdgProfile.setProbability("pMALE", 0.5);
        tdgProfile.setProbability("pOTHER", 0);
        tdgProfile.setProbability("pHERMAPHRODIT", 0);
        tdgProfile.setProbability("pUNDEFINED", 0);
        tdgProfile.setProbability("pUNKNOWN", 0);
    }

}
