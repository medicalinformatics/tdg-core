package dktk.testdatengenerator.db.model;

import de.kairos_med.GenderTypeEnum;
import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.exceptions.ProbabilityException;
import org.junit.*;



import static org.hamcrest.CoreMatchers.is;

/**
 * Created by brennert on 13.07.2017.
 */
@Ignore
public class ProbabilityTest {

    private DBConnector dbConnector;

    @Before
    public void setUp() throws Exception {
        dbConnector = new DBConnector();
        dbConnector.openConnection();
    }

    @After
    public void tearDown() throws Exception {
        dbConnector.closeConnection();
    }

    @Test
    public void testGetProbability() throws Exception {
        Probability probFemale = new Probability("p", GenderTypeEnum.FEMALE, -1, dbConnector);
        Probability episodeMin = new Probability("episodeMin", -1, dbConnector);

        Assert.assertThat(probFemale.getProbability(), is(0.5));
        Assert.assertThat(episodeMin.getProbability(), is(2.0));
    }

    @Test
    public void testSetProbabilityInConstructor() throws Exception {
        Probability p = new Probability("p", GenderTypeEnum.MALE, 0.8, dbConnector);
        Assert.assertThat(p.getProbability(),is(0.8));

    }

    @Test(expected = ProbabilityException.class)
    public void testFindNotExistingProperty() throws Exception {
        new Probability("this property does not exist", -1, dbConnector);
    }

    @Test
    public void testSetProbability() throws Exception {
        Probability pMale = new Probability("p", GenderTypeEnum.MALE, -1, dbConnector);
        pMale.setProbability(0.3);
        Assert.assertThat(pMale.getProbability(), is(0.3));
    }

    @Test(expected = ProbabilityException.class)
    public void testSetProbabilityOver1() throws Exception {
        new Probability("p", GenderTypeEnum.MALE, -1, dbConnector).setProbability(1.01);
    }

    @Test(expected = ProbabilityException.class)
    public void testSetProbabilityUnder0()throws Exception {
        new Probability("p", GenderTypeEnum.MALE, -1, dbConnector).setProbability(-0.01);
    }
}
