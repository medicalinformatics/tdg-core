package dktk.testdatengenerator.db.model;

import dktk.testdatengenerator.db.DBConnector;
import org.junit.*;


import java.sql.ResultSet;

import static org.hamcrest.CoreMatchers.is;

/**
 * Created by brennert on 13.07.2017.
 */
@Ignore
public class CRUDProfileImplTest {

    private DBConnector dbConnector;

    @Before
    public void setUp() throws Exception {
        dbConnector = new DBConnector();
        dbConnector.openConnection();
    }

    @After
    public void tearDown() throws Exception {
        dbConnector.closeConnection();
    }


    @Test
    public void testConstructor() throws Exception {

        CRUDProfileImpl crudProfile = new CRUDProfileImpl("testProfile", "This is a test profile that you should describe here", dbConnector);
        ResultSet resultSet = dbConnector.sendQuery("SELECT * FROM profile WHERE p_id=3;");
        Assert.assertThat(resultSet.next(), is(true));
        Assert.assertThat(resultSet.getString("profile_name"), is("testProfile"));
        Assert.assertThat(resultSet.getString("profile_description"), is("This is a test profile that you should describe here"));

    }

    @Test
    public void testCreate() throws Exception {

        CRUDProfileImpl crudProfile= new CRUDProfileImpl("testProfile", "This is a test profile that you should describe here", dbConnector);
        if(crudProfile.read("pFEMALE") == -1)
            crudProfile.create("pFEMALE", 0.6);
        else
            crudProfile.update("pFEMALE", 0.6);

        ResultSet resultSet = dbConnector.sendQuery("SELECT * FROM profile_properties WHERE fk_p_id=" + crudProfile.getProfileID() + " AND property_name='pFEMALE';");
        Assert.assertThat(resultSet.next(), is(true));
        Assert.assertThat(resultSet.getString("property_name"), is("pFEMALE"));
        Assert.assertThat(resultSet.getDouble("profile_value"), is(0.6));
        dbConnector.closeConnection();
    }

    @Test
    public void testRead() throws Exception {
        CRUDProfileImpl crudProfile = new CRUDProfileImpl("testProfile", "This is a test profile that you should describe here", dbConnector);
        if(crudProfile.read("pMALE") == -1)
            crudProfile.create("pMALE", 0.6);
        else
            crudProfile.update("pMALE", 0.6);
        double pFEMALE = crudProfile.read("pMALE");
        Assert.assertThat(pFEMALE, is(0.6));

    }

    @Test
    public void testReadNotExistingVariable() throws Exception {
        CRUDProfileImpl crudProfile = new CRUDProfileImpl("testProfile", "This is a test profile that you should describe here", dbConnector);
        Assert.assertThat(crudProfile.read("This property should not exist"), is(-1.0));

    }

    @Test
    public void testUpdate() throws Exception {
        CRUDProfileImpl crudProfile = new CRUDProfileImpl("testProfile", "This is a test profile that you should describe here", dbConnector);
        if(crudProfile.read("pUNKNOWN") == -1)
            crudProfile.create("pUNKNOWN", 0.2);
        else
            crudProfile.update("pUNKNOWN", 0.2);
        crudProfile.update("pUNKNOWN", 0.4);
        Assert.assertThat(crudProfile.read("pUNKNOWN"), is(0.4));

    }

    @Test
    public void testDelete() throws Exception {
        CRUDProfileImpl crudProfile = new CRUDProfileImpl("testProfile", "This is a test profile that you should describe here", dbConnector);
        crudProfile.create("pOTHER", 0.5);
        crudProfile.delete("pOTHER");
        Assert.assertThat(crudProfile.read("pOTHER"), is(-1.0));

    }
}
