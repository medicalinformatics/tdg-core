package dktk.testdatengenerator.db;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.postgresql.util.PSQLException;

import java.sql.Connection;
import java.sql.ResultSet;

/**
 * Created by brennert on 22.06.2017.
 */
@Ignore
public class DBConnectionTest {

    @Test
    public void testConnect() throws Exception {
        DBConnector dbConnector = new DBConnector();
        Connection connection = dbConnector.getConnection();
        Assert.assertNotNull("The connection should not be null", connection);
        dbConnector.closeConnection();
    }

    @Test
    public void testSendQuery() throws Exception {
        DBConnector dbConnector = new DBConnector();
        ResultSet queryResult = dbConnector.sendQuery("SELECT * FROM namelist");
        Assert.assertNotNull("The Query result should not be null", queryResult);
        dbConnector.closeConnection();
    }

    @Test(expected = PSQLException.class)
    public void testSendFailingQuery() throws Exception{
        DBConnector dbConnector = new DBConnector();
        dbConnector.sendQuery("SELECT * FROM thisTableShouldNotExist");
        dbConnector.closeConnection();
    }

    @Test
    public void testCloseConnection() throws Exception {
        DBConnector dbConnector = new DBConnector();
        dbConnector.closeConnection();
        Assert.assertTrue("The connection should be closed", dbConnector.getConnection().isClosed());
    }
}
