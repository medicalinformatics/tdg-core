package dktk.testdatengenerator.db;

import de.kairos_med.GenderTypeEnum;
import dktk.testdatengenerator.db.model.ICDCode;
import dktk.testdatengenerator.enums.ICDCodeGroups;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;

/**
 * Created by brennert on 06.07.2017.
 */
@Ignore
public class ICDCodeQueriesTest {

    private ICDCodeQueries icdCodeQueries;

    @Before
    public void setUp() {
        icdCodeQueries = new ICDCodeQueries(new DBConnector());
    }

    @Test
    public void testGetICDCodeValues() throws Exception {
        ArrayList<ICDCode> icdCode = icdCodeQueries.getICDCodeGroupValues(ICDCodeGroups.C16);
        Assert.assertThat("The icd code should have the code C16.0", icdCode.get(0).getCode(), is("C16.0"));
        Assert.assertThat(icdCode.get(0).getDescription(), is("Adenokarzinom der Kardia"));

        ArrayList<ICDCode> icdCodeBetween = icdCodeQueries.getICDCodeGroupValues(ICDCodeGroups.CBetween18And21);
        Assert.assertThat("The icd code should have the code C18.0", icdCodeBetween.get(0).getCode(), is("C18.0"));
        Assert.assertThat(icdCodeBetween.get(0).getDescription(), is("Adenokarzinom des Zäkums")); // this should change in future
    }

    @Test
    public void testGetICDCodeProbability() throws Exception {
        double probabilityMale = icdCodeQueries.getICDCodeProbability(GenderTypeEnum.MALE, ICDCodeGroups.C16);
        Assert.assertThat(probabilityMale, is(0.036));

        double probabilityFemale = icdCodeQueries.getICDCodeProbability(GenderTypeEnum.FEMALE, ICDCodeGroups.C16);
        Assert.assertThat(probabilityFemale, is(0.029));
    }

    @Test
    public void testGetProbabilitySumICDCodes() throws Exception {
        double probabilitySum = icdCodeQueries.getProbabilitySumICDCodes(GenderTypeEnum.FEMALE);
        Assert.assertTrue(probabilitySum <= 1);
        Assert.assertTrue(probabilitySum >= 0);
    }

    @Test
    public void getMissingICDCodesC() throws Exception {
        ArrayList<ICDCode> missingCodes = icdCodeQueries.getMissingICDCodes();
        int j = 0;
        for(ICDCode code : missingCodes){
            Assert.assertTrue(code.getCode().matches("[CD]\\d\\d\\.?\\d?\\d?[\\+\\*]?"));
            j++;
        }
    }
}
