package dktk.testdatengenerator.db;

import de.kairos_med.GenderTypeEnum;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;

/**
 * Created by brennert on 22.06.2017.
 */
@Ignore
public class PatientQueriesTest {

    private DBConnector dbConnector = new DBConnector();

    @Test
    public void testQueryRandomFirstName() throws Exception {
        PatientQueries patientQueries = new PatientQueries(dbConnector);
        DBConnector dbConnector = new DBConnector();

        String firstnameMale = patientQueries.queryRandomFirstName(GenderTypeEnum.MALE);
        Assert.assertTrue("The firstname should exist in namelist table for the gender 'MALE'",
                dbConnector.sendQuery("SELECT * FROM namelist WHERE firstname='"+firstnameMale+"' AND firstname_gender='MALE';").next());

        String firsnameFemale = patientQueries.queryRandomFirstName(GenderTypeEnum.FEMALE);
        Assert.assertTrue("The firstname should exist in namelist table for the gender 'FEMALE'",
                dbConnector.sendQuery("SELECT * FROM namelist WHERE firstname='"+firsnameFemale+"' AND firstname_gender='FEMALE';").next());
        dbConnector.closeConnection();
    }

    @Test
    public void testQueryRandomLastName() throws Exception {
        PatientQueries patientQueries = new PatientQueries(dbConnector);
        DBConnector dbConnector = new DBConnector();

        String lastName = patientQueries.queryRandomLastName();
        Assert.assertTrue("The lastname should exist in namelist table for the gender 'null'",
                dbConnector.sendQuery("SELECT * FROM namelist WHERE lastname='"+lastName+"' AND firstname_gender IS NULL;").next());
        Assert.assertNotNull("The lastname should not be null.", lastName);
        dbConnector.closeConnection();
    }

    @Test
    public void testQueryRandomMariedLastName() throws Exception {
        PatientQueries patientQueries = new PatientQueries(dbConnector);
        DBConnector dbConnector = new DBConnector();

        String mariedLastName = patientQueries.queryRandomLastNameMarried();
        Assert.assertTrue("The lastname should be splitted by '-'",
                mariedLastName.matches("[A-Za-z]+\\-[A-Za-z]+"));
        String[] lastNames = mariedLastName.split("-");
        Assert.assertTrue("The lastname should consist of an maximum of two last names", lastNames.length == 2);
        Assert.assertTrue("The FirstPart of lastname should exist in lastnames table",
                dbConnector.sendQuery("SELECT * FROM namelist WHERE lastname='"+lastNames[0]+"' AND firstname_gender IS NULL;").next());
        Assert.assertTrue("The SecondPart of lastname should exist in lastnames table",
                dbConnector.sendQuery("SELECT * FROM namelist WHERE lastname='"+lastNames[1]+"' AND firstname_gender IS NULL;").next());
        dbConnector.closeConnection();
    }

    @Test
    public void testQueryRandomEthnicity() throws Exception {
        PatientQueries patientQueries = new PatientQueries(dbConnector);
        Assert.assertThat("The ethnicity should be any to Germany related ethnicity",patientQueries.queryRandomEthnicity("DEU"),
                anyOf(is("Germans"), is("Greeks"), is("Italians"), is("Serbs"), is("Turks"), is("Spaniards"), is("Russians"), is("Poles")));
    }
}
