package dktk.testdatengenerator.xml;

import de.kairos_med.CentraXXDataExchange;
import dktk.testdatengenerator.customelements.Patient;
import dktk.testdatengenerator.db.DBConnector;
import dktk.testdatengenerator.db.model.TDGProfile;
import dktk.testdatengenerator.generator.centraxx.PatientGenerator;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;

/**
 * Created by brennert on 19.06.2017.
 */
@Ignore
public class XMLFileWriterFactoryTest {


    @Ignore
    @Test
    public void testCreatePatientsAndWriteInXML() {

        try {

            DBConnector dbConnector = new DBConnector();
            dbConnector.openConnection();
            TDGProfile tdgProfile = new TDGProfile(dbConnector);

            PatientGenerator patientGenerator = new PatientGenerator(tdgProfile, dbConnector);
            ArrayList<Patient> patientList = patientGenerator.generate(5);

            XMLFileWriterFactory xmlFileWriterFactory = new XMLFileWriterFactory(50);
            xmlFileWriterFactory.setSendFilesToCentraXX(false);
            xmlFileWriterFactory.setDeleteFilesAfterProcess(false);

            xmlFileWriterFactory.add(patientList);

            dbConnector.closeConnection();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Ignore //TODO: create new test for splitting the files
    @Test
    public void testProcessCentraXXData() throws Exception {
        // 1. Get Data from centraxx (1 Patient from CentraXX the Others are self created)
        ClassLoader classLoader = getClass().getClassLoader();
        File data = new File(classLoader.getResource("xml/factory/testWriteMultiplePatients.xml").getFile());
        // 2. unmarshal it here
        Unmarshaller unmarshaller = JAXBContext.newInstance(CentraXXDataExchange.class).createUnmarshaller();
        CentraXXDataExchange centraXXData = (CentraXXDataExchange) unmarshaller.unmarshal(data);
        // 3. Give the patient data sets to [XMLFileWriterFactory]
        XMLFileWriterFactory xmlFileWriterFactory = new XMLFileWriterFactory(5);
        xmlFileWriterFactory.add((ArrayList) centraXXData.getEffectData().getPatientDataSet());
        // 4. check if the output file is a splitted version of the input version
        File expected1 = new File(classLoader.getResource("xml/factory/expectedWriteMultiplePatients.xml").getFile());
        File expected2 = new File(classLoader.getResource("xml/factory/expectedWriteMultiplePatients2.xml").getFile());
        File expected3 = new File(classLoader.getResource("xml/factory/expectedWriteMultiplePatients3.xml").getFile());

        File actual1 = new File("C://Users/BrennerT/Desktop/XMLFileWriterOutput/patients1.xml");
        File actual2 = new File("C://Users/BrennerT/Desktop/XMLFileWriterOutput/patients2.xml");
        File actual3 = new File("C://Users/BrennerT/Desktop/XMLFileWriterOutput/patients3.xml");

        Assert.assertEquals("Expect that the the following file exists:", true, actual1.exists());
        Assert.assertEquals("Expect that the the following file exists:", true, actual2.exists());
        Assert.assertEquals("Expect that the the following file exists:", true, actual3.exists());

        compareFiles(actual1, expected1);
        compareFiles(actual2, expected2);
        compareFiles(actual3, expected3);
    }


    // ----------- Helping Methods ----------- //
    private void initializeMock() {
        MockitoAnnotations.initMocks(this);
    }

    private void compareFiles(File expected, File actual) {
        try {
            BufferedReader brExpected = new BufferedReader(new FileReader(expected));
            BufferedReader brActual = new BufferedReader(new FileReader(actual));
            String lineActual = brActual.readLine();
            while (lineActual != null) {
                String lineExpected = brExpected.readLine();
                Assert.assertEquals("This two lines should be equal: ", lineExpected.trim(), lineActual.trim());
                lineActual = brActual.readLine();
            }
            brExpected.close();
            brActual.close();
        } catch (FileNotFoundException fnfe) {
            System.out.println("Couldn't find File");
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            System.out.println("Couldn't read from File");
            ioe.printStackTrace();
        }
    }
}
