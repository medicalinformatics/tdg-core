package dktk.testdatengenerator.xml;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import de.kairos_med.*;
import dktk.testdatengenerator.customelements.Patient;
import dktk.testdatengenerator.generator.centraxx.CentraXXGenerator;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;

/**
 * Created by brennert on 02.06.2017.
 */
@Ignore
public class XMLFileWriterTest {

    private static JAXBContext jaxbContext;
    private CentraXXGenerator centraXXGenerator = new CentraXXGenerator();


    private String testFile1 = "C:\\Users\\juarez\\Desktop\\testResult1.xml";
    private String testFile2 = "C:\\Users\\juarez\\Desktop\\testResult2.xml";

    @BeforeClass
    public static void setUp() {
        //initialize JAXBContext
        try {
            jaxbContext = JAXBContext.newInstance(CentraXXDataExchange.class);
        } catch (JAXBException jaxbe) {
            System.out.println("JAXBContext couldn't be created");
            jaxbe.printStackTrace();
        }
    }

    @Test
    public void testConstructor() throws Exception {

        File testResultFile = new File("C:\\Users\\juarez\\Desktop\\testResult.xml");
        ArrayList<Patient> patientDataSetTypes = new ArrayList<Patient>();

        Marshaller marshaller = jaxbContext.createMarshaller();

        CentraXXDataExchange centraXXDataExchange = centraXXGenerator.generate(patientDataSetTypes);
        XMLFileWriter xmlFileWriter1 = new XMLFileWriter(marshaller, testResultFile, centraXXDataExchange);

        Assert.assertEquals("testcase 1", testResultFile, xmlFileWriter1.getFile());
        Assert.assertEquals("testcase 1", patientDataSetTypes, xmlFileWriter1.getPatients());

        PatientDataSetType patientDataSetTypeMock = Mockito.mock(PatientDataSetType.class);

        patientDataSetTypes.add(new Patient (patientDataSetTypeMock));

        CentraXXDataExchange centraXXDataExchange2 = centraXXGenerator.generate(patientDataSetTypes);
        XMLFileWriter xmlFileWriter2 = new XMLFileWriter(marshaller, testResultFile, centraXXDataExchange2);

        Assert.assertEquals("testcase 2", testResultFile, xmlFileWriter2.getFile());
        Assert.assertEquals("testcase 2", patientDataSetTypes, xmlFileWriter2.getPatients());

        XMLFileWriter xmlFileWriter3 = new XMLFileWriter(marshaller, null, null);

        Assert.assertEquals("If no file is send to the List it should create an own file", testResultFile, xmlFileWriter3.getFile());
        Assert.assertNotNull("The created PatientsList should not be null", xmlFileWriter3.getPatients());
        Assert.assertTrue("The created PatientsList should be empty", xmlFileWriter3.getPatients().isEmpty());
    }

    @Test
    public void testWriteEmptyPatientDataSet() throws Exception {

        File testResultFile = new File(testFile1);
        ArrayList<Patient> patients = new ArrayList<Patient>();

        CentraXXDataExchange centraXXDataExchange = centraXXGenerator.generate(patients);
        Marshaller marshaller = jaxbContext.createMarshaller();
        XMLFileWriter xmlFileWriter = new XMLFileWriter(marshaller, testResultFile, centraXXDataExchange);
        xmlFileWriter.writeToFile();

        ClassLoader classLoader = getClass().getClassLoader();
        File testResultExpected = new File(classLoader.getResource("xml/testWriteEmptyPatientDataSet.xml").getFile());

        compareFiles(testResultExpected, testResultFile);
    }

    @Test
    public void testWritePatientMasterData() throws Exception {
        //first only test writing one patient
        File testResultFile = new File(testFile2);
        ArrayList<Patient> patientDataSetTypes = new ArrayList<Patient>();

        PatientMasterdataType patientMasterdata1 = new PatientMasterdataType();
        patientMasterdata1.setFirstName("Karl");
        patientMasterdata1.setLastName("Gustav");

        DateType birtdayDate = new DateType();
        birtdayDate.setDate(XMLGregorianCalendarImpl.parse("1899-01-01T00:00:00.000+01:00"));
        birtdayDate.setPrecision(DatePrecision.DAY);
        patientMasterdata1.setDateOfBirth(birtdayDate);

        DateType deathDate = new DateType();
        deathDate.setDate(XMLGregorianCalendarImpl.parse("1921-01-01T00:00:00.000+01:00"));
        deathDate.setPrecision(DatePrecision.DAY);
        patientMasterdata1.setDateOfDeath(deathDate);
        PatientDataSetType patient1 = new PatientDataSetType();
        CatalogueDataRefType organisationCatalogueRef = new CatalogueDataRefType();
        organisationCatalogueRef.setValue("DKTK-Testdatengenerator");
        patient1.getOrganisationUnitRefs().add(organisationCatalogueRef);
        patient1.setMasterdata(patientMasterdata1);

        patientDataSetTypes.add(new Patient(patient1));

        CentraXXDataExchange centraXXDataExchange = centraXXGenerator.generate(patientDataSetTypes);

        Marshaller marshaller = jaxbContext.createMarshaller();

        XMLFileWriter xmlFileWriter = new XMLFileWriter(marshaller, testResultFile, centraXXDataExchange);
        xmlFileWriter.writeToFile();

        ClassLoader classLoader = getClass().getClassLoader();
        File testResultExpected = new File(classLoader.getResource("xml/testWritePatientMasterData1.xml").getFile());

        compareFiles(testResultExpected, xmlFileWriter.getFile());

        //second test writing more patients
    }

    @Test
    public void testReadAndWriteFile() throws Exception {
        // get file from resources
        ClassLoader classLoader = getClass().getClassLoader();
        File testResultExpected = new File(classLoader.getResource("xml/testWritePatient105486415569.xml").getFile());
        // unmarshall file
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        CentraXXDataExchange centraXXData = (CentraXXDataExchange) unmarshaller.unmarshal(testResultExpected);
        // marshall with xmlfilewritter
        File testResultFile = new File(testFile2);
        Marshaller marshaller = jaxbContext.createMarshaller();
        XMLFileWriter xmlFileWriter = new XMLFileWriter(marshaller, testResultFile, centraXXData);
        xmlFileWriter.writeToFile();
        // compare the exported and the created file
        compareFiles(testResultExpected, xmlFileWriter.getFile());
    }

    // Helping method
    private void compareFiles(File expected, File actual) {
        try {
            BufferedReader brExpected = new BufferedReader(new FileReader(expected));
            BufferedReader brActual = new BufferedReader(new FileReader(actual));
            String lineActual = brActual.readLine();
            while (lineActual != null) {
                String lineExpected = brExpected.readLine();
                Assert.assertEquals("This two lines should be equal: ", lineExpected.trim(), lineActual.trim());
                lineActual = brActual.readLine();
            }
            brExpected.close();
            brActual.close();
        } catch (FileNotFoundException fnfe) {
            System.out.println("Couldn't find File");
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            System.out.println("Couldn't read from File");
            ioe.printStackTrace();
        }
    }

}
